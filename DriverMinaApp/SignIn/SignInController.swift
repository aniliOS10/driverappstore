//
//  SignInController.swift
//  DriverMinaApp
//
//  Created by Apple on 02/02/21.
//

import UIKit
import JWTDecode


class SignInController: UIViewController {

    
    @IBOutlet weak var txt_Phone : UITextField!
    @IBOutlet weak var txt_Password : UITextField!

    @IBOutlet weak var btn_Signin : UIButton!
    @IBOutlet weak var btn_Forgot : UIButton!
    @IBOutlet weak var btn_Signup : UIButton!
    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    var eyeClick = true
    var sessionManager = SessionManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        CustomizationTextFeild()
        CustomizationButton()
        addEye()
    }
    
    func topViewLayout(){
        if !SignInController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 38
           }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        let sessionManager = SessionManager()
        sessionManager.saveFirstRequestData_BOOL(isBool:false)
        
        NotificationCenter.default.post(name: Notification.Name("HomeTimer_Invalidate"), object: nil)
        
        NotificationCenter.default.post(name: Notification.Name("Open_Requset_Timer_Invalidate"), object: nil)
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_Phone.tag = 1
        txt_Password.tag = 2
        
        txt_Phone.delegate = self
        txt_Password.delegate = self
        
        txt_Phone.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Password.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        
    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Signin.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        btn_Forgot.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Signup.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    @IBAction fileprivate func btnSignUpAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MobileNoSignUpVc") as! MobileNoSignUpVc
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "AddVehicleVc") as! AddVehicleVc
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction fileprivate func btnSignInAction(_ sender: Any) {
        self.view .endEditing(true)


        let trimmedEmailName = txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let trimmedPassword = txt_Password.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        if (trimmedEmailName?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter your phone no without country code.")
            return
        }
        if let validphone = trimmedEmailName, validphone.isValidPhone() || validphone.EmailValidation() {
                    print("Success")
              }else{
                    print("Enter either valid phone or email 2")
                    MessageAlert(title:"",message: "Please enter your phone no without country code.")
                    return
        }
        if (trimmedPassword?.isEmpty)!{
                    MessageAlert(title:"",message: "Please enter your password.")
                    return
        }
    
        var dict: Dictionary<String, Any> = [:]
            dict["emailPhone"] = trimmedEmailName
            dict["password"] = trimmedPassword
            dict["deviceType"] = GlobalConstants.deviceType
            dict["deviceToken"] = "Device Token".deviceToken
        
            print(dict)
            LoginRequest(Params:dict)
        
    }
    
    @IBAction fileprivate func btnForgotAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotController") as! ForgotController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func LoginRequest(Params:[String: Any]){
        
        DriverLoginRequest.shared.loginData(requestParams: Params) { (obj,status,msg,authToken) in
        
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else{
                
                if obj?.isEmailConfirmed ?? true {
                    
                }
                
                if obj?.isPhoneNoVerified ?? true{
                    
                }
                
                if obj?.screenId == 1 {
                    do {
                        let jsonData = try decode(jwt: obj?.accessToken ?? "")
                        let claim = jsonData.claim(name: "UserId")
                        if let UserId = claim.integer {
                            print("USERID in jwt was \(UserId)")
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalDocumentController") as! PersonalDocumentController
                            vc.str_AccessToken = obj?.accessToken ?? ""
                            vc.int_UserId = UserId
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else{
                        }
                    } catch {
                        print(error)
                    }
                }
                
                if obj?.screenId == 2 {
                    
                    do {
                        let jsonData = try decode(jwt: obj?.accessToken ?? "")
                        let claim = jsonData.claim(name: "UserId")
                        if let UserId = claim.integer {
                            print("USERID in jwt was \(UserId)")
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleVc") as! AddVehicleVc
                            vc.int_UserId = UserId
                            vc.str_AccessToken = obj?.accessToken ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else{
                        }
                    } catch {
                        print(error)
                    }
                }
                if obj?.screenId == 3 {
                    var userModelLogin = UserModel()
                    userModelLogin = obj!
                    self.sessionManager.createLoginSession(userModel:userModelLogin)
                    UserDefaults.standard.set(true, forKey: "Login")
                    RootControllerManager().SetRootViewController()
                }
            }
        }
    }
}

extension SignInController : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func addEye(){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "eye_ic")?.maskWithColor(color: UIColor.lightGray), for: .normal)
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        
        button.frame = CGRect(x: CGFloat(txt_Password.frame.size.width - 40), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        
        button.addTarget(self, action: #selector(self.reveal), for: .touchUpInside)
        
        txt_Password.rightView = button
        txt_Password.rightViewMode = .always
        
        }
        
        @IBAction func reveal(_ sender: UIButton) {
            if(eyeClick == true) {
                sender.setImage(UIImage(named: "eye_ic"), for: .normal)
                txt_Password.isSecureTextEntry = false
            } else {
                sender.setImage(UIImage(named: "eye_ic")?.maskWithColor(color: UIColor.lightGray), for: .normal)
                txt_Password.isSecureTextEntry = true
            }
            eyeClick = !eyeClick
        }
}


extension UIViewController
{
    static var hasSafeArea: Bool {
        guard #available(iOS 11.0, *), let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
            return false
        }
        return true
    }

    
    func MessageAlert(title:String,message:String)
    {
        let alert = UIAlertController(title:"Oops!", message:  message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    
    
    func popMessageAlert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message:  message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
            
            self.navigationController?.popViewController(animated: true)
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    
    func SessionMessageAlert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message:  message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
        
            NotificationCenter.default.post(name: Notification.Name("SessionExpire"), object: nil)
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
}
extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
           let coloredImage = UIImage(cgImage: cgImage)
           return coloredImage
        } else {
           return nil
        }
    }
}
