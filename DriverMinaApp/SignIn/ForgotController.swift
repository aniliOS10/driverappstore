//
//  ForgotController.swift
//  DriverMinaApp
//
//  Created by Apple on 08/02/21.
//

import UIKit

class ForgotController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var txt_Phone : UITextField!
    @IBOutlet weak var btn_Continue : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        CustomizationTextFeild()
        CustomizationButton()
    }
    
    func topViewLayout() {
        if !ForgotController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        txt_Phone.becomeFirstResponder()
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_Phone.delegate = self
        txt_Phone.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }

    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Continue.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
    }
    

    @IBAction fileprivate func backAction(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func btnContinueAction(_ sender: Any) {
        self.view .endEditing(true)

        //without spaces email
        let trimmedEmailName = txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (trimmedEmailName?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter your phone or email")
            return
        }
        
        if let validphone = trimmedEmailName, validphone.isValidPhone() || validphone.EmailValidation() {
                print("Success")
              }else{
                print("Enter either valid phone or email 2")
                MessageAlert(title:"",message: "Enter either valid phone or email")
                return
        }
        
        forgotAPIRequest(Params:["emailPhone": trimmedEmailName ?? ""])
    }
    
    func forgotAPIRequest(Params:[String: Any]){
        
        ForgotPasswordRequest.shared.forgotData(requestParams: Params) { (obj,status,msg,authToken) in
        
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordController") as! ResetPasswordController
                vc.emailStr = self.txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


extension ForgotController : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}

