//
//  ResetPasswordController.swift
//  DriverMinaApp
//
//  Created by Apple on 01/03/21.
//

import UIKit

class ResetPasswordController: UIViewController {
    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var txt_Phone : UITextField!
    @IBOutlet weak var txt_OTP : UITextField!
    @IBOutlet weak var txt_Password : UITextField!

    @IBOutlet weak var btn_Done : UIButton!
    
    var emailStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        CustomizationTextFeild()
        CustomizationButton()
        txt_Phone.text = emailStr
    }
    
    func topViewLayout() {
        if !ResetPasswordController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_Phone.delegate = self
        txt_OTP.delegate = self
        txt_Password.delegate = self

        txt_Phone.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_OTP.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Password.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        
    }

    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Done.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
    }
    

    @IBAction fileprivate func backAction(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction fileprivate func resendOTPAction(_ sender: Any) {
        
        ForgotPasswordRequest.shared.forgotData(requestParams: ["emailPhone": emailStr ]) { (obj,status,msg,authToken) in
        
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else{
                let alert = UIAlertController(title:"", message:"Otp sent on your phone no", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"Ok" , style: .cancel, handler:{ (UIAlertAction)in
                    self.txt_OTP.text? = ""
                }))
                self.present(alert, animated: true, completion: {
                    
                })
            }
        }
    }
    
    @IBAction fileprivate func btnContinueAction(_ sender: Any) {
        self.view .endEditing(true)

        //without spaces email
        let trimmedEmailName = txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (trimmedEmailName?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter your phone or email")
            return
        }
        
        if let validphone = trimmedEmailName, validphone.isValidPhone() || validphone.EmailValidation() {
                print("Success")
              }else{
                print("Enter either valid phone or email 2")
                MessageAlert(title:"",message: "Enter either valid phone or email")
                return
        }
        
        
        let otpString = txt_OTP.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (otpString?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter OTP")
            return
        }
        
        let passString = txt_Password.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (passString?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter Password")
            return
        }
        if passString!.count < 6{
            MessageAlert(title:"",message: "Required minimum 6 Characters Password")
            return
        }
        
        
        var dict: Dictionary<String, Any> = [:]
            dict["emailPhone"] = trimmedEmailName
            dict["otp"] = otpString
            dict["newPassword"] = passString

        reSetAPIRequest(Params:dict)
    }
    
    func reSetAPIRequest(Params:[String: Any]){
        
        ResetDriverPasswordRequest.shared.resetData(requestParams: Params) { (obj,status,msg,authToken) in
        
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else{
                let alert = UIAlertController(title:"", message:msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"Ok" , style: .cancel, handler:{ (UIAlertAction)in
                    
                    self.navigationController!.popToRootViewController(animated: true)

                }))
                self.present(alert, animated: true, completion: {
                    
                })

            }
            
        }
    }
    
}



extension ResetPasswordController : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}


