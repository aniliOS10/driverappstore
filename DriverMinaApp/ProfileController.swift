//
//  ProfileController.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit

class ProfileController: UIViewController {

    @IBOutlet weak var lbeName: UILabel!
    @IBOutlet weak var lbeSp: UILabel!
    @IBOutlet weak var lbe_PhoneNo: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var lbeRating: UILabel!
    @IBOutlet weak var lbeTodayEar: UILabel!

    var userModel = UserModel()
    var sessionManager = SessionManager()
    var personalInfoModel = Personal_VehicleDetails()

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
    }
    
    func topViewLayout(){
        if !ProfileController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        headerUpdate()
        personalInfoModel = sessionManager.getPersonalVehicleDetails()
        lbeRating.text = String(personalInfoModel.rating)
        
        lbeTodayEar.text = String(format: "R %.2f", personalInfoModel.todayEarnings)
        
        if personalInfoModel.todayEarnings == 0.0 {
            lbeTodayEar.text =  "R 0.00"
        }
    }
    
    
    @IBAction func Rating_Action(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "YourRatingController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction fileprivate func p_Action(_ sender: Any){

        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "PersonalDetailsUpdateVc")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction fileprivate func VehicleDetails_Action(_ sender: Any){
        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "VehicleDetailsVc")
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func BankDetails_Action(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "BankDetailsVc")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction fileprivate func Earning_Action(_ sender: Any) {

        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "EarningReportsVc")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    //MARK:-   headerUpdate

    func headerUpdate(){
        
        userModel = sessionManager.getUserDetails()
        let img = GlobalConstants.ImageBaseURL  + userModel.pic
        let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var strURL = String()
            strURL = escapedAddress ?? ""
        
        print(strURL)
        
        imgProfile.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))
        
        lbeName.text = userModel.firstName + " " + userModel.lastName
        
        if userModel.phoneNumber != "" {
            lbe_PhoneNo.text = String(userModel.phoneNumber)
        }
    }
}
