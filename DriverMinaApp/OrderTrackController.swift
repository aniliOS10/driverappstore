//
//  OrderTrackController.swift
//  DriverMinaApp
//
//  Created by Apple on 08/03/21.
//

import UIKit
import MapKit
import CoreLocation
class OrderTrackController: UIViewController ,MKMapViewDelegate{

    
    var requestID = 0
    var orderNumber = 0
    var senderNumber = ""

    let sessionManager = SessionManager()
    var acceptOrderArray : [GetAcceptedModel] = []
    
    @IBOutlet weak var lbe_Delivery: UILabel!
    @IBOutlet weak var lbe_Distance: UILabel!
    @IBOutlet weak var lbe_Duration: UILabel!
    @IBOutlet weak var lbe_SType: UILabel!
    @IBOutlet weak var lbe_Status: UILabel!
    @IBOutlet weak var lbe_DateTime: UILabel!
    @IBOutlet weak var lbe_OrderNo: UILabel!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        dataShowOnView()
    }
    
    func topViewLayout(){
        if !OrderTrackController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    func dataShowOnView(){
        if self.acceptOrderArray.count > 0 {
            let data : GetAcceptedModel = self.acceptOrderArray[0]
            self.lbe_Duration.text = data.duration
            self.lbe_Distance.text = data.distance
            self.lbe_SType.text = data.deliveryType
            self.lbe_OrderNo.text = String(orderNumber)
            self.lbe_DateTime.text = data.notifyDate + ", " + data.notifyTime
         // self.lbe_Pickup.text = data.senderAddress
            self.lbe_Delivery.text = data.senderAddress
            self.lbe_Status.text = data.status
            
            if data.deliveryType == "Normal"{
                lbe_SType.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).greenColor(Alpha: 1)
            }
            else if data.deliveryType == "Express"{
                lbe_SType.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).redPlan(Alpha: 1)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @IBAction fileprivate func senderCall_Action(_ sender: Any){
        
            guard let url = URL(string: "telprompt://\(senderNumber)"),
                UIApplication.shared.canOpenURL(url) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func Direction_Action(_ sender: Any) {
        alertSheet()
    }
    
    
    @IBAction fileprivate func ParcelDetails_Action(_ sender: Any) {
        
        let storyBoard_Home = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard_Home.instantiateViewController(withIdentifier: "ParcelDetailsRequestController") as! ParcelDetailsRequestController;     controller.requestID = requestID
            controller.isHide_Action = true
        self.navigationController?.pushViewController(controller, animated: true)
        
        
//        let storyboard = UIStoryboard(name: "Home", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ParcelDetailsRequestController") as! ParcelDetailsRequestController
//        vc.requestID = requestID
//        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//            self.present(vc, animated: true, completion: nil)
    }

    @IBAction fileprivate func parcelPickup_Action(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "Call_Api_Accepted")

        var dict: Dictionary<String, Any> = [:]
            dict["driverId"] = sessionManager.getUserDetails().userID
            dict["requestId"] = requestID
            dict["statusId"] = 3
            dict["driverLat"] = Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            dict["driverLong"] = Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            print(dict)

        DeliveryStatusDetailsRequest.shared.deliveryStatusData(requestParams: dict, true) { (obj,status,msg,strAny) in
        
            if status {
                self.alertViewSuccess(title: "Done", mess: msg)
            }
            else{
                
            }
        }
    }
    
    
    @IBAction fileprivate func goForDelivery_Action(_ sender: Any) {
        
        var dict: Dictionary<String, Any> = [:]
            dict["driverId"] = sessionManager.getUserDetails().userID
            dict["requestId"] = requestID
            dict["statusId"] = 4
        dict["driverLat"] = Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
        dict["driverLong"] = Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            print(dict)

        DeliveryStatusDetailsRequest.shared.deliveryStatusData(requestParams: dict, true) { (obj,status,msg,strAny) in
        
            if status {
                self.alertViewSuccess(title: "Done", mess: msg)
            }
            else{
                
            }
        }
    }

    func alertViewSuccess(title:String,mess:String){
           let alert = UIAlertController(title: title, message:mess, preferredStyle: .alert)
           
           let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
               self.navigationController?.popViewController(animated: true)

           })
           alert.addAction(ok)
           
           DispatchQueue.main.async(execute: {
               self.present(alert, animated: true)
           })
       }

    
    
    //MARK:-   Map Alert Function
    
    func alertSheet(){
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Choose application", message: "" , preferredStyle: .actionSheet)
        
            let MapButton = UIAlertAction(title: "Maps", style: .default) { _ in
                
                self.openAppleMap()
            
            }
            actionSheetControllerIOS8.addAction(MapButton)
            
            let GoogleButton = UIAlertAction(title: "Google Maps", style: .default)
            { _ in
                self.googleMap()
                
            }
            actionSheetControllerIOS8.addAction(GoogleButton)
            
            
            let CancelActionButton = UIAlertAction(title: "Cancel", style: .cancel)
            { _ in
                
            }
            actionSheetControllerIOS8.addAction(CancelActionButton)
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
            
            
            
        }else{
            openAppleMap()
        }
    }
    
    
    
    func googleMap(){
        
        if self.acceptOrderArray.count > 0 {
             let data : GetAcceptedModel = self.acceptOrderArray[0]
            
            let  tlatitude = Double(data.senderLat)
            let  tlongitude = Double(data.senderLong)
            
            let urlbase = URL(string: "comgooglemaps://?saddr=&daddr=\(tlatitude),\(tlongitude)")
           
            UIApplication.shared.open(urlbase!, options: [:]
               , completionHandler: nil)
            
        }
    }

    
    func openAppleMap() {
        
    if self.acceptOrderArray.count > 0 {
        
        let data : GetAcceptedModel = self.acceptOrderArray[0]

        let  tlatitude = Double(data.senderLat)
        let  tlongitude = Double(data.senderLong)
        
        let center = CLLocationCoordinate2D(latitude:tlatitude, longitude:tlongitude)

         openMapsAppWithDirections(to: center, destinationName: data.senderAddress)
    }
}
    
    
    func openMapsAppWithDirections(to coordinate: CLLocationCoordinate2D, destinationName name: String) {
      let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
      let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = name 
        mapItem.openInMaps(launchOptions: options)
    }
    
}
