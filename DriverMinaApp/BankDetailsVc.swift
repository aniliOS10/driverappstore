//
//  BankDetailsVc.swift
//  DriverMinaApp
//
//  Created by Apple on 06/04/21.
//

import UIKit

class BankDetailsVc: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    @IBOutlet weak var txtAcName: UITextField!
    @IBOutlet weak var txtAcNo: UITextField!
    @IBOutlet weak var txtBranch: UITextField!
    @IBOutlet weak var txtSwift: UITextField!
    @IBOutlet weak var txtBank: UITextField!
    @IBOutlet weak var txtAddressBank: UITextField!
    
    let sessionManager = SessionManager()
    var BankInfo : GetBankProfileObject?
    
    var accId = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        GetBankInfoAPIRequest()
    }
    
    func topViewLayout(){
        if !BankDetailsVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Get Bank Info API Request
    func GetBankInfoAPIRequest(){
        self.view.endEditing(true)
    
        GetBankInfoDataAPIRequest.shared.GetDoctorBank(requestParams: [:]) { (obj, message, status) in
            
            if status == false {
                
              //  self.MessageAlert(title: "", message: message!)
            }
            else
            {
                self.BankInfo = obj
                self.bankInfo()
            }
        }
    }
    
    
    // MARK: - Refresh Bank Info
    func bankInfo()
    {
        txtAcName.text = self.BankInfo?.accountName
        txtAcNo.text = self.BankInfo?.accountNumber
        txtBank.text = self.BankInfo?.bank
        txtBranch.text = self.BankInfo?.branchCode
        txtAddressBank.text = self.BankInfo?.branch
        txtSwift.text = self.BankInfo?.swiftCode
        accId  = self.BankInfo?.accId ?? 0
    }
    
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func Save_Action(_ sender: Any) {
        
        if txtAcName.text == ""{
            self.showErrorMSg(text: "Please enter Account Name")
            return
        }
        
        if txtAcNo.text == ""{
            self.showErrorMSg(text: "Please enter Account No")
            return
        }
    
    
        if txtBranch.text == ""{
            self.showErrorMSg(text: "Please enter Branch Code")
            return
        }
        
        if txtSwift.text == ""{
            self.showErrorMSg(text: "Please enter Swift Code")
            return
        }
        
        if txtBank.text == "" {
            self.showErrorMSg(text: "Please enter Bank Name")
            return
        }
    
        if txtAddressBank.text == ""{
            self.showErrorMSg(text: "Please enter Branch Address")
            return
        }
    
        
        let params = ["accId": accId,
                      "driverId": sessionManager.getUserDetails().userID,
                      "accountName":txtAcName.text ?? "",
                      "accountNumber":txtAcNo.text ?? "",
                      "branchCode":txtBranch.text ?? "",
                      "swiftCode":txtSwift.text ?? "",
                      "bank":txtBank.text ?? "",
                      "branch":txtAddressBank.text ?? "",
        ] as [String : Any]

        
        AddBankInfoAPI.shared.AddBank(requestParams:params, isLoader: true) { (message,status,New)  in

                           if !status
                           {
                               if message == "" {
                                   self.showErrorMSg(text: "Something Went Wrong")
                               }
                               else{
                                   self.showErrorMSg(text: message!)
                               }
                           }
                           else
                           {
                            self.alertViewSuccess(title: "Save", mess:"Bank details save successfully.")
                           }
            }
       }
    
    
    func alertViewSuccess(title:String,mess:String){
           let alert = UIAlertController(title: title, message:mess, preferredStyle: .alert)
           let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
            self.navigationController?.popViewController(animated: true)

           })
           alert.addAction(ok)
           DispatchQueue.main.async(execute: {
               self.present(alert, animated: true)
           })
       }
    }
class AddBankInfoAPI: NSObject {
    
    static let shared = AddBankInfoAPI()
    
    func  AddBank(requestParams : [String:Any],isLoader:Bool, completion: @escaping (_ message : String?, _ status : Bool,_ session:Bool) -> Void) {
        
        var strUrl = ""
        strUrl = "BaseURL".AddBankInfoURL
    
        print("requestParams - %@", requestParams)
        let sessionManager = SessionManager()

        
        AlamofireRequest.shared.PostBodyForRaw(urlString: strUrl, parameters: requestParams , authToken: sessionManager.getUserDetails().accessToken, isLoader: isLoader, loaderMessage: "") { (data, error) in
            
            if error == nil{
                
                if let status = data?["status"] as? Bool{
                    
                    var messageString : String = ""
    
                    if let msg = data?["message"] as? String{
                        messageString = msg
                    }
                    if status == true{
                        completion(messageString, true,true)
                    }
                    else{
                        completion(messageString, false,true)
                    }
                }
                else
                {
                    if let status = data?["message"] as? String{
                        completion(status, false,true)
                    }
                    else{
                       completion(MainController().languageKey(key: "There was an error connecting to the server."), false,true)
                    }
                }
            }else{
                completion(MainController().languageKey(key: "There was an error connecting to the server.try again"), false,true)
            }
        }
    }
}


class GetBankInfoDataAPIRequest: NSObject {
    
    static let shared = GetBankInfoDataAPIRequest()
    let sessionManager = SessionManager()

    func GetDoctorBank(requestParams : [String:Any], completion: @escaping ( _ object: GetBankProfileObject?, _ message : String?, _ status : Bool) -> Void) {
        
        let url = "BaseURL".GetDriverBankDetailsURL

        var urlString = String()
        urlString  =  String(format: "%@?driverId=%d",url,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)
        
        AlamofireRequest.shared.PostBodyFrom(urlString:urlString, isLoader : true,param : nil ,loaderMessage : "Getting",auth:sessionManager.getUserDetails().accessToken, isNetwork: true) { (data,error) in
            
            if error == nil{
                
                if let status = data?["status"] as? Bool{
                    
                    var messageString : String = ""

                    if let msg = data?["message"] as? String{
                        messageString = msg
                    }
    
                    if status == true{
            
                        if let Result = data?["data"] as? [String : Any]{
                           let PatientPersonalModelss : GetBankProfileObject = GetBankProfileObject.init(model: Result as [String : Any])
                            completion(PatientPersonalModelss,messageString, status)
                        }
                    }
                    else
                    {
                        completion(nil,messageString,status)
                    }
                }
                else
                {
                    completion(nil, "There was an error connecting to the server.",false)
                    
                }
            }
            else{
                completion(nil,"There was an error connecting to the server.try again",false)
            }
        }
    }
}

class GetBankProfileObject: NSObject {

    var accountName =  ""
    var accountNumber =  ""
    var branchCode  =  ""
    var swiftCode  =  ""
    var bank  =  ""
    var branch  =  ""
    var accId = 0

   
    init(model: [String : Any]) {
        
        
        if let accIdINT = model["accountName"] as? Int{
            self.accId = accIdINT
        }
        if let medicalDegrees = model["accountName"] as? String{
            self.accountName = medicalDegrees
        }
        if let degreeFroms = model["accountNumber"] as? String{
            self.accountNumber = String(degreeFroms)
        }
        if let otherDegrees = model["branchCode"] as? String{
            self.branchCode = otherDegrees
        }
        if let otherDegrees = model["swiftCode"] as? String{
            self.swiftCode = otherDegrees
        }
        if let otherDegrees = model["bank"] as? String{
            self.bank = otherDegrees
        }
        if let otherDegrees = model["branch"] as? String{
            self.branch = otherDegrees
        }
    }
}


