//
//  CompletedTableViewCell.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit
class CompletedTableViewCell: UITableViewCell {

    @IBOutlet weak var lbe_DateTime: UILabel!
    @IBOutlet weak var lbe_SType: UILabel!
    @IBOutlet weak var lbe_DeliveryPrice: UILabel!
    @IBOutlet weak var mapView_Parcel: GMSMapView!
    @IBOutlet weak var lbe_Pickup: UILabel!
    @IBOutlet weak var lbe_Delivery: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func dataShow_OnView(data:GetCompletedModel){
        
         let objectParcelDetails : GetCompletedModel = data
        
        lbe_Pickup.text = objectParcelDetails.senderAddress
        lbe_Delivery.text = objectParcelDetails.receiverAddress


//         let latS = String(objectParcelDetails.senderLat)
//         let logS = String(objectParcelDetails.senderLong)
//
//         let destinationLat = String(objectParcelDetails.receiverLat)
//         let destinationLong = String(objectParcelDetails.receiverLong)
//
//        let cl_lat =   CLLocationCoordinate2D(latitude: Double(latS) ?? 00.00, longitude: Double(logS) ?? 00.00).latitude
//
//        let cl_long = CLLocationCoordinate2D(latitude: Double(latS) ?? 00.00, longitude: Double(logS) ?? 00.00).longitude
//
//        let camera = GMSCameraPosition.camera(withLatitude:cl_lat,longitude:cl_long, zoom:12.5)
//
//        self.mapView_Parcel.animate(to: camera)
//
//        ParcelDetails_DrawLine(SourceLat: latS, SourceLong:logS, DestinationLat: destinationLat, DestinationLong:destinationLong)
    }
    

  func ParcelDetails_DrawLine(SourceLat:String , SourceLong: String , DestinationLat: String , DestinationLong : String)
    {
    
        self.mapView_Parcel.clear()

        let Pickup = GMSMarker()
        Pickup.position = CLLocationCoordinate2D(latitude: Double(SourceLat) ?? 00.00, longitude: Double(SourceLong) ?? 00.00)
        Pickup.icon = UIImage(named: "cir_blue_point")
        Pickup.title = "Pickup"
        Pickup.map = mapView_Parcel
    
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(Pickup.position)
    
        let Delivery = GMSMarker()
        Delivery.position = CLLocationCoordinate2D(latitude: Double(DestinationLat) ?? 00.00, longitude: Double(DestinationLong) ?? 00.00 )
        Delivery.icon = UIImage(named: "squar_green_point")
        Delivery.title = "Delivery"
        Delivery.map = mapView_Parcel
    
        drawLine(SourceLat: SourceLat, SourceLong: SourceLong, DestinationLat: DestinationLat, DestinationLong: DestinationLong)
    }
    
    
    func drawLine(SourceLat:String , SourceLong: String , DestinationLat: String , DestinationLong : String)
    {

        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let origin = "\(SourceLat),\(SourceLong)"
        let destination = "\(DestinationLat),\(DestinationLong)"

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=driving&key=AIzaSyBKjkZ9biB8ye5_kUJaYl2YYYCLDCkzM8s")!
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {

                        if let preRoutes = json["routes"] as? NSArray {
                            if  preRoutes.count > 0 {
                                let routes = preRoutes[0] as! NSDictionary
                            let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                            let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                            DispatchQueue.main.async(execute: {
                                let path = GMSPath(fromEncodedPath: polyString)
                                let polyline = GMSPolyline(path: path)
                                polyline.strokeWidth = 3
                                polyline.strokeColor = UIColor.black
                                polyline.map = self.mapView_Parcel
                                
                                if self.mapView_Parcel != nil
                                {
                                 let bounds = GMSCoordinateBounds(path: path!)
                                 self.mapView_Parcel!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                                }
                            })
                        }
                        }
                    }
                } catch {
                    print("parsing error")
                }
            }
        })
        task.resume()
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
