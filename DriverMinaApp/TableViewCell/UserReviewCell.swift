//
//  UserReviewCell.swift
//  DriverMinaApp
//
//  Created by Apple on 06/04/21.
//

import UIKit

class UserReviewCell: UITableViewCell {

    
    @IBOutlet weak var lbe_UserName: UILabel!
    @IBOutlet weak var lbe_Review: UILabel!
    @IBOutlet weak var lbe_Date: UILabel!
    @IBOutlet weak var lbe_Rating: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
