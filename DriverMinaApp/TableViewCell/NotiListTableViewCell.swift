//
//  NotiListTableViewCell.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit

class NotiListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbe_Text: UILabel!
    @IBOutlet weak var lbe_Date: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(Notification : NotificationModel){

        lbe_Text.text = Notification.text
        getTimeSendNewDate(Notification.createdOn)
        
    }
    
    
    func getTimeSendNewDate(_ releaseDate : String) -> (Int,Int){
        
        let currentDate = Date()
        let calendar = Calendar.current
      
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let stdate : String = releaseDate
        let startDate = dateFormatter.date(from: stdate) ?? Date()
      
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: startDate, to: currentDate)
        
        var minuteInt = 0
      
        if diffDateComponents.minute! >  0 {
            minuteInt = (diffDateComponents.minute ?? 0)
        }
        
        if minuteInt == 0
        {
            lbe_Date.text = "Just Now"
        }
        
        if minuteInt > 0 {
            
            if minuteInt == 1 {
                lbe_Date.text = String(format: "%d minute ago", minuteInt)
            }
            else{
                lbe_Date.text = String(format: "%d minutes ago", minuteInt)
            }
        }
        
        if diffDateComponents.hour! >  0 {
            
            if diffDateComponents.hour! == 1 {
                lbe_Date.text = String(format: "%d hour ago", diffDateComponents.hour!)
            }
            else{
                lbe_Date.text = String(format: "%d hours ago", diffDateComponents.hour!)
            }
        }
        
        if diffDateComponents.day! >  0 {
            
            if diffDateComponents.day! == 1 {
                lbe_Date.text = String(format: "%d day ago", diffDateComponents.day!)

            }
            else{
                lbe_Date.text = String(format: "%d days ago", diffDateComponents.day!)

            }
        }
        
        return (minuteInt,0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
