//
//  MyJobsAcceptTableViewCell.swift
//  DriverMinaApp
//
//  Created by Apple on 05/03/21.
//

import UIKit

class MyJobsAcceptTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Cancel: UIButton!
    
    
    @IBOutlet weak var lbe_Pickup: UILabel!
    @IBOutlet weak var lbe_Delivery: UILabel!
    @IBOutlet weak var lbe_Distance: UILabel!
    @IBOutlet weak var lbe_Duration: UILabel!
    @IBOutlet weak var lbe_Price: UILabel!
    @IBOutlet weak var lbe_SType: UILabel!
    @IBOutlet weak var lbe_Status: UILabel!
    @IBOutlet weak var lbe_DateTime: UILabel!
    @IBOutlet weak var lbe_OrderNo: UILabel!


    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
