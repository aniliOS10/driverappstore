//
//  NewRequestTableViewCell.swift
//  DriverMinaApp
//
//  Created by Apple on 03/03/21.
//

import UIKit

class NewRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Details: UIButton!
    
    @IBOutlet weak var btn_Accept: UIButton!
    @IBOutlet weak var btn_Reject: UIButton!

    
    @IBOutlet weak var lbe_Pickup: UILabel!
    @IBOutlet weak var lbe_Delivery: UILabel!
    @IBOutlet weak var lbe_Distance: UILabel!
    @IBOutlet weak var lbe_Duration: UILabel!
    @IBOutlet weak var lbe_SerType: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
