//
//  ParcelDetailsListTableViewCell.swift
//  DriverMinaApp
//
//  Created by Apple on 03/03/21.
//

import UIKit

class ParcelDetailsListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbe_Title: UILabel!
    @IBOutlet weak var lbe_Data: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
