//
//  BrandListController.swift
//  DriverMinaApp
//
//  Created by Apple on 20/04/21.
//

import UIKit

class BrandListController: UIViewController,UISearchBarDelegate {

    
    var carBrandsArray = NSMutableArray()
    var carBrandsAllArray = NSMutableArray()

    var VehicleID_Selected = 0
    
    var DataPassBrand : ((_ startTime : String ,_ endTime : Int) -> ())?
    var DataPassColors : ((_ startTime : String ,_ endTime : Int) -> ())?

    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var tableView_Brand: UITableView?
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var lbeTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        searchBar.delegate = self
        
        if VehicleID_Selected == 0 {
            lbeTitle.text = "Colors"
        }
        else if VehicleID_Selected == 1 {
            lbeTitle.text = "Scooter Brands"
        }
        else if (VehicleID_Selected == 2){
            lbeTitle.text = "Car Brands"
        }
        else{
            lbeTitle.text = "Bakkie Brands"
        }

        if VehicleID_Selected == 0 {
            getAllVehicleColoursAPI(true)
        }
        else{
            getAllBrandsAPI(true)
        }
    }
    
    func topViewLayout(){
        if !BrandListController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    func getAllVehicleColoursAPI(_ loader:Bool){
        
        VehicleColoursRequest.shared.getVehicleColoursData(requestParams: [:],loader) { (object,status)  in
            if status {

            self.carBrandsAllArray.removeAllObjects()

            
            for i in 0..<(object?.data?.list?.count ?? 0)
            {
                let dictArray = object?.data?.list?[i]
                let dict = NSMutableDictionary()
                dict["vehicleColorName"] = dictArray?.vehicleColorName
                dict["vehicleColorID"] = dictArray?.vehicleColorID
                dict["colors"] = dictArray?.vehicleColorCode
                self.carBrandsAllArray.add(dict)
            }
                self.carBrandsArray = self.carBrandsAllArray
                self.tableView_Brand?.reloadData()
        }
    }
    }
    
    

    func getAllBrandsAPI(_ loader:Bool){
        
        VehicleBrandsRequest.shared.getVehicleBrandsData(requestParams: [:],loader) { [self] (object,status)  in
            if status {
               
            self.carBrandsAllArray.removeAllObjects()
            if self.VehicleID_Selected == 1 {
                
                for i in 0..<(object?.scooterBrands?.scooterBrands?.count ?? 0)
                {
                    let dict = NSMutableDictionary()
                    dict["brandID"] = object?.scooterBrands?.scooterBrands?[i].brandID
                    dict["brandName"] = object?.scooterBrands?.scooterBrands?[i].brandName
                    self.carBrandsAllArray.add(dict)
                }
            }
            
            if self.VehicleID_Selected == 2 {
                for i in 0..<(object?.carBrands?.carBrands?.count ?? 0)
                {
                    let dict = NSMutableDictionary()
                    dict["brandID"] = object?.carBrands?.carBrands?[i].brandID
                    dict["brandName"] = object?.carBrands?.carBrands?[i].brandName
                    self.carBrandsAllArray.add(dict)
                }
            }
            
            if self.VehicleID_Selected == 3 {
                for i in 0..<(object?.bakkieBrands?.bakkieBrands?.count ?? 0)
                {
                    let dict = NSMutableDictionary()
                    dict["brandID"] = object?.bakkieBrands?.bakkieBrands?[i].brandID
                    dict["brandName"] = object?.bakkieBrands?.bakkieBrands?[i].brandName
                    self.carBrandsAllArray.add(dict)
                }
            }
                carBrandsArray = carBrandsAllArray
                tableView_Brand?.reloadData()
        }
    }
}
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchBar.showsCancelButton = false
            searchBar.text = ""
            searchBar.resignFirstResponder()
        
        carBrandsArray = carBrandsAllArray
        tableView_Brand?.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String){
        
        if VehicleID_Selected == 0 {
            let searchPredicate = NSPredicate(format: "vehicleColorName contains[c] %@", searchText)
            let array = (self.carBrandsAllArray).filtered(using: searchPredicate)
            self.carBrandsArray =  NSMutableArray(array: array)
            self.tableView_Brand!.reloadData()
        }
        else{
            let searchPredicate = NSPredicate(format: "brandName contains[c] %@", searchText)
            let array = (self.carBrandsAllArray).filtered(using: searchPredicate)
            self.carBrandsArray =  NSMutableArray(array: array)
            self.tableView_Brand!.reloadData()
        }
    }
}

//MARK: - TableViewDataSource Delegate
extension BrandListController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carBrandsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "BrandListCell", for: indexPath) as? BrandListCell else {
            return BrandListCell()
        }
        
        cellOf.selectionStyle = .none
        var dict = NSDictionary()
        dict = carBrandsArray.object(at: indexPath.row) as! NSDictionary
        
        if VehicleID_Selected == 0 {
            cellOf.lbe_Name.text = dict["vehicleColorName"] as? String
            let Color = UIColor(hexString: dict["colors"] as? String ?? "")
            cellOf.viewColor.backgroundColor = Color
            cellOf.viewColor.isHidden = false
        }
        else{
            cellOf.lbe_Name.text = dict["brandName"] as? String
            cellOf.viewColor.backgroundColor = UIColor.white
            cellOf.viewColor.isHidden = true
        }
        return cellOf
    }
}


extension BrandListController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        var dict = NSDictionary()
        dict = carBrandsArray.object(at: indexPath.row) as! NSDictionary
        if VehicleID_Selected == 0 {
            self.DataPassColors?(dict["vehicleColorName"] as! String,dict["vehicleColorID"] as! Int )
        }
        else{
            self.DataPassBrand?(dict["brandName"] as! String,dict["brandID"] as! Int )
        }
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
