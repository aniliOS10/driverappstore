//
//  GetDriverInfoRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 22/04/21.
//

import UIKit

class GetDriverInfoRequest: NSObject {
    
    static let shared  = GetDriverInfoRequest()
    
    func GetDriverInfoData(requestParams:[String:Any],_ loader: Bool, completion: @escaping (_ object: Personal_VehicleDetails?, _ status : Bool) -> Void) {

        let sessionManager = SessionManager()
        let url = "BaseURL".GetDriverInfoURL
    
        var urlString = String()
        urlString  =  String(format: "%@?driverId=%d",url,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)

        AlamofireRequest.shared.PostBodyFrom(urlString:urlString,isLoader:false, param:nil,loaderMessage: "", auth:sessionManager.getUserDetails().accessToken,isNetwork:true) { (data,error) in
            
            if error == nil
                {
                if let status = data?["status"] as? Bool{
                    if status == true{
                        if let Result = data?["data"] as? [String : Any]{
                            print(Result)

                            let userModel : Personal_VehicleDetails = Personal_VehicleDetails.init(model: Result as [String : Any])

                            completion(userModel,true)
                    }
                }
                else{
                      completion(nil,false)
                }
            }
        }
    }
}
}



class Personal_VehicleDetails: NSObject, NSCoding{

    var name : String?
    var phoneNumber: String?
    var vehicleBrand : String?
    var vehicleColor: String?
    var registrationNumber: String?
    var profilePic = ""
    var dob: String?
    var email: String?
    var address: String?
    var city: String?
    var province: String?
    var vehicleTypeId = 0
    var todayEarnings = 0.0
    var rating = 0.0
    var driverSelfieImgPath = ""
    var driverLicenseImgPath = ""
    var driverProofOfResidenceImgPath = ""
    var vehicleRegisterationImgPath = ""
    var vehicleFrontSideImgPath = ""
    var vehicleBackSideImgPath = ""
    var provinceId = 0
    var vehicleBrandId = 0
    var vehicleColorId = 0
    override init() {
        
    }
    
    init(model: [String : Any]) {
        
        if let n = model["name"] as? String{
            self.name = n
        }
        if let p = model["phoneNumber"] as? String{
            self.phoneNumber = p
        }
        
        if let vb = model["vehicleBrand"] as? String{
            self.vehicleBrand = vb
        }
        
        if let vc = model["vehicleColor"] as? String{
            self.vehicleColor = vc
        }
        
        if let rn = model["registrationNumber"] as? String{
            self.registrationNumber = rn
        }
        
        if let pp = model["profilePic"] as? String{
            self.profilePic = pp
        }
        
        if let e = model["email"] as? String{
            self.email = e
        }
        
        if let ad = model["address"] as? String{
            self.address = ad
        }
        
        if let ci = model["city"] as? String{
            self.city = ci
        }
        
        if let pr = model["province"] as? String{
            self.province = pr
        }
        
        if let vt = model["vehicleTypeId"] as? Int{
            self.vehicleTypeId = vt
        }
        
        if let te = model["todayEarnings"] as? Double{
            self.todayEarnings = te
        }
        
        if let r = model["rating"] as? Double{
            self.rating = r
        }
        
        if let si = model["driverSelfieImgPath"] as? String{
            self.driverSelfieImgPath = si
        }
        
        if let dl = model["driverLicenseImgPath"] as? String{
            self.driverLicenseImgPath = dl
        }
        if let pr = model["driverProofOfResidenceImgPath"] as? String{
            self.driverProofOfResidenceImgPath = pr
        }
        
        if let vr = model["vehicleRegisterationImgPath"] as? String{
            self.vehicleRegisterationImgPath = vr
        }
       
        if let vr = model["vehicleFrontSideImgPath"] as? String{
            self.vehicleFrontSideImgPath = vr
        }
        
        if let vr = model["vehicleBackSideImgPath"] as? String{
            self.vehicleBackSideImgPath = vr
        }
        
        if let te = model["provinceId"] as? Int{
            self.provinceId = te
        }
    
        if let te = model["vehicleBrandId"] as? Int{
            self.vehicleBrandId = te
        }
        
        if let te = model["vehicleColorId"] as? Int{
            self.vehicleColorId = te
        }
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        var mutDict : [String : Any] = [:]
    
        mutDict["name"] =  aDecoder.decodeObject(forKey: "name")
        mutDict["email"] =  aDecoder.decodeObject(forKey: "email")

        mutDict["phoneNumber"] =  aDecoder.decodeObject(forKey: "phoneNumber")
        mutDict["vehicleBrand"] =  aDecoder.decodeObject(forKey: "vehicleBrand")
        mutDict["vehicleColor"] =  aDecoder.decodeObject(forKey: "vehicleColor")
        mutDict["registrationNumber"] =  aDecoder.decodeObject(forKey: "registrationNumber")
        mutDict["profilePic"] =  aDecoder.decodeObject(forKey: "profilePic")
        mutDict["address"] =  aDecoder.decodeObject(forKey: "address")
        mutDict["city"] =  aDecoder.decodeObject(forKey: "city")
        mutDict["province"] =  aDecoder.decodeObject(forKey: "province")
        mutDict["driverSelfieImgPath"] =  aDecoder.decodeObject(forKey: "driverSelfieImgPath")
        
        mutDict["driverLicenseImgPath"] =  aDecoder.decodeObject(forKey: "driverLicenseImgPath")
        
        mutDict["driverProofOfResidenceImgPath"] =  aDecoder.decodeObject(forKey: "driverProofOfResidenceImgPath")
        
        mutDict["vehicleRegisterationImgPath"] =  aDecoder.decodeObject(forKey: "vehicleRegisterationImgPath")
        
        mutDict["vehicleFrontSideImgPath"] =  aDecoder.decodeObject(forKey: "vehicleFrontSideImgPath")

        mutDict["vehicleBackSideImgPath"] =  aDecoder.decodeObject(forKey: "vehicleBackSideImgPath")
        
        mutDict["vehicleTypeId"] =  aDecoder.decodeInteger(forKey:"vehicleTypeId")
        mutDict["todayEarnings"] =  aDecoder.decodeDouble(forKey: "todayEarnings")
        mutDict["rating"] =  aDecoder.decodeDouble(forKey: "rating")
        
        mutDict["vehicleColorId"] =  aDecoder.decodeInteger(forKey: "vehicleColorId")
        mutDict["vehicleBrandId"] =  aDecoder.decodeInteger(forKey: "vehicleBrandId")
        mutDict["provinceId"] =  aDecoder.decodeInteger(forKey: "provinceId")


        
        self.init(model: mutDict)
    }
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(name, forKey: "name")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(vehicleBrand, forKey: "vehicleBrand")
        aCoder.encode(vehicleColor, forKey: "vehicleColor")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(registrationNumber, forKey: "registrationNumber")
        aCoder.encode(profilePic, forKey: "profilePic")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(province, forKey: "province")
        aCoder.encode(vehicleTypeId, forKey: "vehicleTypeId")
        aCoder.encode(todayEarnings, forKey: "todayEarnings")
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(driverSelfieImgPath, forKey: "driverSelfieImgPath")
        aCoder.encode(driverLicenseImgPath, forKey: "driverLicenseImgPath")
        aCoder.encode(driverProofOfResidenceImgPath, forKey: "driverProofOfResidenceImgPath")
        aCoder.encode(vehicleRegisterationImgPath, forKey: "vehicleRegisterationImgPath")
        aCoder.encode(vehicleFrontSideImgPath, forKey: "vehicleFrontSideImgPath")
        aCoder.encode(vehicleBackSideImgPath, forKey: "vehicleBackSideImgPath")
        
        aCoder.encode(provinceId, forKey: "provinceId")
        aCoder.encode(vehicleBrandId, forKey: "vehicleBrandId")
        aCoder.encode(vehicleColorId, forKey: "vehicleColorId")
    }
}
