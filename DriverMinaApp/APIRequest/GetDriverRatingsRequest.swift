//
//  GetDriverRatingsRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 07/04/21.
//

import UIKit

class GetDriverRatingsRequest: NSObject {
    
    static let shared  = GetDriverRatingsRequest()
    
    func GetRatingData(requestParams:[String:Any],_ loader: Bool, completion: @escaping (_ object: [DriverRatings]?, _ status : Bool, _ totalRating:Double) -> Void) {

        let sessionManager = SessionManager()
        let url = "BaseURL".GetDriverRatingsAPI
    
        var urlString = String()
        urlString  =  String(format: "%@?driverId=%d",url,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)

        AlamofireRequest.shared.PostBodyFrom(urlString:urlString, isLoader : loader,param : nil ,loaderMessage : "",auth:sessionManager.getUserDetails().accessToken, isNetwork: true) { (data,error) in
            
            if error == nil
                {
                if let status = data?["status"] as? Bool{
                    if status == true{
                        var DoctorData : [DriverRatings] = []
                        
                        if let dataValue = data?["data"] as? NSDictionary {
                            if let Result = dataValue["reviewList"] as? [Any]{
                    
                            for i in 0..<Result.count{
                                var response = Dictionary<String, Any>()
                                    response = Result[i] as! [String : Any]
                                    response["userRating"] = dataValue["userRating"]
                                let Doctor : DriverRatings = DriverRatings.init(model:response)
                                    DoctorData.append(Doctor)
                                }
                                completion(DoctorData,true,dataValue["userRating"] as? Double ?? 5.0)
                        }
                        }
                }
                else{
                    completion(nil,false,5.0)
                }
            }
        }
    }
}
}

class DriverRatings: NSObject {
    
     var rating = 0.0
     var userName = ""
     var commentedAt = ""
     var review = ""
     var userRating = 0.0


    init(model: [String : Any]) {
        
        if let id = model["userRating"] as? Double{
           self.userRating = Double(id)
        }
        
         if let id = model["rating"] as? Double{
            self.rating = Double(id)
         }
        if let id = model["userName"] as? String{
            self.userName = id
        }
        if let id = model["commentedAt"] as? String{
            self.commentedAt = id
        }
        if let id = model["review"] as? String{
            self.review = id
        }
        
    }
}


