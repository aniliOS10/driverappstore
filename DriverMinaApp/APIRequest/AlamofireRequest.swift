
 // AlamofireRequest.swift
 // DriverMinaApp


import UIKit
import Alamofire
import Reachability
class AlamofireRequest: NSObject {
    
    var reachability: Reachability?
    static let shared = AlamofireRequest()
    var StatusCodeValue = 0

    
    func PutBodyForRawData(urlString : String,parameters : Parameters ,authToken : String?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success: [String : AnyObject]?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            let urL = URL(string: urlString)
            if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
                let jsonString = String(data: data, encoding: .utf8) {
                
                var request = URLRequest(url: urL!)
                request.httpMethod = HTTPMethod.put.rawValue
                
                if authToken != nil{
                    let bearer : String = "Bearer \(authToken!)"
                    request.addValue(bearer, forHTTPHeaderField: "Authorization")
                }
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonString.data(using: .utf8)
                request.timeoutInterval = 20

                AF.request(request)
                    .responseJSON { response in
                        
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                        }
                
                        self.SessionEnd(code: self.StatusCodeValue)
                        
                        switch response.result
                        {
                        case .failure(let error):
                            Indicator.shared.stopAnimating()
                            completion(nil , error)
                            
                        case .success(let value):
        
                            if let responseDictionary = value as? [String: AnyObject] {
                                
                                print(responseDictionary)
                                Indicator.shared.stopAnimating()
                                completion(responseDictionary, nil)
                            }
                            else{
                                Indicator.shared.stopAnimating()
                                completion(nil , response.error)
                            }
                            
                        }
                }
            }
            else{
                Indicator.shared.stopAnimating()
            }
        }
        else
        {
            MsgAlert()
        }
        
    }
    
    
    func PostBodyForRawData(urlString : String,parameters : Parameters ,authToken : String?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success: Data?,_ error : Error?) -> Void) {
        
        if InterNetConnection(){

            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
        
        let urL = URL(string: urlString)
        if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
            let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            
            if authToken != nil && authToken != ""{
                let bearer : String = "Bearer \(authToken!)"
                request.addValue(bearer, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonString.data(using: .utf8)
            request.timeoutInterval = 20

            AF.request(request)
                .responseJSON { response in
                    
                    if let statusCode = response.response?.statusCode
                    {
                        self.StatusCodeValue = statusCode
                    }
                    
                    self.SessionEnd(code: self.StatusCodeValue)
                    Indicator.shared.stopAnimating()

                    switch response.result
                    {
                                    case .failure(let error):
                                        Indicator.shared.stopAnimating()
                                        completion(nil , error)
                                        
                                    case .success(let value):
                    
                                        if let responseDictionary = value as? [String: AnyObject] {
                                            print(responseDictionary)
                                            
                                            Indicator.shared.stopAnimating()
                                            completion(response.data, nil)

                                        }
                                        else{
                                            Indicator.shared.stopAnimating()
                                            completion(nil , response.error)
                                        }
                                    }
            }
        }
        else{
            Indicator.shared.stopAnimating()
        }
    }
    else
        {
            MsgAlert()
        }
    
    }
    
    
    
    func PostBodyForRaw(urlString : String,parameters : Parameters ,authToken : String?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success:[String : AnyObject]?,_ error : Error?) -> Void) {
        
        if InterNetConnection(){

            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
        
        let urL = URL(string: urlString)
        if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
            let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            
            if authToken != nil && authToken != ""{
                let bearer : String = "Bearer \(authToken!)"
                request.addValue(bearer, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonString.data(using: .utf8)
            request.timeoutInterval = 20

            AF.request(request)
                .responseJSON { response in
                    
                    if let statusCode = response.response?.statusCode
                    {
                        self.StatusCodeValue = statusCode
                    }
                    
                    self.SessionEnd(code: self.StatusCodeValue)
                    Indicator.shared.stopAnimating()

                    switch response.result
                    {
                                    case .failure(let error):
                                        Indicator.shared.stopAnimating()
                                        completion(nil , error)
                                        
                                    case .success(let value):
                    
                                        if let responseDictionary = value as? [String: AnyObject] {
                                            print(responseDictionary)
                                            
                                            Indicator.shared.stopAnimating()
                                            completion(responseDictionary, nil)

                                        }
                                        else{
                                            Indicator.shared.stopAnimating()
                                            completion(nil , response.error)
                                        }
                                    }
            }
        }
        else{
            Indicator.shared.stopAnimating()
        }
    }
    else
        {
            MsgAlert()
        }
    
    }
    
    
    
    func LoginPostBodyForRawData(urlString : String,parameters : Parameters ,authToken : String?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success:[String : AnyObject]?,_ error : Error?) -> Void) {
        
        if InterNetConnection(){

            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
        
        let urL = URL(string: urlString)
        if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
            let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            
            if authToken != nil && authToken != ""{
                let bearer : String = "Bearer \(authToken!)"
                request.addValue(bearer, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonString.data(using: .utf8)
            request.timeoutInterval = 20

            
            AF.request(request)
                .responseJSON { response in
                    
                    if let statusCode = response.response?.statusCode
                    {
                        self.StatusCodeValue = statusCode
                    }
                    
                    self.SessionEnd(code: self.StatusCodeValue)

                    switch response.result
                    {
                                    case .failure(let error):
                                        Indicator.shared.stopAnimating()
                                        completion(nil , error)
                                        
                                    case .success(let value):
                    
                                        if let responseDictionary = value as? [String: AnyObject] {
                                            Indicator.shared.stopAnimating()
                                            completion(responseDictionary, nil)
                                        }
                                        else{
                                            Indicator.shared.stopAnimating()
                                            completion(nil , response.error)
                                        }
                                    }
            }
        }
        else{
            Indicator.shared.stopAnimating()
        }
    }
    else
        {
            MsgAlert()
        }
    
    }
    

    func deleredBodyForRawData(urlString : String,parameters : Parameters ,authToken : String?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success: [String : AnyObject]?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
    
            let urL = URL(string: urlString)
            if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
                let jsonString = String(data: data, encoding: .utf8) {
                
                var request = URLRequest(url: urL!)
                request.httpMethod = HTTPMethod.delete.rawValue
            
                if authToken != nil{
                    
                    let bearer : String = "Bearer \(authToken!)"
                    request.addValue(bearer, forHTTPHeaderField: "Authorization")
                    
                }
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonString.data(using: .utf8)
                request.timeoutInterval = 20

                AF.request(request)
                    .responseJSON { response in
                        
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                        }
                        
                        self.SessionEnd(code: self.StatusCodeValue)
                        
                        switch response.result
                        {
                                case .failure(let error):
                                Indicator.shared.stopAnimating()
                                completion(nil , error)
                                            
                                case .success(let value):
                        
                                if let responseDictionary = value as? [String: AnyObject] {
                                                
                                    print(responseDictionary)
                                    Indicator.shared.stopAnimating()
                                    completion(responseDictionary, nil)
                                }
                                else{
                                    Indicator.shared.stopAnimating()
                                    completion(nil , response.error)
                        }
                    }
                }
            }
            else{
                Indicator.shared.stopAnimating()
            }
        }
        else
        {
            MsgAlert()
        }
    }
    
   
    func ImageUploadWithMultipartFormData(urlString : String,parameters : Parameters ,authToken : String?,imageData : UIImage,isLoader : Bool, loaderMessage : String,filename:String, completion: @escaping (_ success: [String : AnyObject]?,_ error : Error?) -> Void) {
       
        if InterNetConnection() {

            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            

        let urL = URL(string: urlString)
                   let bearer : String = "Bearer \(authToken ?? "")"
        
        let headers: HTTPHeaders = [
               "Authorization": bearer,
               "Content-type": "multipart/form-data"
           ]
             
            AF.upload(
               multipartFormData: { multipartFormData in
                  let image = imageData.pngData() as NSData?

                        if image != nil{
                            let data : Data = image! as Data
                            multipartFormData.append(data, withName: "imgFile", fileName: filename, mimeType:"image/jpeg")
                }
           },
               to: urL!, method: .post , headers: headers)
        
               .response { response in
                
                print("Response Code:---->",response.response?.statusCode as Any)
                
                if let statusCode = response.response?.statusCode
                    {
                      self.StatusCodeValue = statusCode
                    }
                                 
                    self.SessionEnd(code: self.StatusCodeValue)
                    Indicator.shared.stopAnimating()
                
               if let data = response.data {
                   let json = String(data: data, encoding: String.Encoding.utf8)
                   print("Failure Response: \(json ?? "")")
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with:data, options: [])
                        print(jsonResponse)
                        completion(jsonResponse as? [String : AnyObject] , nil)
                        
                    } catch let parsingError {
                        print("Error", parsingError)
                        completion(nil , parsingError)
                }
            }
        }
    }
}
    
    func getBodyFrom(urlString : String ,isLoader : Bool,param : Parameters? ,loaderMessage : String,auth: String?, completion: @escaping (_ success: Any?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            if let url = URL(string: urlString) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.get.rawValue
                
                if auth != nil{
                    let bearer : String = "Bearer \(auth!)"
                        urlRequest.addValue(bearer, forHTTPHeaderField: "Authorization")
                        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                AF.request(urlRequest)
                    .responseJSON { response in
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                        }
                        self.SessionEnd(code: self.StatusCodeValue)
                        
                       switch response.result
                                             {
                                       case .failure(let error):
                                           Indicator.shared.stopAnimating()
                                           completion(nil , error)
                                           
                                       case .success(let value):
                       
                                           if let responseDictionary = value as? [String: AnyObject] {
                                               print(responseDictionary)
                                               Indicator.shared.stopAnimating()
                                               completion(response.data as Any, nil)
                                           }
                                           else{
                                               Indicator.shared.stopAnimating()
                                               completion(nil , response.error)
                                           }
                       }
                    }
                }
            }
            else
            {
                MsgAlert()
            }
    }
    
    
    
    func getBodyRowData(urlString : String ,isLoader : Bool,parameters : Parameters? ,loaderMessage : String,auth: String?, completion: @escaping (_ success: AnyObject?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {

            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            let urL = URL(string: urlString)
                if let data = try? JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted),
                let jsonString = String(data: data, encoding: .utf8) {
                
                var request = URLRequest(url: urL!)
                request.httpMethod = HTTPMethod.post.rawValue
                
                
                if auth != nil{
                    
                    let bearer : String = "Bearer \(auth!)"
                    
                    request.addValue(bearer, forHTTPHeaderField: "Authorization")
                   
                }
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonString.data(using: .utf8)
                request.timeoutInterval = 20

                AF.request(request)
                    .responseJSON { response in
                        
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                        }
                        
                        self.SessionEnd(code: self.StatusCodeValue)
                        
                        switch response.result
                        {
                                        case .failure(let error):
                                            Indicator.shared.stopAnimating()
                                            completion(nil , error)
                                            
                                        case .success(let value):
                        
                                            if let responseDictionary = value as? [String: AnyObject] {
                                                
                                                print(responseDictionary)
                                                Indicator.shared.stopAnimating()
                                             completion(responseDictionary as AnyObject, nil)
                                            }
                                            else{
                                                Indicator.shared.stopAnimating()
                                                completion(nil , response.error)
                        }
                    }
                }
            }
            else{
                Indicator.shared.stopAnimating()
            }
        }
            else
            {
                MsgAlert()
            }
    }
    
    
    func PostBodyFrom(urlString : String ,isLoader : Bool,param : Parameters? ,loaderMessage : String,auth: String?, isNetwork: Bool, completion: @escaping (_ success: AnyObject?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {
       
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            if let url = URL(string: urlString) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                
                if auth != nil{
                    
                      let bearer : String = "Bearer \(auth!)"
                      urlRequest.addValue(bearer, forHTTPHeaderField: "Authorization")
                      urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                urlRequest.timeoutInterval = 40

                AF.request(urlRequest)
                    .responseJSON { response in
                        
                    if let statusCode = response.response?.statusCode
                    {
                        self.StatusCodeValue = statusCode
                    }
                        self.SessionEnd(code: self.StatusCodeValue)
                        switch response.result
                       {
                                       case .failure(let error):
                                           Indicator.shared.stopAnimating()
                                           completion(nil , error)
                                           
                                       case .success(let value):
                       
                                           if let responseDictionary = value as? [String: AnyObject] {
                                                Indicator.shared.stopAnimating()
                                                completion(responseDictionary as AnyObject, nil)
                                           }
                                           else{
                                               Indicator.shared.stopAnimating()
                                               completion(nil , response.error)
                        }
                    }
                    if isLoader{
                        Indicator.shared.stopAnimating()
                    }
                }
            }
        }
        else
        {
            if isNetwork {
                MsgAlert()
            }
        }
    }
    
    
    
    func getBodyFromDict(urlString : String ,isLoader : Bool,param : Parameters? ,loaderMessage : String,auth: String?, isNetwork: Bool, completion: @escaping (_ success: AnyObject?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {
       
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            if let url = URL(string: urlString) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.get.rawValue
                
                if auth != nil{
                    
                      let bearer : String = "Bearer \(auth!)"
                      urlRequest.addValue(bearer, forHTTPHeaderField: "Authorization")
                      urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                }
            
                
                AF.request(urlRequest)
                    .responseJSON { response in
                        
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                        }
                        
                        self.SessionEnd(code: self.StatusCodeValue)
        
                        switch response.result

                       {
                            case .failure(let error):
                                Indicator.shared.stopAnimating()
                            completion(nil , error)
                                           
                            case .success(let value):
                       
                                if let responseDictionary = value as? [String: AnyObject] {
                                    Indicator.shared.stopAnimating()
                                    completion(responseDictionary as AnyObject, nil)
                                }
                                else{
                                    Indicator.shared.stopAnimating()
                                    completion(nil , response.error)
                        }
                    }
                        
                        if isLoader{
                            Indicator.shared.stopAnimating()
                        }
                }
            }
        }
        else
        {
            if isNetwork {
                MsgAlert()
            }
        }
    }
    
    // MARK: On Line Data Send API
    func PostBodyFrom_onLine(urlString : String ,isLoader : Bool,param : Parameters? ,loaderMessage : String,auth: String?, isNetwork: Bool,_ alertView:Bool, completion: @escaping (_ success: AnyObject?,_ error : Error?) -> Void) {
        
            if let url = URL(string: urlString) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.post.rawValue

                if auth != nil{
                    
                      let bearer : String = "Bearer \(auth!)"
                      urlRequest.addValue(bearer, forHTTPHeaderField: "Authorization")
                      urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                urlRequest.timeoutInterval = 40
                AF.request(urlRequest)
                    .responseJSON { response in
                        
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                            
                            if alertView {
                                self.SessionEnd(code: self.StatusCodeValue)
                            }
                        }
                        switch response.result

                       {
                                       case .failure(let error):
                                           completion(nil , error)
                                           
                                       case .success(let value):
                       
                                           if let responseDictionary = value as? [String: AnyObject] {
                                            completion(responseDictionary as AnyObject, nil)
                                           }
                                           else{
                                               completion(nil , response.error)
                        }
                    }
                        
                    if isLoader{
                       Indicator.shared.stopAnimating()
                    }
                }
            }
        }
        
    
    
    // MARK: On Line Location Send API

    func PostBodyForRawData_OnLine(urlString : String,parameters : Parameters ,authToken : String?,isLoader : Bool, loaderMessage : String, completion: @escaping (_ success:[String : AnyObject]?,_ error : Error?) -> Void) {
        
        let urL = URL(string: urlString)
        if let data = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted),
            let jsonString = String(data: data, encoding: .utf8) {
            
            var request = URLRequest(url: urL!)
            request.httpMethod = HTTPMethod.post.rawValue
            
            if authToken != nil && authToken != ""{
                let bearer : String = "Bearer \(authToken!)"
                request.addValue(bearer, forHTTPHeaderField: "Authorization")
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonString.data(using: .utf8)
            request.timeoutInterval = 40

            AF.request(request)
                .responseJSON { response in
                    
                    if let statusCode = response.response?.statusCode
                    {
                        self.StatusCodeValue = statusCode
                    }
                
                    switch response.result
                    {
                                    case .failure(let error):
                                        completion(nil , error)
                                        
                                    case .success(let value):
                    
                                        if let responseDictionary = value as? [String: AnyObject] {
                                            
                                            completion(responseDictionary, nil)

                                        }
                                        else{
                                            completion(nil , response.error)
                                        }
                }
            }
        }
        else{
            Indicator.shared.stopAnimating()
        }
    }
  

    func DeleteBodyFrom(urlString : String ,isLoader : Bool,param : Parameters? ,loaderMessage : String,auth: String?, completion: @escaping (_ success: AnyObject?,_ error : Error?) -> Void) {
        
        if InterNetConnection() {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            if let url = URL(string: urlString) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.delete.rawValue
                
                if auth != nil{
                    
                    let bearer : String = "Bearer \(auth!)"
                    urlRequest.addValue(bearer, forHTTPHeaderField: "Authorization")
                    urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                AF.request(urlRequest)
                    .responseJSON { response in
                        
                        if let statusCode = response.response?.statusCode
                        {
                            self.StatusCodeValue = statusCode
                        }
                        self.SessionEnd(code: self.StatusCodeValue)
                        
                        if isLoader{
                            Indicator.shared.stopAnimating()
                        }
                        
                        
                        switch response.result

                               {
                                case .failure(let error):
                                    Indicator.shared.stopAnimating()
                                    completion(nil , error)
                                    
                                case .success(let value):
                
                                    if let responseDictionary = value as? [String: AnyObject] {
                                        
                                        print(responseDictionary)
                                        Indicator.shared.stopAnimating()
                                        
                                    completion(responseDictionary as AnyObject, nil)
                                    }
                                else{
                                    Indicator.shared.stopAnimating()
                                    completion(nil , response.error)
                        }
                    }
                }
            }
        }
        else
        {
            MsgAlert()
        }
    }
    
    
    func InterNetConnection()->Bool  {
        reachability = try? Reachability()
        if reachability?.connection != .unavailable {
            return true
        }
        else
        {
            return false
        }
    }
    
    func MsgAlert()
    {
        let alert = UIAlertController(title: "No Internet Connection", message: "Internet not available, Cross check your internet connectivity and try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func SessionEnd(code:Int)
    {
        if code == 401
        {
            NotificationCenter.default.post(name: Notification.Name("HomeTimer_Invalidate"), object: nil)
            
            NotificationCenter.default.post(name: Notification.Name("Open_Requset_Timer_Invalidate"), object: nil)

            Indicator.shared.stopAnimating()
            UIViewControllerX().SessionExpireAlter()
            return
        }
    }
}


