//
//  DeliveryStatsDetailsRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 19/03/21.
//

import UIKit

class DeliveryStatusDetailsRequest: NSObject {
    
    static let shared = DeliveryStatusDetailsRequest()

    func deliveryStatusData(requestParams : [String:Any], _ isLoader:Bool, completion: @escaping (_ object: [String:Any]?, _ status : Bool, _ mess: String, _ strAny: String) -> Void) {
    
        let sessionManager = SessionManager()

        let url = "BaseURL".DeliveryStatsDetails
            print("URL - ",url)
    
        AlamofireRequest.shared.PostBodyForRaw(urlString:url, parameters:requestParams, authToken: sessionManager.getUserDetails().accessToken, isLoader:isLoader, loaderMessage: "") { (data,error) in
                
                if error == nil{
                    if let status = data?["status"] as? Bool{
                        print(data as Any)
                        var strMessage = ""
                        strMessage = data?["message"] as? String ?? ""
                        
                        if status {
                            if var Result = data?["data"] as? [String : Any]{
                                completion(nil,true,strMessage,"")
                            }
                        }
                        else{
                            completion(nil,false,strMessage,"")
                        }
                    }
                    else{
                    completion(nil,false,"There was an error connecting to the server.try again","")
                    }
                }
                else{
                    completion(nil,false,"There was an error connecting to the server.try again","")
            }
        }
    }
}


class DeliveryVerifyPINRequest: NSObject {
    
    static let shared = DeliveryVerifyPINRequest()

    func deliveryVerifyPINRequestData(requestParams : [String:Any], _ isLoader:Bool, completion: @escaping (_ object: [String:Any]?, _ status : Bool, _ mess: String, _ strAny: String) -> Void) {
    
        let sessionManager = SessionManager()

        let url = "BaseURL".VerifyPINDetails
        print("URL - ",url)
    
        AlamofireRequest.shared.PostBodyForRaw(urlString:url, parameters:requestParams, authToken: sessionManager.getUserDetails().accessToken, isLoader:isLoader, loaderMessage: "") { (data,error) in
                
                
                if error == nil{
                    if let status = data?["status"] as? Bool{
                        print(data as Any)
                        var strMessage = ""
                        strMessage = data?["message"] as? String ?? ""
                        
                        if status {
                           completion(nil,true,strMessage,"")
                        }
                        else{
                            completion(nil,false,strMessage,"")
                        }
                    }
                    else{
                        completion(nil,false,"There was an error connecting to the server.try again","")
                    }
                }
                else{
                    completion(nil,false,"There was an error connecting to the server.try again","")
                }
            }
        }
}
