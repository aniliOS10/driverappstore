//
//  GetParcelDetailsRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 15/03/21.
//

import UIKit

class GetParcelDetailsRequest: NSObject{
    
    static let shared = GetParcelDetailsRequest()
    
    func getParcelDetailsRequestData(requestId:Int,_ loader:Bool,completion: @escaping (_ object:GetParcelDetails?, _ status:Bool) -> Void) {
        
        let sessionManager = SessionManager()
        var url = "BaseURL".DriverGetRequestDetails_ForAccept
        url = String(format: "%@?requestId=%d",url,requestId)
        print("URL - ",url)
        
        AlamofireRequest.shared.getBodyFrom(urlString: url, isLoader:loader, param: nil, loaderMessage: "Parcel Details", auth:sessionManager.getUserDetails().accessToken) { (obj, error) in
        
            guard let _data: Data = obj as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(GetParcelDetails.self, from: _data)
                
                if let statusData = model.status {
                    if statusData {
                        completion(model,statusData)
                    }
                    else{
                        completion(model,statusData)
                    }
                }
                else{
                    MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                    completion(nil, false)
                }
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
        }
    }
}
  
}


struct GetParcelDetails: Codable {
    var status: Bool?
    var data: GetParcelDetailsClass?
    var message: String?
    var code: Int?
}

// MARK: - DataClass
struct GetParcelDetailsClass: Codable {
    var uovm: Uovm?
    var listDeliveryDetails: [ListDeliveryDetail]?
}

// MARK: - ListDeliveryDetail
struct ListDeliveryDetail: Codable {
    var driverID, requestID, statusID: Int?
    var requestStatus: String?
    var driverLat, driverLong: Double?
    var statusUpdateDate, statusUpdateTime, deliveryDateTimeUTC: String?

    enum CodingKeys: String, CodingKey {
        case driverID = "driverId"
        case requestID = "requestId"
        case statusID = "statusId"
        case requestStatus, driverLat, driverLong, statusUpdateDate, statusUpdateTime, deliveryDateTimeUTC
    }
}

// MARK: - Uovm
struct Uovm: Codable {
    var orderID, requestID: Int?
    var sourceAddress, sourceLat, sourceLong, destinationAddress: String?
    var destinationLat, destinationLong, deliveryType: String?
    var price: Int?
    var duration, distance, senderName, senderPhoneNo: String?
    var receiverName, receiverEmail: String?
    var dialCode: Int?
    var receiverMobileNumber, parcelName, parcelNotes, parcelImgBefore: String?
    var parcelImgAfter, orderCreatedDate, orderCreatedTime, orderStatus: String?
    var lastRequestStatusID: Int?
    var lastRequestStatus: String?

    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case requestID = "requestId"
        case sourceAddress, sourceLat, sourceLong, destinationAddress, destinationLat, destinationLong, deliveryType, price, duration, distance, senderName, senderPhoneNo, receiverName, receiverEmail, dialCode, receiverMobileNumber, parcelName, parcelNotes, parcelImgBefore, parcelImgAfter, orderCreatedDate, orderCreatedTime, orderStatus
        case lastRequestStatusID = "lastRequestStatusId"
        case lastRequestStatus
    }
}


