//
//  JobProgressRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit

class JobProgressRequest: NSObject {
    
    static let shared  = JobProgressRequest()
    
    func GetJobProgressData(requestParams:[String:Any],_ loader: Bool, completion: @escaping (_ object: [GetJobProgressModel]?, _ status : Bool) -> Void) {

        let sessionManager = SessionManager()
        let url = "BaseURL".GetWorkInProgressAPI
    
        var urlString = String()
        urlString  =  String(format: "%@?driverId=%d",url,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)

        AlamofireRequest.shared.getBodyFromDict(urlString:urlString,isLoader:loader, param:nil,loaderMessage: "Job In Progress", auth:sessionManager.getUserDetails().accessToken, isNetwork:true) { (data,error) in
            
            if error == nil
                {
                if let status = data?["status"] as? Bool{
                    if status == true{
                        var DoctorData : [GetJobProgressModel] = []
                        if let Result = data?["data"] as? [Any]{
                            print(Result)
                            for Doctors in Result{
                                let Doctor : GetJobProgressModel = GetJobProgressModel.init(model: Doctors as! [String : Any])
                                DoctorData.append(Doctor)
                            }
                            completion(DoctorData,true)
                    }
                }
                else{
                      completion(nil,false)
                }
            }
        }
    }
}
}

class GetJobProgressModel: NSObject {
    
     var requestId = 0
     var distance = ""
     var duration = ""
     var notifyDate = ""
     var notifyTime = ""
     var receiverAddress = ""
     var senderAddress = ""
     var status = ""
     var deliveryType = ""
     var deliveryPrice = 0
     var deliveryTypeId = 1
     var orderId = 0
     var receiverLat = 0.0
     var receiverLong = 0.0
     var senderLat = 0.0
     var senderLong = 0.0
     var receiverPhoneNumber = ""

    init(model: [String : Any]) {
        
        if let id = model["senderLat"] as? Double{
            self.senderLat = Double(id)
        }
        if let id = model["senderLong"] as? Double{
            self.senderLong = Double(id)
        }
        if let id = model["receiverLong"] as? Double{
            self.receiverLong = id
        }
        if let id = model["receiverLat"] as? Double{
            self.receiverLat = id
        }
        
        if let deliveryPriceData = model["receiverPhoneNumber"] as? String{
            self.receiverPhoneNumber = deliveryPriceData
        }
        
        if let deliveryPriceData = model["deliveryPrice"] as? Int{
            self.deliveryPrice = deliveryPriceData
        }
        if let deliveryTypeId = model["deliveryTypeId"] as? Int{
            self.deliveryTypeId = deliveryTypeId
        }
        if let id = model["orderId"] as? Int{
            self.orderId = id
        }
        if let id = model["requestId"] as? Int{
            self.requestId = id
        }
        if let id = model["deliveryType"] as? String{
            self.deliveryType = id
        }
        if let patientId = model["distance"] as? String{
            self.distance = patientId
        }
        if let doctorId = model["duration"] as? String{
            self.duration = doctorId
        }
        if let sessionId = model["orderDate"] as? String
        {
            self.notifyDate = sessionId
        }
        if let token = model["orderTime"] as? String{
            self.notifyTime = token
        }
        if let expireTime = model["senderAddress"] as? String{
            self.senderAddress = expireTime
        }
        if let createdAt = model["status"] as? String{
            self.status = createdAt
        }
        if let createdAt = model["receiverAddress"] as? String{
            self.receiverAddress = createdAt
        }
    }
}

