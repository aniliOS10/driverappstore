//
//  VehicleTypesRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 17/02/21.
//

import UIKit

class VehicleTypesRequest: NSObject {

    static let shared = VehicleTypesRequest()
    
    func getVehicleTypesData(requestParams : [String:Any],_ loader: Bool, completion: @escaping (_ object: VehicleTypesObject?, _ status : Bool) -> Void) {
        
        let url = "BaseURL".GetVehicleTypesURL
        print("URL - ",url)
        
        AlamofireRequest.shared.getBodyFrom(urlString: url, isLoader:false, param: nil, loaderMessage: "", auth:nil) { (obj, error) in
        
            guard let _data: Data = obj as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(VehicleTypesObject.self, from: _data)
                completion(model, true)
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
        }
    }
}
}



// MARK: - VehicleTypesObject
struct VehicleTypesObject: Codable {
    let status: Bool?
    let message: String?
    let data: VehicleListClass
    let code: Int?
}

// MARK: - DataClass
struct VehicleListClass: Codable {
    let list: [ListVehicleData]
}

// MARK: - List
struct ListVehicleData: Codable {
    let vehicleTypeID: Int?
    let vehicleType: String?

    enum CodingKeys: String, CodingKey {
        case vehicleTypeID = "vehicleTypeId"
        case vehicleType
    }
}
