//
//  SignInPhoneRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 01/03/21.
//

import UIKit

class SignInPhoneRequest: NSObject {

    static let shared = SignInPhoneRequest()
    
    func signInPhoneData(requestParams : [String:Any], completion: @escaping (_ object: PhoneObject?, _ status : Bool, _ mess : String) -> Void) {
        
        let url = "BaseURL".ResendPhoneURL
        print("URL - ",url)
        
        AlamofireRequest.shared.PostBodyForRawData(urlString: url, parameters: requestParams, authToken: nil, isLoader: true, loaderMessage: "SignUp") { (obj,error) in
        
            guard let _data: Data = obj else {
                completion(nil,false,"There was an error connecting to the server.try again")
                return
            }
            do {
                let model = try JSONDecoder().decode(PhoneObject.self, from: _data)
                
                var strMessage = ""
                strMessage = model.message
                
                var status = Bool()
                status = model.status
                
                completion(model,status,strMessage)
            }
            catch(let error) {
                
                print("Could not decode Response, error \(error)")
                completion(nil,false,"There was an error connecting to the server.try again")
      }
    }
  }
}



// MARK: - PhoneObject
struct PhoneObject: Codable {
    let status: Bool
    let message: String
    let data: phoneDataClass
    let code: Int
}

// MARK: - DataClass
struct phoneDataClass: Codable {
}






class SignInVerifyPhoneRequest: NSObject {

    static let shared = SignInVerifyPhoneRequest()
    
    func verifyPhoneData(requestParams : [String:Any], completion: @escaping (_ object: PhoneObject?, _ status : Bool, _ mess : String) -> Void) {
        
        let url = "BaseURL".VerifyPhoneURL
        print("URL - ",url)
        
        AlamofireRequest.shared.PostBodyForRawData(urlString: url, parameters: requestParams, authToken: nil, isLoader: true, loaderMessage: "") { (obj,error) in
        
            guard let _data: Data = obj else {
                completion(nil,false,"There was an error connecting to the server.try again")
                return
            }
            do {
                let model = try JSONDecoder().decode(PhoneObject.self, from: _data)
                
                var strMessage = ""
                strMessage = model.message
                
                var status = Bool()
                status = model.status
                
                completion(model,status,strMessage)
            }
            catch(let error) {
                
                print("Could not decode Response, error \(error)")
                completion(nil,false,"There was an error connecting to the server.try again")
      }
    }
  }
}
