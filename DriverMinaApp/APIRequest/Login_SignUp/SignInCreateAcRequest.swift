//
//  SignInCreateAcRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 01/03/21.
//

import UIKit
import JWTDecode

class SignInCreateAcRequest: NSObject {

    static let shared = SignInCreateAcRequest()
    
    func signInCreateData(requestParams : [String:Any], completion: @escaping (_ object: CreateData?, _ status : Bool, _ mess : String, _ AuthToken : String) -> Void) {
        
        let url = "BaseURL".SignInPersonalDetailsURL
        print("URL - ",url)
        
        AlamofireRequest.shared.PostBodyForRawData(urlString: url, parameters: requestParams, authToken: nil, isLoader: true, loaderMessage: "SignUp") { (obj,error) in

            guard let _data: Data = obj else {
                completion(nil,false,"There was an error connecting to the server.try again","")
                return
            }
            do {
                let model = try JSONDecoder().decode(CreateData.self, from: _data)
                
                var strMessage = ""
                strMessage = model.message
                
                var status = Bool()
                status = model.status
                
                var strAuthToken = ""
                strAuthToken = model.data
                
                do {
                    let jsonData = try decode(jwt: strAuthToken)
                    let claim = jsonData.claim(name: "UserId")
                    if let UserId = claim.integer {
                        print("USERID in jwt was \(UserId)")
                        UserDefaults.standard.set(UserId, forKey: "USER_ID")
                        completion(model,status,strMessage,strAuthToken)
                    }
                    else{
                        completion(nil,false,"There was an error connecting to the server.try again","")
                    }
                } catch {
                    print(error)
                    UserDefaults.standard.set(0, forKey: "USER_ID")
                    completion(nil,false,"There was an error connecting to the server.try again","")
                }
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                completion(nil,false,"There was an error connecting to the server.try again","")
      }
    }
  }
}

// MARK: - Welcome
struct CreateData: Codable {
    let status: Bool
    let data, message: String
    let code: Int
}
