//
//  ForgotPasswordRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 01/03/21.
//

import UIKit

class ForgotPasswordRequest: NSObject {
    
    static let shared = ForgotPasswordRequest()
    
    func forgotData(requestParams : [String:Any], completion: @escaping (_ object: LoginData?, _ status : Bool, _ mess: String, _ str: String) -> Void) {
        
        let url = "BaseURL".ForgotPasswordURL
        print("URL - ",url)
        
        AlamofireRequest.shared.PostBodyForRawData(urlString: url, parameters: requestParams, authToken: nil, isLoader: true, loaderMessage: "") { (obj,error) in
        
            guard let _data: Data = obj else {
                completion(nil,false,"There was an error connecting to the server.try again","")
                return
            }

            do {
                let model = try JSONDecoder().decode(LoginData.self, from: _data)
                
                var strMessage = ""
                strMessage = model.message ?? ""
                
                var status = Bool()
                status = model.status ?? false
                
                completion(model,status,strMessage,"")

            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                completion(nil,false,"There was an error connecting to the server.try again","")

        }
    }
}
}


class ResetDriverPasswordRequest: NSObject {
    
    static let shared = ResetDriverPasswordRequest()
    
    func resetData(requestParams : [String:Any], completion: @escaping (_ object: [String:Any]?, _ status : Bool, _ mess: String, _ str: String) -> Void) {
        
        let url = "BaseURL".ResetDriverPasswordURL
        print("URL - ",url)
        
        AlamofireRequest.shared.PostBodyForRaw(urlString:url, parameters:requestParams, authToken: nil, isLoader:true, loaderMessage: "") { (data,error) in
            
            
            if error == nil{
                if let status = data?["status"] as? Bool{
                    print(data as Any)
                    var strMessage = ""
                    strMessage = data?["message"] as? String ?? ""
                    
                    if status {
                       completion(nil,true,strMessage,"")
                    }
                    else{
                        completion(nil,false,strMessage,"")
                    }
                }
                else{
                completion(nil,false,"There was an error connecting to the server.try again","")
                }
            }
            else{
                completion(nil,false,"There was an error connecting to the server.try again","")

            }
        }
}
}
