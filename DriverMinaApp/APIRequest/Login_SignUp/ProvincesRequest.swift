//
//  ProvincesList.swift
//  DriverMinaApp
//
//  Created by Apple on 17/02/21.
//

import UIKit
import Foundation

class ProvincesListAPI: NSObject {

    static let shared = ProvincesListAPI()
    
    func getProvincesListData(requestParams : [String:Any],_ loader: Bool, completion: @escaping (_ object: ProvincesListObject?, _ status : Bool) -> Void) {
        
        let url = "BaseURL".GetAllProvincesURL
        print("URL - ",url)
        
        AlamofireRequest.shared.getBodyFrom(urlString: url, isLoader:loader, param: nil, loaderMessage: "", auth:nil) { (obj, error) in
        
            guard let _data: Data = obj as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(ProvincesListObject.self, from: _data)
                completion(model, true)
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
      }
    }
  }
}

// MARK: - ProvincesListObject
struct ProvincesListObject: Codable {
    let status: Bool
    let message: String
    let data: DataClass
    let code: Int
}

// MARK: - DataClass
struct DataClass: Codable {
    let list: [List]
}

// MARK: - List
struct List: Codable {
    let provinceID: Int
    let name: String

    enum CodingKeys: String, CodingKey {
        case provinceID = "provinceId"
        case name
    }
}

