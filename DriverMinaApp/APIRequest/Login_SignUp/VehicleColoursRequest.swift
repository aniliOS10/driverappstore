//
//  VehicleColoursRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 17/02/21.
//

import UIKit

class VehicleColoursRequest: NSObject {
    
    static let shared = VehicleColoursRequest()
    
    func getVehicleColoursData(requestParams : [String:Any],_ loader: Bool, completion: @escaping (_ object: ColoursDataObject?, _ status : Bool) -> Void) {
        
        let url = "BaseURL".GetVehicleColoursURL
        print("URL - ",url)
        
        AlamofireRequest.shared.getBodyFrom(urlString: url, isLoader:true, param: nil, loaderMessage: "", auth:nil) { (obj, error) in
        
            guard let _data: Data = obj as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(ColoursDataObject.self, from: _data)
                completion(model, true)
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
        }
    }
}
}


// MARK: - ColoursDataObject
struct ColoursDataObject: Codable {
    let status: Bool?
    let message: String?
    let data: ColoursDataList?
    let code: Int?
}

// MARK: - DataClass
struct ColoursDataList: Codable {
    let list: [ListColours]?
}

// MARK: - List
struct ListColours: Codable {
    let vehicleColorID: Int?
    let vehicleColorName: String?
    let vehicleColorCode: String?


    enum CodingKeys: String, CodingKey {
        case vehicleColorID = "vehicleColorId"
        case vehicleColorName
        case vehicleColorCode
    }
}
