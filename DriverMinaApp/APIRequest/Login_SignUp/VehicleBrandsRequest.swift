//
//  VehicleBrandsRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 17/02/21.
//

import UIKit

class VehicleBrandsRequest: NSObject {
    
    static let shared = VehicleBrandsRequest()
    
    func getVehicleBrandsData(requestParams : [String:Any],_ loader: Bool, completion: @escaping (_ object: VehicleBrandObject?, _ status : Bool) -> Void) {
        
        let url = "BaseURL".GetVehicleBrandsURL
        print("URL - ",url)
        
        AlamofireRequest.shared.getBodyFrom(urlString: url, isLoader:true, param: nil, loaderMessage: "", auth:nil) { (obj, error) in
        
            guard let _data: Data = obj as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(VehicleBrandObject.self, from: _data)
                completion(model, true)
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
        }
    }
}
}




// MARK: - Welcome
struct VehicleBrandObject: Codable {
    let status: Bool?
    let message: String?
    let carBrands: CarBrands?
    let scooterBrands: ScooterBrands?
    let bakkieBrands: BakkieBrands?
    let code: Int?
}

// MARK: - BakkieBrands
struct BakkieBrands: Codable {
    let bakkieBrands: [Brand]?
}

// MARK: - Brand
struct Brand: Codable {
    let brandID: Int?
    let brandName: String?
    let vehicleTypeID: Int?

    enum CodingKeys: String, CodingKey {
        case brandID = "brandId"
        case brandName
        case vehicleTypeID = "vehicleTypeId"
    }
}

// MARK: - CarBrands
struct CarBrands: Codable {
    let carBrands: [Brand]?
}

// MARK: - ScooterBrands
struct ScooterBrands: Codable {
    let scooterBrands: [Brand]?
}
