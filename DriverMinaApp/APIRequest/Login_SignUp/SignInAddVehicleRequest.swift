//
//  SignInAddVehicleRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 19/02/21.
//

import UIKit

class SignInAddVehicleRequest: NSObject {

    static let shared = SignInPersonalRequest()
    
    func signInPersonalData(requestParams : [String:Any], completion: @escaping (_ object: ProvincesListObject?, _ status : Bool) -> Void) {
        
        let url = "BaseURL".SignInAddVehicleDetailsURL
        print("URL - ",url)
        
        AlamofireRequest.shared.getBodyFrom(urlString: url, isLoader:false, param: nil, loaderMessage: "", auth:nil) { (obj, error) in
        
            guard let _data: Data = obj as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(ProvincesListObject.self, from: _data)
                completion(model, true)
            }
            catch(let error) {
                print("Could not decode Response, error \(error)")
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
      }
    }
  }
}
