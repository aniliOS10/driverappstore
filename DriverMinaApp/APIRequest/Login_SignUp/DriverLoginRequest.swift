//
//  DriverLoginRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 19/02/21.
//

import UIKit
import JWTDecode

class DriverLoginRequest: NSObject {

    static let shared = DriverLoginRequest()

    func loginData(requestParams : [String:Any], completion: @escaping (_ object: UserModel?, _ status : Bool, _ mess: String, _ str: String) -> Void) {
    
            let url = "BaseURL".DriverLoginURL
            print("URL - ",url)
    
            AlamofireRequest.shared.LoginPostBodyForRawData(urlString: url, parameters: requestParams, authToken: nil, isLoader: true, loaderMessage: "SignIn") { (data,error) in
                
                
                if error == nil{
                    if let status = data?["status"] as? Bool{
                        print(data as Any)
                        var strMessage = ""
                        strMessage = data?["message"] as? String ?? ""
                        
                        if status {
                            if var Result = data?["data"] as? [String : Any]{
                                do {
                                    let jsonData = try decode(jwt: Result["accessToken"] as! String)
                                    let claim = jsonData.claim(name: "UserId")
                                    if let UserId = claim.integer {
                                        print("USERID in jwt was \(UserId)")
                                        Result["userID"] = UserId
                                        
                                        let userModel : UserModel = UserModel.init(model: Result as [String : Any])
                                        
                                        completion(userModel,status,strMessage,"")
                                    }
                                } catch {
                                    completion(nil,false,"There was an error connecting to the server.try again","")
                                }
                            }
                        }
                        else{
                            completion(nil,false,strMessage,"")
                        }
                    }
                    else{
                    completion(nil,false,"There was an error connecting to the server.try again","")
                    }
                }
                else{
                    completion(nil,false,"There was an error connecting to the server.try again","")

                }
            }
        }
    
    
    
    
}

//// MARK: - Login Object
struct LoginData: Codable {
    let status: Bool?
    let message: String?
    let data: UserDataLogin?
    let code: Int?
}

// MARK: - DataClass
struct UserDataLogin: Codable {
    let firstName, lastName, dialCode, phoneNumber: String?
    let isPhoneNoVerified: Bool?
    let email: String?
    let isEmailConfirmed: Bool?
    let userTypeID: Int?
    let screenId: Int?
    let accessToken: String?
    let idProof, selfie: String?

    enum CodingKeys: String, CodingKey {
        case firstName, lastName, dialCode, phoneNumber, isPhoneNoVerified, email, isEmailConfirmed,screenId
        case userTypeID = "userTypeId"
        case accessToken, idProof, selfie
    }
}
