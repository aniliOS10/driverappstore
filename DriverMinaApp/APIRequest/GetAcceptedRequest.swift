//
//  GetAcceptedRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 10/03/21.
//

import UIKit

class GetAcceptedRequest: NSObject {

    static let shared  = GetAcceptedRequest()
    
    func GetAcceptedRequestData(requestParams:[String:Any],_ loader: Bool, completion: @escaping (_ object: [GetAcceptedModel]?, _ status : Bool) -> Void) {

        let sessionManager = SessionManager()
        let url = "BaseURL".GetAcceptedRequestsAll
    
        var urlString = String()
        urlString  =  String(format: "%@?driverId=%d",url,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)

        AlamofireRequest.shared.PostBodyFrom(urlString:urlString,isLoader:loader, param:nil,loaderMessage: "Accepted Jobs", auth:sessionManager.getUserDetails().accessToken,isNetwork:true) { (data,error) in
            
            if error == nil
                {
                if let status = data?["status"] as? Bool{
                    if status == true{
                        var DoctorData : [GetAcceptedModel] = []
                        if let Result = data?["data"] as? [Any]{
                            print(Result)
                            for Doctors in Result{
                                let Doctor : GetAcceptedModel = GetAcceptedModel.init(model: Doctors as! [String : Any])
                                DoctorData.append(Doctor)
                            }
                            completion(DoctorData,true)
                    }
                }
                else{
                      completion(nil,false)
                }
            }
      }
    }
  }
}

class GetAcceptedModel: NSObject {
    
     var requestId = 0
     var distance = ""
     var duration = ""
     var notifyDate = ""
     var notifyTime = ""
     var receiverAddress = ""
     var senderAddress = ""
     var status = ""
     var deliveryType = ""
     var deliveryPrice = 0
     var deliveryTypeId = 1
     var orderId = 0
     var receiverLat = ""
     var receiverLong = ""
     var senderLat = 0.0
     var senderLong = 0.0
     var receiverPhoneNumber = ""
     var senderPhoneNumber = ""

    init(model: [String : Any]) {
        
        if let id = model["senderLat"] as? Double{
            self.senderLat = Double(id)
        }
        if let id = model["senderLong"] as? Double{
            self.senderLong = Double(id)
        }
        if let id = model["receiverLong"] as? String{
            self.receiverLong = id
        }
        if let id = model["receiverLat"] as? String{
            self.receiverLat = id
        }
        
        if let deliveryPriceData = model["receiverPhoneNumber"] as? String{
            self.receiverPhoneNumber = deliveryPriceData
        }
        
        if let deliveryPriceData = model["senderPhoneNumber"] as? String{
            self.senderPhoneNumber = deliveryPriceData
        }
        
        if let deliveryPriceData = model["deliveryPrice"] as? Int{
            self.deliveryPrice = deliveryPriceData
        }
        if let deliveryTypeId = model["deliveryTypeId"] as? Int{
            self.deliveryTypeId = deliveryTypeId
        }
        if let id = model["orderId"] as? Int{
            self.orderId = id
        }
        if let id = model["requestId"] as? Int{
            self.requestId = id
        }
        if let id = model["deliveryType"] as? String{
            self.deliveryType = id
        }
        if let patientId = model["distance"] as? String{
            self.distance = patientId
        }
        if let doctorId = model["duration"] as? String{
            self.duration = doctorId
        }
        if let sessionId = model["orderDate"] as? String
        {
            self.notifyDate = sessionId
        }
        if let token = model["orderTime"] as? String{
            self.notifyTime = token
        }
        if let expireTime = model["senderAddress"] as? String{
            self.senderAddress = expireTime
        }
        if let createdAt = model["status"] as? String{
            self.status = createdAt
        }
        if let createdAt = model["receiverAddress"] as? String{
            self.receiverAddress = createdAt
        }
        
    }
}
