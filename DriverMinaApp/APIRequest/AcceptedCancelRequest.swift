//
//  AcceptedCancelRequest.swift
//  DriverMinaApp
//
//  Created by Apple on 18/03/21.
//

import UIKit

class AcceptedCancelRequest: NSObject{
    
    static let shared  = AcceptedCancelRequest()
    
    func GetAcceptedCancelData(_ orderId:Int,_ loader: Bool, completion: @escaping (_ object: Any?, _ status : Bool) -> Void) {

        let sessionManager = SessionManager()
        let url = "BaseURL".OrderCancelByDriver
    
        var urlString = String()
        urlString  =  String(format: "%@?orderId=%d&driverId=%d",url,orderId,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)

        AlamofireRequest.shared.getBodyFrom(urlString:urlString,isLoader:true, param:nil,loaderMessage: "Cancel", auth:sessionManager.getUserDetails().accessToken) { (data,error) in
            
            guard let _data: Data = data as? Data else {
                MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
                completion(nil, false)
                return
            }
            
            if let json : [String:Any] = self.nsdataToJSON(data: _data){
                if let status = json["status"] as? Bool{
                    if status == true{
                        completion(nil, status)
                    }
                    else{
                        completion(nil, status)
            }
        }
    }
    else {
        MainController().showErrorMSg(text: "There was an error connecting to the server.try again")
        }
  }
}
    
    func nsdataToJSON(data: Data) -> [String:Any]? {
        do {
            if let json : [String:Any] = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
             return json
            }
    } catch {
        print("parsing error")
    }
        return nil
    }
}


