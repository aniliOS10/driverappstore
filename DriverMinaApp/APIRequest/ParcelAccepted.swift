//
//  ParcelAccepted.swift
//  DriverMinaApp
//
//  Created by Apple on 12/03/21.
//

import UIKit

class ParcelAccepted: NSObject
{
    
    static let shared  = ParcelAccepted()
    
    func ParcelAcceptedData(requestParams:[String:Any],_ loader: Bool, completion: @escaping (_ object: [String : AnyObject]?, _ status : Bool,_ message:String) -> Void) {
        
        let url = "BaseURL".DriverResponseToNoticeAll
        var userModel = UserModel()
        let sessionManager = SessionManager()
        userModel = sessionManager.getUserDetails()

        AlamofireRequest.shared.LoginPostBodyForRawData(urlString: url, parameters: requestParams, authToken: userModel.accessToken, isLoader: loader, loaderMessage: "") { (data,error) in

        if error == nil{
            
            if let status = data?["status"] as? Bool{
                if status {
                    completion(data,status,data?["message"] as? String ?? "There was an error connecting to the server.try again")
                }
                else{
                    completion(data,status,data?["message"] as? String ?? "There was an error connecting to the server.try again")
                }
            }
        }
    }
}
}

