
import Foundation
import UIKit

struct GlobalConstants {
    static let deviceType: String = "iOS"
    static let language: String = "English"
    static let Messagetitle: String = "2MaMina"
    static let MalePlaceHolding: String = "placeholder_Male"
    static let ImageBaseURL = "https://maminaapi.azurewebsites.net/"

}

enum LoginType : String{
    case Email = "email"
    case Google = "google"
    case Facebook = "facebook"
}

enum UserType : String{
    case Admin = "admin"
    case SubAdmin = "subadmin"
}

extension String{
    
    static let LocalBaseURL = "https://maminaapi.azurewebsites.net/api/"
    static let LiveBaseURL = "https://maminaapi.azurewebsites.net/api/"
    
    
    var path: String{
        return .LocalBaseURL
    }
    
    var deviceToken : String{
        if let token = UserDefaults.standard.object(forKey: "DToken"){
            return token as! String
        }
        return "rvcxZZgghjkjskjskajskjas"
    }
    
        
    // MARK: - Vehicles API
    
    var ForgotPasswordURL : String {
        return "Path".path + "Auth/ForgotPassword"
    }
    var ResendPhoneURL:String {
        return "Path".path + "Driver/ResendDriverPhoneOTP"
    }
    var VerifyPhoneURL:String {
        return "Path".path + "Driver/VerifyDriverPhone"
    }
    
    var GetAllProvincesURL: String{
        return "Path".path + "Vehicles/GetAllProvinces"
    }
    
    var GetVehicleColoursURL: String{
        return "Path".path + "Vehicles/GetAllVehicleColours"
    }
    
    var GetVehicleTypesURL: String{
        return "Path".path + "Vehicles/GetAllVehicleTypes"
    }
    var GetVehicleBrandsURL: String{
        return "Path".path + "Vehicles/GetAllVehicleBrands"
    }
    var DriverLoginURL:String {
        return "Path".path + "Driver/DriverLogin"
    }
    
    var LogOutURL:String {
        return "Path".path + "Driver/DriverLogout"
    }
    var SignInPersonalDetailsURL : String {
        return "Path".path + "Driver/DriverPersonalDetails"
    }
       
    var SignInAddDriverDocumentsURL : String {
        return "Path".path + "Driver/AddDriverDocuments"
    }
    
    var SignInAddVehicleDetailsURL : String {
        return "Path".path + "Vehicles/AddVehicleDetails"
    }
    
    var ResetDriverPasswordURL : String {
        return "Path".path + "Driver/ResetDriverPassword"
    }
    
    var AddDriverDocumentsURL : String {
        return "Path".path + "Driver/AddDriverDocuments"
    }
    
    var DriverOnlineURL : String {
        return "Path".path + "Driver/SaveNoticeForDriver"
    }
   
    var AddDriverWorkStatus : String {
        return "Path".path + "Driver/AddDriverWorkStatus"
    }
    
    var GetRequestNoticeForTopDrivers : String {
        return "Path".path + "Driver/GetRequestNoticeForTopDrivers"
    }
    
    var GetOpenRequestsAPI : String {
        return "Path".path + "Driver/GetOpenRequests"
    }

    var GetAcceptedRequestsAll : String {
        return "Path".path + "Driver/GetAcceptedRequests"
    }
    
    var GetWorkInProgressAPI : String {
        return "Path".path + "Driver/GetWorkInProgress"
    }
    
    var GetDriverRatingsAPI : String {
        return "Path".path + "Driver/GetDriverRatings"
    }
    
    var GetCompletedWorkAPI : String {
        return "Path".path + "Driver/GetCompletedWork"
    }

    var DriverResponseToNoticeAll : String {
        return "Path".path + "Driver/DriverResponseToNotice"
    }

    var DriverGetRequestDetails_ForAccept : String {
        return "Path".path + "Request/GetRequestDetails"
    }

    var OrderCancelByDriver : String {
        return "Path".path + "Orders/OrderCancelByDriver"
    }
    
    var GetDriverEarningsURL : String {
        return "Path".path + "Orders/GetDriverEarnings"
    }

    var DeliveryStatsDetails : String {
        return "Path".path + "Request/DeliveryDetails"
    }
    
    var VerifyPINDetails : String {
        return "Path".path + "Driver/VerifyPIN"
    }
       
    var AddBankInfoURL : String {
        return "Path".path + "Driver/AddUpdateDriverBankDetails"
    }
    
    var GetDriverBankDetailsURL : String {
        return "Path".path + "Driver/GetDriverBankDetails"

    }
    
    var NotificationURL : String {
        return "Path".path + "Notification/GetDriverNotifications"

    }
    
    var DeleteNotificationURL: String {
        return "Path".path + "Notification/DeleteDriverNotification"

    }
    
    var GetDriverInfoURL : String {
        return "Path".path + "Driver/GetDriverInfo"

    }
    
    var UpdateDriverInfoURL : String {
        return "Path".path + "Admin/UpdateDriverInfo"
    }
        
    var UpdateVehicleDetailsURL : String {
        return "Path".path + "Vehicles/UpdateVehicleDetails"
    }
    
    var ChangePasswordURL : String {
        return "Path".path + "Driver/ChangePassword"
    }
}

