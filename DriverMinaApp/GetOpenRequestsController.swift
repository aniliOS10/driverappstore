//
//  GetOpenRequestsController.swift
//  DriverMinaApp
//
//  Created by Apple on 31/03/21.
//

import UIKit
import Foundation
import Starscream

class GetOpenRequestsController: UIViewController, WebSocketDelegate,StreamDelegate  {
   
    var socket: WebSocket!
    var openArray = [[String: Any]]()

    var noDuplicatesArray = [[String: Any]]()
    let sessionManager = SessionManager()
    var userModel = UserModel()
    let refreshControl = UIRefreshControl()

    var timerAPI = Timer()
    var isApiCall = false
    var isAddData = false
  
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var tableView_GetParcel: UITableView?
    @IBOutlet weak var viewNoData: UIView!

    @objc func RefreshScreen() {
       
        if AlamofireRequest.shared.InterNetConnection()
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
              //  self.newJobs(isLoader: false)
            }
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    
    func start_API_Timer(){
        

        timerAPI.invalidate()
        timerAPI = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(Data_API_Timer), userInfo: nil, repeats: true)
    }
    
    @objc func Data_API_Timer() {
       // newJobs(isLoader: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userModel = sessionManager.getUserDetails()

        topViewLayout()
        viewNoData.isHidden = false
        

        if AlamofireRequest.shared.InterNetConnection(){
       
          Indicator.shared.startAnimating(withMessage: "Getting", colorType: UIColor.white, colorText: UIColor.white)
        
            let request = URLRequest(url: URL(string: "wss://wsmamina.azurewebsites.net/GetOpenRequests")!)
            socket = WebSocket(request: request)
            socket.delegate = self
            socket.connect()
        }
        else{
            AlamofireRequest.shared.MsgAlert()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Open_Requset_Timer_Invalidate), name: NSNotification.Name(rawValue: "Open_Requset_Timer_Invalidate"), object: nil)

    }
    
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            print("websocket is connected:")
            socket.write(string:String(userModel.userID))
            print(convertTimeFormaterS(date: Date()))
            isApiCall = true

        case .disconnected(let reason, let code):
            print("websocket is disconnected: \(reason) with code: \(code)")
            Indicator.shared.stopAnimating()
        case .text(let string):
            print(convertTimeFormaterS(date: Date()))
            Indicator.shared.stopAnimating()
            
            string_To_Json(string)
            
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            Indicator.shared.stopAnimating()
            break
        case .error(let error):
            print(error!)
            Indicator.shared.stopAnimating()
            break
        }
    }
    
    
    func string_To_Json(_ str:String){
        
        let data = Data(str.utf8)
        do {
            if let arrayData = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                    if arrayData.count > 0 {
                        
                        if !self.isAddData {
                            
                         //   self.openArray.append(contentsOf: arrayData as! [[String : Any]])
                            noDuplicatesArray = self.noDuplicates(arrayData as! [[String : Any]])
                            
                            self.tableView_GetParcel?.reloadData()
                            self.viewNoData.isHidden = true
                            self.refreshControl.endRefreshing()
                            
                        }
                        else{
                            print("No")
                        }
                    }
                    else{
                        self.noDuplicatesArray.removeAll()
                        self.openArray.removeAll()
                        self.tableView_GetParcel?.reloadData()
                        self.viewNoData.isHidden = false
                        self.refreshControl.endRefreshing()
                    }
             }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    
    func noDuplicates(_ arrayOfDicts: [[String: Any]]) -> [[String: Any]] {
        var noDuplicates = [[String: Any]]()
        var usedNames = [String]()
        
        for dict in arrayOfDicts {
            var name = "0"
            var i = Int()
            i = dict["requestId"] as? Int ?? 0
            name = String(i)
            if !usedNames.contains(name) {
                noDuplicates.append(dict)
                usedNames.append(name)
            }
            else{
               // print("No Data")
            }
        }
        return noDuplicates
    }
    
    
    func topViewLayout(){
        if !GetOpenRequestsController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func Open_Requset_Timer_Invalidate(_ notification: NSNotification) {
        
        if self.isApiCall{
            socket.disconnect()
        }
    }

    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        timerAPI.invalidate()
        socket.disconnect()
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - TableViewDataSource Delegate
extension GetOpenRequestsController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noDuplicatesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "NewRequestTableViewCell", for: indexPath) as? NewRequestTableViewCell else {
            return NewRequestTableViewCell()
        }
        
        cellOf.selectionStyle = .none
        
        var dict = NSDictionary()
        if noDuplicatesArray.count > indexPath.row {
            dict = noDuplicatesArray[indexPath.row] as NSDictionary
            cellOf.lbe_Pickup.text = dict["senderAddress"] as? String
            cellOf.lbe_Delivery.text = dict["receiverAddress"] as? String
            cellOf.lbe_Distance.text = dict["distance"] as? String
            cellOf.lbe_Duration.text = dict["duration"] as? String
            cellOf.lbe_SerType.text = dict["deliveryType"] as? String
    
            if cellOf.lbe_SerType.text == "Normal"{
                cellOf.lbe_SerType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
            }
            else if cellOf.lbe_SerType.text == "Express"{
                cellOf.lbe_SerType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
            }
        }
        
        cellOf.btn_Details.tag = indexPath.row
        cellOf.btn_Details.addTarget(self, action: #selector(action_ParcelDetails), for: .touchUpInside)
        
        cellOf.btn_Accept.tag = indexPath.row
        cellOf.btn_Accept.addTarget(self, action: #selector(action_ParcelAccepted), for: .touchUpInside)
        
        cellOf.btn_Reject.tag = indexPath.row
        cellOf.btn_Reject.addTarget(self, action: #selector(action_ParcelReject), for: .touchUpInside)
        return cellOf
    }
    
    
    @objc func action_ParcelDetails(sender: UIButton){
        
    if noDuplicatesArray.count > sender.tag {
        var dict = NSDictionary()
            dict = noDuplicatesArray[sender.tag] as NSDictionary
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ParcelDetailsRequestController") as! ParcelDetailsRequestController
        vc.requestID = dict["requestId"] as? Int ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc func action_ParcelAccepted(sender: UIButton){
        self.isAddData = true
        userModel = sessionManager.getUserDetails()

        var dict = NSDictionary()
        if noDuplicatesArray.count > sender.tag {
            dict = noDuplicatesArray[sender.tag] as NSDictionary
            
        var Data: Dictionary<String, Any> = [:]
            Data["driverId"] = userModel.userID
            Data["requestId"] = dict["requestId"] as? Int
            Data["driverLat"] =  Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            Data["driverLong"] =  Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            Data["hasAccepted"] = true
            
            if self.noDuplicatesArray.count > 0 {
                if self.noDuplicatesArray.count > sender.tag {
                    self.noDuplicatesArray.remove(at: sender.tag)
                    self.tableView_GetParcel?.reloadData()
                }
            }
            
            parcelAccepted_API(Params:Data, index: sender.tag)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.isAddData = false
            }
        }
    }
    
    @objc func action_ParcelReject(sender: UIButton){
        
        userModel = sessionManager.getUserDetails()
        
        var dict = NSDictionary()
        if noDuplicatesArray.count > sender.tag {
            dict = noDuplicatesArray[sender.tag] as NSDictionary
            
        var Data: Dictionary<String, Any> = [:]
            Data["driverId"] = userModel.userID
            Data["requestId"] = dict["requestId"] as? Int
            Data["driverLat"] =  Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            Data["driverLong"] =  Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            Data["hasAccepted"] = false
            
            parcelAccepted_API(Params:Data, index: sender.tag)
        }
    }
}

    
//MARK: - TableViewDelegate Delegate
extension GetOpenRequestsController : UITableViewDelegate
{
    
    func dynamicFontSize(_ FontSize: CGFloat) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.size.width
        let calculatedFontSize = screenWidth / 375 * FontSize
        return calculatedFontSize
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label = UILabel ()
        label.frame = CGRect.init(x: 0, y: 0, width:width, height: CGFloat.greatestFiniteMagnitude)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        let style = NSMutableParagraphStyle()
            style.lineSpacing = 4
            style.alignment = .left
            label.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle: style])
           
            label.sizeToFit()
        
        let maxSize = CGSize(width:width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
    
        return CGFloat(linesRoundedUp * Int(charSize) + 25)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height_Text: CGFloat = 30
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        var fontNew = UIFont()
      
        fontNew = .RegularFont(Float(dynamicFontSize(14.0)))

        var dict = NSDictionary()
        if noDuplicatesArray.count > indexPath.row {
            dict = noDuplicatesArray[indexPath.row] as NSDictionary
            height_Text = heightForView(text:dict["senderAddress"] as? String ?? "", font: fontNew, width: screenWidth - 60)

            height_Text = heightForView(text:dict["receiverAddress"] as? String ?? "", font: fontNew, width: screenWidth - 72) + height_Text
        }
        return height_Text + 115
    }
}


//MARK: - API -
extension GetOpenRequestsController{


    func parcelAccepted_API(Params:[String: Any], index: Int){
        
        ParcelAccepted.shared.ParcelAcceptedData(requestParams: Params, true) { (obj,status,mess) in
            if status{
                let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                    
                    if self.noDuplicatesArray.count == 0 {
                        self.navigationController?.popViewController(animated: true)
                    }
                }))
                
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                
                if let code = obj?["code"] as? Int{
                    if code == 201{
                        
                            let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                                        if self.noDuplicatesArray.count == 0 {
                                        self.navigationController?.popViewController(animated: true)
                                                            }
                            }))
                        DispatchQueue.main.async {

                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                            let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                                if self.noDuplicatesArray.count == 0 {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }))
                        DispatchQueue.main.async {

                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else{
                        let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                            if self.noDuplicatesArray.count == 0 {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    func dataRemoveFrom_Array(_ requestID: Int){
        var acceptArrayRemove = [[String: Any]]()
        acceptArrayRemove = noDuplicatesArray
        
        for i in 0..<acceptArrayRemove.count
        {
            var dict = NSDictionary()
            dict = acceptArrayRemove[i] as NSDictionary
            if dict["requestId"] as! Int == requestID {
                if self.noDuplicatesArray.count > i {
                    self.noDuplicatesArray.remove(at: i)
                }
            }
        }
        
      //  noDuplicatesArray = self.noDuplicates(self.openArray)
        self.tableView_GetParcel?.reloadData()
        
      
    }
}


