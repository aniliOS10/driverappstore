//
//  NotiListController.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit

class NotiListController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet fileprivate var tableView : UITableView!
    
    var NotificationData : [NotificationModel] = []
    let sessionManager = SessionManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        
        CallNotificationAPIRequest(isLoader: true)
        
    }
    func topViewLayout(){
        if !NotiListController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func CallNotificationAPIRequest(isLoader:Bool)
    {
    
        var userModel = UserModel()
        userModel = sessionManager.getUserDetails()
        
        let  URL_String = String(format: "?driverId=%d", userModel.userID) 
    
        NotificationRequest(url: URL_String ,isLoader:isLoader)
    }
    
    
    @IBAction fileprivate func delete_Action(_ sender: Any) {
        let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete all notifications?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .destructive) {
            UIAlertAction in
            self.DeleteNotificationsAPI("", isLoader: true)

        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
}

extension NotiListController
{
    // MARK: - Get Patient Input Info API Request
    func NotificationRequest(url:String,isLoader:Bool){
        self.view.endEditing(true)
        
        NotificationAPIRequest.shared.Notification(requestParams:url, isLoader: isLoader) { (obj, message, status)  in
            
            if status == false
            {
                self.popMessageAlert(title: "", message: "You have no new notifications", RootView: false)
            }
            else
            {
                self.NotificationData = obj!
                if self.NotificationData.count == 0
                {
                    self.popMessageAlert(title: "", message: "You have no new notifications", RootView: false)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    func DeleteNotificationsAPI(_ id :String, isLoader:Bool){
        
        DeleteNotificationAPIRequest.shared.DeleteNotification(requestParams: [:], ID: id,isLoader:isLoader)  { ( message, status) in
            if status {
                if id == "" {
                    self.NotificationData.removeAll()
                    self.tableView.reloadData()
                    self.navigationController?.popViewController(animated: true)

                }
            }
            else
            {
                self.MessageAlert(title: "", message: message!)
            }
        }
    }
}


//MARK: - TableViewDataSource Delegate
extension NotiListController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOfProfile = tableView.dequeueReusableCell(withIdentifier: "NotiListTableViewCell", for: indexPath) as? NotiListTableViewCell else {
            return NotiListTableViewCell()
        }
        cellOfProfile.setCellData(Notification: self.NotificationData[indexPath.row])

        cellOfProfile.selectionStyle = .none
        return cellOfProfile
        
    }
}
//MARK: - TableViewDelegate Delegate
extension NotiListController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}


class NotificationAPIRequest: NSObject {
    
    static let shared = NotificationAPIRequest()
    
    func Notification(requestParams:String ,isLoader:Bool, completion: @escaping (_ object: [NotificationModel]?,_ message : String?, _ status : Bool) -> Void) {
        let sessionManager = SessionManager()

        var urlString = ""
            urlString = "BaseURL".NotificationURL + requestParams
        

        AlamofireRequest.shared.getBodyFromDict(urlString:urlString, isLoader: isLoader, param: [:], loaderMessage: "Getting", auth: sessionManager.getUserDetails().accessToken, isNetwork: true) { (data, error) in
            
            if error == nil{
                print(data as Any)
                
                if let status = data?["status"] as? Bool{
                    
                    var messageString : String = ""
                    if let msg = data?["message"] as? String{
                        messageString = msg
                    }
                    
                    if status == true{
                        
                        var NotificationData : [NotificationModel] = []
                            
                        if let NotificationList = data?["data"] as? [Any]{
                                
                                for Notifications in NotificationList{
                                    let Notification : NotificationModel = NotificationModel.init(model: Notifications as! [String : Any])
                                    NotificationData.append(Notification)
                                }
                                completion(NotificationData, messageString, true)
                            }
                    }
                    else{
                        completion(nil,messageString, false)
                    }
                }
                else
                {
                    
                    completion(nil, MainController().languageKey(key: "There was an error connecting to the server."), false)
                    
                }
                
            }else{
                completion(nil,MainController().languageKey(key: "There was an error connecting to the server.try again"), false)
            }
        }
    }
}

class NotificationModel: NSObject {
    
    var id: String = ""
    var createdOn: String = ""
    var text: String = ""
    var toUserId: String = ""
    var type = 0
    
    init(model: [String : Any]) {
        
        if let id = model["id"] as? String{
            self.id = id
        }
        if let createdOns = model["createdOn"] as? String{
            self.createdOn = createdOns
        }
        if let texts = model["text"] as? String{
            self.text = texts
        }
        if let toUserIds = model["toUserId"] as? String{
            self.toUserId = toUserIds
        }
        if let types = model["type"] as? Int{
            self.type = types
        }
    }
}

class DeleteNotificationAPIRequest: NSObject {
    
    static let shared = DeleteNotificationAPIRequest()
    
    func DeleteNotification(requestParams : [String:Any],ID:String,isLoader:Bool, completion: @escaping (_ message : String?, _ status : Bool) -> Void) {
    
        let sessionManager = SessionManager()

        var urlString = String()
        urlString = String(format:"%@?driverId=%d&notifId=%@","BaseURL".DeleteNotificationURL,sessionManager.getUserDetails().userID,ID)

        AlamofireRequest.shared.DeleteBodyFrom(urlString: urlString, isLoader: isLoader, param: requestParams, loaderMessage: "Deleting..", auth: sessionManager.getUserDetails().accessToken){ (data, error) in
            if error == nil{
                
                if let status = data?["status"] as? Bool{
    
                    var messageString : String = ""
                    if let msg = data?["message"] as? String{
                        messageString = msg
                    }
                    if status == true{
                        completion(messageString, true)
                    }
                    else{
                        completion(messageString, false)
                    }
                }
                else
                {
                    completion( MainController().languageKey(key: "There was an error connecting to the server."), false)
                }
                
            }else{
                completion(MainController().languageKey(key: "There was an error connecting to the server.try again"), false)
            }
        }
    }
}
