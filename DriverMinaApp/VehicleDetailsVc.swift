//
//  VehicleDetailsVc.swift
//  DriverMinaApp
//
//  Created by Apple on 22/04/21.
//

import UIKit
import SDWebImage
import Photos
import MobileCoreServices
import DropDown
import Alamofire
import Reachability
class VehicleDetailsVc: UIViewController {
    
    @IBOutlet weak var txt_VehicleType : UITextField!
    @IBOutlet weak var txt_VehicleName : UITextField!
    @IBOutlet weak var txt_VehicleColor : UITextField!
    @IBOutlet weak var txt_VehicleReg : UITextField!
    
    @IBOutlet weak var lbe_VehicleType : UILabel!
    @IBOutlet weak var lbe_VehicleName : UILabel!
    @IBOutlet weak var lbe_VehicleColor : UILabel!
    
    @IBOutlet weak var btn_VehicleType : UIButton!
    @IBOutlet weak var btn_VehicleName : UIButton!
    @IBOutlet weak var btn_VehicleColor : UIButton!
    
    @IBOutlet weak var btn_Car : UIButton!
    @IBOutlet weak var btn_Scooter : UIButton!
    @IBOutlet weak var btn_Bakkie : UIButton!
    @IBOutlet weak var btn_Submit : UIButton!
    @IBOutlet weak var btn_Front : UIButton!
    @IBOutlet weak var btn_Rear : UIButton!

    @IBOutlet weak var img_Front : UIImageView!
    @IBOutlet weak var img_Rear : UIImageView!

    @IBOutlet weak var view_Front : UIView!
    @IBOutlet weak var view_Rear : UIView!
    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    
    var VehicleArray = NSArray()

    var ColoursArray = [String]()
    var ColoursArrayID = [Int]()
    
    var CarBrandsArray = [String]()
    var CarBrandsArrayID = [Int]()

    var isFront = false
    var isRear = false
    
    var VehicleID_Selected = 1
    var int_ColoursID = 0
    var int_CarBrandID = 0
    var int_ServiceType = 0
    var int_UserId = 0
    var countAPI = 0
    var imageType = ""
    var str_AccessToken = ""
    var imagePicker = UIImagePickerController()
    var personalInfoModel = Personal_VehicleDetails()
    var sessionManager = SessionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        topViewLayout()
        CustomizationTextFeild()
        CustomizationButton()
    }
    

    func topViewLayout(){
        if !VehicleDetailsVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        dataShow_InUI()
    }
    
    
    func dataShow_InUI(){
        
        personalInfoModel = sessionManager.getPersonalVehicleDetails()
        if personalInfoModel.vehicleTypeId == 1 {
            scooter_VehicleName()
        }
        else if (personalInfoModel.vehicleTypeId == 2){
            car_VehicleName()
        }
        else{
            bakkie_VehicleName()
        }
        
        txt_VehicleName.text = personalInfoModel.vehicleBrand
        self.int_CarBrandID = personalInfoModel.vehicleBrandId
        txt_VehicleColor.text = personalInfoModel.vehicleColor
        self.int_ColoursID = personalInfoModel.vehicleColorId
        txt_VehicleReg.text = personalInfoModel.registrationNumber
        
        
        if let url = URL(string: GlobalConstants.ImageBaseURL + personalInfoModel.vehicleFrontSideImgPath) {
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                img_Front.image = UIImage(data: imageData) ?? UIImage()
                isFront = true
            }
        }
        
        if let url = URL(string: GlobalConstants.ImageBaseURL + personalInfoModel.vehicleBackSideImgPath) {
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                img_Rear.image = UIImage(data: imageData) ?? UIImage()
                isRear = true
            }
        }
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_VehicleType.delegate = self
        txt_VehicleName.delegate = self
        txt_VehicleColor.delegate = self
        txt_VehicleReg.delegate = self

        txt_VehicleType.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_VehicleName.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_VehicleColor.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_VehicleReg.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Submit.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Front.tag = 10001
        btn_Front.addTarget(self, action: #selector(action_Front), for: .touchUpInside)
        
        btn_Rear.tag = 10002
        btn_Rear.addTarget(self, action: #selector(action_Rear), for: .touchUpInside)
        
    }
    
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    @objc func action_Front(sender: UIButton){
        imageType = "Front"
        chooseImageFromGalleryAndCamera()
        
    }
    
    @objc func action_Rear(sender: UIButton){
        imageType = "Rear"
        chooseImageFromGalleryAndCamera()
    }
    
    //MARK:- Action
    @IBAction fileprivate func Next_Action(_ sender: Any) {
        self.view.endEditing(true)
        update_DataAddVehicleVc()
    }
    
    @IBAction func VehicleServiceType_Action(_ sender: Any) {
      //  setupServiceTypeDropDown()
    }
    
    @IBAction func VehicleColor_Action(_ sender: Any) {
         
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"BrandListController") as! BrandListController

        vc.modalPresentationStyle = .overFullScreen
        vc.VehicleID_Selected = 0
        vc.DataPassColors = { (starttime , endTime) in
            self.txt_VehicleColor.text = starttime
            self.int_ColoursID = endTime
        }

        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func VehicleName_Action(_ sender: Any) {

        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"BrandListController") as! BrandListController

        vc.modalPresentationStyle = .overFullScreen
        vc.VehicleID_Selected = self.VehicleID_Selected
        vc.DataPassBrand = { (starttime , endTime) in
            
            self.txt_VehicleName.text = starttime
            self.int_CarBrandID = endTime
            
        }

        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func bakkie_Action(_ sender: Any) {

        bakkie_VehicleName()

    }
    
    func bakkie_VehicleName() {
        btn_Bakkie.setTitleColor(UIColor.white, for: .normal)
        btn_Bakkie.titleLabel?.font =  .BoldFont(16)
        btn_Bakkie.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Scooter.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Scooter.titleLabel?.font =  .BoldFont(16)
        btn_Scooter.backgroundColor = UIColor.white
        
        btn_Car.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Car.titleLabel?.font =  .BoldFont(16)
        btn_Car.backgroundColor = UIColor.white
        
        lbe_VehicleName.text = "Bakkie Brand"
        lbe_VehicleColor.text = "Bakkie Color"
        VehicleID_Selected = 3
        self.CarBrandsArray.removeAll()
        self.CarBrandsArrayID.removeAll()
        self.txt_VehicleName.text = ""
        self.int_CarBrandID = 0
        self.txt_VehicleColor.text = ""
        self.int_ColoursID = 0
        self.txt_VehicleType.text = ""
        self.txt_VehicleReg.text = ""
    }
    
    @IBAction func car_Action(_ sender: Any) {

        car_VehicleName()
        
    }
    
    
    func  car_VehicleName(){
        btn_Car.setTitleColor(UIColor.white, for: .normal)
        btn_Car.titleLabel?.font =  .BoldFont(16)
        btn_Car.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Scooter.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Scooter.titleLabel?.font =  .BoldFont(16)
        btn_Scooter.backgroundColor = UIColor.white
        
        btn_Bakkie.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Bakkie.titleLabel?.font =  .BoldFont(16)
        btn_Bakkie.backgroundColor = UIColor.white
        
      
        lbe_VehicleName.text = "Car Brand"
        lbe_VehicleColor.text = "Car Color"
        VehicleID_Selected = 2
        self.CarBrandsArray.removeAll()
        self.CarBrandsArrayID.removeAll()
        self.txt_VehicleName.text = ""
        self.int_CarBrandID = 0
        self.txt_VehicleColor.text = ""
        self.int_ColoursID = 0
        self.txt_VehicleType.text = ""
        self.txt_VehicleReg.text = ""
    }
    
    @IBAction func scooter_Action(_ sender: Any) {
        
        scooter_VehicleName()

    }
    
    func scooter_VehicleName(){
        btn_Scooter.setTitleColor(UIColor.white, for: .normal)
        btn_Scooter.titleLabel?.font =  .BoldFont(16)
        btn_Scooter.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Car.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Car.titleLabel?.font =  .BoldFont(16)
        btn_Car.backgroundColor = UIColor.white
        
        btn_Bakkie.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Bakkie.titleLabel?.font =  .BoldFont(16)
        btn_Bakkie.backgroundColor = UIColor.white
        
        lbe_VehicleName.text = "Scooter Brand"
        lbe_VehicleColor.text = "Scooter Color"
        VehicleID_Selected = 1
        self.CarBrandsArray.removeAll()
        self.CarBrandsArrayID.removeAll()
        self.txt_VehicleName.text = ""
        self.int_CarBrandID = 0
        self.txt_VehicleColor.text = ""
        self.int_ColoursID = 0
        self.txt_VehicleType.text = ""
        self.txt_VehicleReg.text = ""
    }
    

    func update_DataAddVehicleVc(){
        
        if int_CarBrandID == 0 {
            MessageAlert(title:"",message: "Please select vehicle Brand")
            return
        }
        
        if int_ColoursID == 0 {
            MessageAlert(title:"",message: "Please select vehicle Colours")
            return
        }
        
        if txt_VehicleReg.text?.count == 0 {
            MessageAlert(title:"",message: "Please enter vehicle Registration number")
            return
        }
        
        if isFront == false {
            MessageAlert(title:"",message: "Please add Vehicle Front picture")
            return
        }
        
        if isRear == false {
            MessageAlert(title:"",message: "Please add Vehicle Rear picture")
            return
        }
        let sessionManager = SessionManager()

        var dict: Dictionary<String, AnyObject> = [:]
            dict["VehicleTypeId"] = VehicleID_Selected as AnyObject
            dict["BrandId"] = int_CarBrandID as AnyObject
            dict["VehicleColorId"] = int_ColoursID as AnyObject
            dict["RegisterationNumber"] = txt_VehicleReg.text as AnyObject
            dict["DriverId"] = sessionManager.getUserDetails().userID as AnyObject

        updateVehicle_MultipartFormData(parameters:dict,FrontSideImage : self.img_Front.image!,BackSideImage : self.img_Rear.image!,isLoader: true, loaderMessage: "Update"){ [self] (obj,er) in

            if obj?["status"] as? Bool ?? false {
                
                if let data = obj?["data"] as? [String:Any]{

                    var userModelLogin = Personal_VehicleDetails()
                    userModelLogin = sessionManager.getPersonalVehicleDetails()
                    
                    userModelLogin.registrationNumber = txt_VehicleReg.text
                    userModelLogin.vehicleFrontSideImgPath = data["frontSideImgPath"] as? String ?? ""
                    userModelLogin.vehicleBackSideImgPath = data["backSideImgPath"] as? String ?? ""

                    userModelLogin.vehicleColor = txt_VehicleColor.text
                    userModelLogin.vehicleColorId = self.int_ColoursID
                    userModelLogin.vehicleBrand = txt_VehicleName.text
                    userModelLogin.vehicleBrandId = self.int_CarBrandID
                    userModelLogin.vehicleTypeId = self.VehicleID_Selected
                    
                    self.sessionManager.createPersonal_VehicleDetails_Session(userModel:userModelLogin)
                    
                    self.alertViewSuccess(title: "Update", mess:"Vehicle details update successfully.")
                }
            }
            else{
                self.MessageAlert(title:"",message: obj?["message"] as? String ?? "There was an error connecting to the server.try again")
            }
        }
    }

    func alertViewSuccess(title:String,mess:String){
           let alert = UIAlertController(title: title, message:mess, preferredStyle: .alert)
           let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
            self.navigationController?.popViewController(animated: true)

           })
           alert.addAction(ok)
           DispatchQueue.main.async(execute: {
               self.present(alert, animated: true)
           })
       }

}
extension VehicleDetailsVc : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}

extension VehicleDetailsVc : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
                

    @objc func chooseImageFromGalleryAndCamera()
            {
                let alert = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
                
                alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{
                    (UIAlertAction)in
                    print("User click Approve button")
                    self.handelUploadCamera()
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{
                    (UIAlertAction)in
                    print("User click Edit button")
                    self.handelUploadTap()
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{
                    (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
                
            }

    @objc func handelUploadTap() {
                let photos = PHPhotoLibrary.authorizationStatus()
                if photos == .notDetermined {
                    PHPhotoLibrary.requestAuthorization({status in
                        if status == .authorized{
                    DispatchQueue.main.async {

                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                                
                                self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                                self.imagePicker.allowsEditing = true
                                
                                self.imagePicker.mediaTypes = [kUTTypeImage as String]
                                self.present(self.imagePicker, animated: true, completion: nil)
                        }
                            }
                        }
                        
                    })
                }
                else if photos == .authorized {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){

                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                        imagePicker.allowsEditing = true
                        
                        imagePicker.mediaTypes = [kUTTypeImage as String]
                        self.present(imagePicker, animated: true, completion: nil)
                        
                    }
                }
            }

    @objc func handelUploadCamera()  {
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                    imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                    imagePicker.mediaTypes = [kUTTypeImage as String]
                    imagePicker.allowsEditing = true
                    present(imagePicker, animated: true, completion: nil)
                }
            }
            // MARK: - Image Picker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController,
                                       didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
                
    if let editedImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage) {
                    self.dismiss(animated: true, completion: nil)
                    
                    if imageType == "Front" {
                        self.img_Front.image = editedImage
                        isFront = true
                    }
                    if imageType == "Rear" {
                        self.img_Rear.image = editedImage
                        isRear = true
                    }
                }
                else if let origionalImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) {
                    self.dismiss(animated: true, completion: nil)
                    if imageType == "Front" {
                        self.img_Front.image = origionalImage
                        isFront = true
                    }
                    if imageType == "Rear" {
                        self.img_Rear.image = origionalImage
                        isRear = true
                    }
                 }
                self.dismiss(animated: true, completion: nil)
            }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
                dismiss(animated: true, completion: nil)
       }
    }

extension VehicleDetailsVc {

    func updateVehicle_MultipartFormData(parameters : [String : AnyObject],FrontSideImage : UIImage,BackSideImage : UIImage,isLoader : Bool, loaderMessage : String, completion: @escaping ( _ success: [String : AnyObject]?, _ error : Error?) -> Void) {
    
        if AlamofireRequest.shared.InterNetConnection()
        {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            let sessionManager = SessionManager()

            let urlString = "BaseURL".UpdateVehicleDetailsURL

            let escapedAddress = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""

            let urL = URL(string: escapedAddress)

            let bearer : String = "Bearer \(sessionManager.getUserDetails().accessToken ?? "")"

            let headers: HTTPHeaders = [
                "Authorization": bearer,
                "Content-type": "multipart/form-data"
            ]
            
            AF.upload(
                multipartFormData: { multipartFormData in
                    let image1 = FrontSideImage.jpegData(compressionQuality: 0.8)!
                    let image2 = BackSideImage.jpegData(compressionQuality: 0.8)!
                    
                    for key in parameters.keys{
                        let name = String(key)
                        if let val = parameters[name] as? String{
                            multipartFormData.append(val.data(using: .utf8)!, withName: name)
                        }
                        if let new = parameters[name] as? Int {
                            let value = "\(new)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: name)
                        }
                    }

                    if image1 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        
                        name = name + ".jpeg"
                        
                        multipartFormData.append(image1, withName: "FrontSideImage", fileName: name, mimeType:"image/jpeg")
                    }
                    if image2 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        name = name + ".jpeg"

                        multipartFormData.append(image2, withName: "BackSideImage", fileName: name, mimeType:"image/jpeg")
                    }
                },
                to: urL!, method: .post , headers: headers)
                
                .response { response in
                    
                    debugPrint(response)
                    print("Response Code:---->",response.response?.statusCode as Any)
                    Indicator.shared.stopAnimating()
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        print("Response: \(json ?? "")")
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with:data, options: [])
                            print(jsonResponse)
                            completion(jsonResponse as? [String : AnyObject] , nil)
                            
                        } catch let parsingError {
                            print("Error", parsingError)
                            completion(nil , parsingError)
                        }
                    }
                }
        }
        else{
            AlamofireRequest.shared.MsgAlert()
        }
    }

}
