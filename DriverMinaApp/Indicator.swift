

import UIKit
import NVActivityIndicatorView
class Indicator: NSObject,NVActivityIndicatorViewable {
    
    var activityData = ActivityData()
    static let shared = Indicator()
    
    func startAnimating(withMessage : String, colorType : UIColor, colorText : UIColor) {
        
        var colorBg = UIColor()
            colorBg = UIColor(red: 0, green: 0, blue: 0, alpha: 0.68)
        
        activityData = ActivityData(size: CGSize(width: 74, height: 74),
                                    message: withMessage,
                                    messageFont: .SemiBoldFont(19),
                                    type: NVActivityIndicatorType.lineScalePulseOutRapid,
                                    color: UIColor.white,
                                    padding: 0,
                                    displayTimeThreshold: NVActivityIndicatorView.DEFAULT_BLOCKER_DISPLAY_TIME_THRESHOLD,
                                    minimumDisplayTime: NVActivityIndicatorView.DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME,
                                    backgroundColor: colorBg,
                                    textColor: UIColor.white)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData, nil)
        
    }
    
    func stopAnimating(){
        
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        
    }
}

