//
//  EarningReportsVc.swift
//  DriverMinaApp
//
//  Created by Apple on 06/04/21.
//

import UIKit
import Charts

class EarningReportsVc: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var lbe_TotalEarning: UILabel!
    @IBOutlet weak var lbe_DateToday: UILabel!
    @IBOutlet weak var lbe_TodayEarning: UILabel!
    @IBOutlet weak var barChart: BarChartView!

    var lineChartEntry = [ChartDataEntry]()
    var lineChartEntry2 = [ChartDataEntry]()
    var total = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        topViewLayout()
        getEarningReportsList(true)
        
        lbe_DateToday.text = String(format: "%@ (TODAY)", convertFormaterEMMM(date: Date()))
    }
    

    func topViewLayout(){
        if !EarningReportsVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getEarningReportsList(_ loader:Bool) {
        
        GetDriverEarnings.shared.GetDriverEarningsData(requestParams:[:], loader) { (obj,status) in
            DispatchQueue.main.async {
            if status{
               
                if obj?.count ?? 0 > 0 {
                    self.lbe_TodayEarning.text = String(format: "R %.2f",obj?[0].todayEarnings ?? 00)
                    
                    self.lbe_TotalEarning.text = String(format: "R %.2f", obj?[0].totalEarnings ?? 00)
        
                    for i in 0..<(obj?[0].lastWeekEarningsArray.count)!{
                        var dict = NSDictionary()
                        dict = obj?[0].lastWeekEarningsArray[i] as! NSDictionary
                        self.total.add(dict.value(forKey: "amount") as Any)
                    }
                    
                    DispatchQueue.main.async {
                       self.barChatDataShow()
                    }
                }
            }
        }
    }
}
    
    func barChatDataShow(){
        
    //  total = [150.0, 125.0, 135.2, 175.5, 190.5, 300.5, 185.5]

        var entries: [BarChartDataEntry] = []
        var isBool = false

        for i in 0..<total.count {
            let entry = BarChartDataEntry.init(x: Double(i), y: total[i] as! Double)
            entries.append(entry)
            if  total[i] as! Double > 0.0 {
                isBool = true
            }
        }
    
        let barChartDataSet = BarChartDataSet.init(entries: entries, label: nil)
        
        barChartDataSet.setColor(NSUIColor.init(hexString: "3DB24B"))
        barChartDataSet.valueTextColor = NSUIColor.init(hexString: "7F7F7F")
        barChartDataSet.valueFont = .BoldFont(13)
        barChartDataSet.drawIconsEnabled = false

        if !isBool {
            barChartDataSet.valueTextColor = UIColor.clear
        }
       
        self.barChart.data = BarChartData.init(dataSet: barChartDataSet)
    
        let xAxis = barChart.xAxis
        
        let cal = NSCalendar.current
        var date = cal.startOfDay(for: Date())

        var arrDates = [String]()

        for _ in 1 ... 7 {
            date = cal.date(byAdding: Calendar.Component.day, value: -1, to: date)!
            let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E"
            let dateString = dateFormatter.string(from: date)
            arrDates.append(dateString)
        }
        let formatter = XAxesValueFormatter()

        let daysArray: [String?] = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        if arrDates.count > 0 {
            formatter.daysArray = arrDates.reversed()
        }
        else{
            formatter.daysArray = daysArray
        }
        xAxis.valueFormatter = formatter
        xAxis.labelCount = 7
        setupAppearanceFor(chartView: barChart)
    }
    
    
    func setupAppearanceFor(chartView: BarChartView!) {
        
            chartView.delegate = self
            chartView.chartDescription?.enabled = false
            chartView.dragEnabled = false
            chartView.setScaleEnabled(true)
            chartView.pinchZoomEnabled = false
            chartView.drawBarShadowEnabled = false
            chartView.drawValueAboveBarEnabled = true
            chartView.setVisibleXRangeMaximum(7)
            chartView.drawGridBackgroundEnabled=false
            chartView.legend.enabled=false
            chartView.fitBars = true
            chartView.rightAxis.enabled = false
            chartView.leftAxis.enabled = false
            chartView.doubleTapToZoomEnabled = false
            chartView.isUserInteractionEnabled = false
            chartView.maxVisibleCount = 60
            
            let xAxis = chartView.xAxis
            xAxis.labelPosition = .bottom
            xAxis.labelTextColor = NSUIColor.init(hexString: "7F7F7F")
            xAxis.labelFont = .BoldFont(13)
            xAxis.labelHeight = 36
            xAxis.yOffset = 10
            xAxis.labelRotatedHeight = 20
            xAxis.granularity = 0
           
            xAxis.drawGridLinesEnabled = false
    }
}



extension EarningReportsVc: ChartViewDelegate
{
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight)
    {
        print("chartValueSelected : x = \(highlight.x)")
    }
    
    public func chartValueNothingSelected(_ chartView: ChartViewBase)
    {
        print("chartValueNothingSelected")
    }
}


class GetDriverEarnings: NSObject {
    
    static let shared  = GetDriverEarnings()
    
    func GetDriverEarningsData(requestParams:[String:Any],_ loader: Bool, completion: @escaping (_ object: [GetDriverEarningsModel]?, _ status : Bool) -> Void) {

        let sessionManager = SessionManager()
        let url = "BaseURL".GetDriverEarningsURL
    
        var urlString = String()
        urlString  =  String(format: "%@?driverId=%d",url,sessionManager.getUserDetails().userID)
        print("URL - ",urlString)

        AlamofireRequest.shared.getBodyFromDict(urlString:urlString,isLoader:loader, param:nil,loaderMessage: "Getting", auth:sessionManager.getUserDetails().accessToken, isNetwork:true) { (data,error) in
            
            if error == nil
                {
                if let status = data?["status"] as? Bool{
                    if status == true{
                        var DoctorData : [GetDriverEarningsModel] = []
                        if let Result = data?["data"] as? NSDictionary{
                            let Doctor : GetDriverEarningsModel = GetDriverEarningsModel.init(model: Result as! [String : Any])
                            DoctorData.append(Doctor)
                            completion(DoctorData,true)
                    }
                }
                else{
                      completion(nil,false)
                }
            }
        }
    }
}
}

class GetDriverEarningsModel: NSObject {
    
    var todayEarnings = 0.0
    var totalEarnings = 0.0
    var lastWeekEarningsArray = NSArray()

    init(model: [String : Any]) {
        
        if let id = model["todayEarnings"] as? Double{
            self.todayEarnings = Double(id)
        }
        if let id = model["totalEarnings"] as? Double{
            self.totalEarnings = Double(id)
        }
        if let arrayData = model["lastWeekEarnings"] as? NSArray{
            lastWeekEarningsArray = arrayData
        }
    }
}

public class XAxesValueFormatter: NSObject, IAxisValueFormatter {
    var daysArray: [String?] = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]

    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
    return daysArray[Int(value)] ?? ""
  }
}
