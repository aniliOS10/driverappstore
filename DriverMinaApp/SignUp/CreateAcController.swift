//
//  CreateAcController.swift
//  DriverMinaApp
//
//  Created by Apple on 03/02/21.
//

import UIKit
import DropDown


class CreateAcController: UIViewController{

    @IBOutlet weak var txt_Name : UITextField!
    @IBOutlet weak var txt_Last : UITextField!
    @IBOutlet weak var txt_Phone : UITextField!

    @IBOutlet weak var txt_Email : UITextField!
    @IBOutlet weak var txt_Password : UITextField!
    @IBOutlet weak var txt_ConfirmPass : UITextField!

    @IBOutlet weak var txt_City : UITextField!
    @IBOutlet weak var txt_Address : UITextField!
    
    @IBOutlet weak var lbe_Province : UILabel!
    @IBOutlet weak var lbe_DialCode : UILabel!

    
    @IBOutlet weak var btn_Next : UIButton!
    @IBOutlet weak var btn_Province : UIButton!

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    var str_DialCode = "27"
    var str_PhoneNo = ""
    var int_Province = 0
    
    var statesArray = [String]()
    var statesArrayID = [Int]()
    let dropStates = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        dataGetProvinces(false)
        txt_Phone.text =  str_PhoneNo
        lbe_DialCode.text = str_DialCode
        topViewLayout()
        CustomizationTextFeild()
        CustomizationButton()
        setDropsColor()
    }
    
    func setDropsColor() {
        DropDown.appearance().backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().cornerRadius = 10
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().textFont = .SemiBoldFont(16)
    }
    
    func topViewLayout(){
        if !CreateAcController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    func dataGetProvinces(_ loader:Bool){
        ProvincesListAPI.shared.getProvincesListData(requestParams: [:],loader) { (object,status)  in
            self.statesArray.removeAll()
            self.statesArrayID.removeAll()

            for i in 0..<(object?.data.list.count ?? 0)
            {
                let dict = object?.data.list[i]
                self.statesArray.append(dict?.name ?? "")
                self.statesArrayID.append(dict?.provinceID ?? 0)
            }
            self.setupIDTypeDropDown()
        }
    }
    
    
    //MARK:-  States Drills Api
    
    @IBAction func ID_Type_Action(_ sender: Any) {
        if statesArray.count > 0 {
            dropStates.show()
        }
        else{
            dataGetProvinces(true)
        }
    }
    
    func setupIDTypeDropDown() {
    
        dropStates.anchorView = btn_Province
        dropStates.bottomOffset = CGPoint(x: 0, y: btn_Province.bounds.height)
        dropStates.dataSource = self.statesArray
        dropStates.selectionAction = { [weak self] (index, item) in
            self!.lbe_Province.text = item
            self!.int_Province = self?.statesArrayID[index] ?? 0
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view .endEditing(true)
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_Name.delegate = self
        txt_Last.delegate = self
        txt_Phone.delegate = self
        txt_Email.delegate = self
        txt_Password.delegate = self
        txt_ConfirmPass.delegate = self
        txt_City.delegate = self
        txt_Address.delegate = self

        txt_Name.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Last.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Phone.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Email.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Password.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_ConfirmPass.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_City.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Address.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)

    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Next.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
    }
    
    
    @IBAction fileprivate func Next_Action(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let nameText = txt_Name.text ?? ""
        
        if nameText.isEmpty{
            MessageAlert(title:"",message: "Please enter First Name")
            return
        }
        
        let lastText = txt_Last.text ?? ""
        
        if lastText.isEmpty{
            MessageAlert(title:"",message: "Please enter Last Name")
            return
        }
        
        let trimmedPhoneName = txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
     
        if (trimmedPhoneName?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter Mobile number")
            return
        }
        
        let trimmedEmailName = txt_Email.text?.trimmingCharacters(in: .whitespacesAndNewlines)
     
        if (trimmedEmailName?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter Email")
            return
        }
        
        if  !(trimmedEmailName?.EmailValidation())!{
                MessageAlert(title:"",message: "Enter either valid Email")
                return
              }
        
        let addressText = txt_Address.text ?? ""

        if addressText.isEmpty{
            MessageAlert(title:"",message: "Please enter Address")
            return
        }
        
        let cityText = txt_City.text ?? ""
        if cityText.isEmpty{
            MessageAlert(title:"",message: "Please enter City")
            return
        }
        
        if int_Province == 0
        {
            MessageAlert(title:"",message: "Please select Province")
            return
        }
        
    
        let trimmedPassword = txt_Password.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (trimmedPassword?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter Password")
            return
        }
        
        if trimmedPassword!.count < 6{
            MessageAlert(title:"",message: "Required minimum 6 Characters Password")
            return
        }
    
        let trimmedConfirmPassword = txt_ConfirmPass.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (trimmedConfirmPassword?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter Confirm Password")
            return
        }
        
        if  trimmedPassword != trimmedConfirmPassword {
            MessageAlert(title:"",message: "Password not match")
            return
        }
        
        if str_DialCode == "" {
            str_DialCode = "27"
        }
        
        var dict: Dictionary<String, Any> = [:]
            dict["firstName"] = nameText
            dict["lastName"] = lastText
            dict["dialCode"] = str_DialCode
            dict["phoneNumber"] = trimmedPhoneName
            dict["email"] = trimmedEmailName
            dict["address"] = addressText
            dict["city"] = cityText
            dict["provinceId"] = int_Province
            dict["password"] = trimmedPassword
            dict["deviceType"] = GlobalConstants.deviceType
            dict["deviceToken"] = "Device Token".deviceToken
        
            print(dict)
            SignupRequest(Params: dict)
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func SignupRequest(Params:[String: Any]){
        
        SignInCreateAcRequest.shared.signInCreateData(requestParams: Params) { (obj,status,msg,authToken) in
        
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonalDocumentController") as! PersonalDocumentController
                
                    vc.str_AccessToken = authToken
                    vc.int_UserId =   UserDefaults.standard.integer(forKey: "USER_ID")

                   self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension CreateAcController : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}
