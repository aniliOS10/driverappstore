//
//  CompleteAcController.swift
//  DriverMinaApp
//
//  Created by Apple on 08/02/21.
//

import UIKit

class CompleteAcController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
