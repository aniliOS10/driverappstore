//
//  AddVehicleVc.swift
//  DriverMinaApp
//
//  Created by Apple on 10/02/21.
//

import UIKit
import SDWebImage
import Photos
import MobileCoreServices
import DropDown
import Alamofire
import Reachability

class AddVehicleVc: UIViewController {

    
    @IBOutlet weak var txt_VehicleType : UITextField!
    @IBOutlet weak var txt_VehicleName : UITextField!
    @IBOutlet weak var txt_VehicleColor : UITextField!
    @IBOutlet weak var txt_VehicleReg : UITextField!
    
    @IBOutlet weak var lbe_VehicleType : UILabel!
    @IBOutlet weak var lbe_VehicleName : UILabel!
    @IBOutlet weak var lbe_VehicleColor : UILabel!
    
    @IBOutlet weak var btn_VehicleType : UIButton!
    @IBOutlet weak var btn_VehicleName : UIButton!
    @IBOutlet weak var btn_VehicleColor : UIButton!
    
    @IBOutlet weak var btn_Car : UIButton!
    @IBOutlet weak var btn_Scooter : UIButton!
    @IBOutlet weak var btn_Bakkie : UIButton!
    @IBOutlet weak var btn_Submit : UIButton!
    @IBOutlet weak var btn_Front : UIButton!
    @IBOutlet weak var btn_Rear : UIButton!

    @IBOutlet weak var img_Front : UIImageView!
    @IBOutlet weak var img_Rear : UIImageView!

    @IBOutlet weak var view_Front : UIView!
    @IBOutlet weak var view_Rear : UIView!
    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!


    var imagePicker = UIImagePickerController()

    let dropServiceType = DropDown()
    let dropVehicleName = DropDown()
    let dropVehicleColor = DropDown()
    
    var VehicleArray = NSArray()

    var ColoursArray = [String]()
    var ColoursArrayID = [Int]()
    
    var CarBrandsArray = [String]()
    var CarBrandsArrayID = [Int]()

    var isFront = false
    var isRear = false
    
    var VehicleID_Selected = 1
    var int_ColoursID = 0
    var int_CarBrandID = 0
    var int_ServiceType = 0
    var int_UserId = 0
    var countAPI = 0
    var imageType = ""
    var str_AccessToken = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        getVehicleAPI(false)
        topViewLayout()
        CustomizationTextFeild()
        CustomizationButton()
        
        setDropsColor()

        NotificationCenter.default.addObserver(self, selector: #selector(self.Type_Action), name: NSNotification.Name(rawValue: "Type_Action"), object: nil)

    }
    
    
    @objc func Type_Action(_ notification: NSNotification) {
        let defaults = UserDefaults.standard
        let type = defaults.integer(forKey: "Type")
        
        if type == 1 {
            self.txt_VehicleType.text = "Normal".capitalized
            self.int_ServiceType = type
        }
        if type == 2 {
            self.txt_VehicleType.text = "Express".capitalized
            self.int_ServiceType = type
        }
        if type == 3 {
            self.txt_VehicleType.text = "Bakkie".capitalized
            self.int_ServiceType = type
        }
        
    }
    func topViewLayout(){
        if !AddVehicleVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    
    func setDropsColor() {
        DropDown.appearance().backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().cornerRadius = 10
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().textFont = .SemiBoldFont(18)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_VehicleType.delegate = self
        txt_VehicleName.delegate = self
        txt_VehicleColor.delegate = self
        txt_VehicleReg.delegate = self

        txt_VehicleType.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_VehicleName.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_VehicleColor.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_VehicleReg.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Submit.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Front.tag = 10001
        btn_Front.addTarget(self, action: #selector(action_Front), for: .touchUpInside)
        
        btn_Rear.tag = 10002
        btn_Rear.addTarget(self, action: #selector(action_Rear), for: .touchUpInside)
        
    }
    
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    @objc func action_Front(sender: UIButton){
        imageType = "Front"
        chooseImageFromGalleryAndCamera()
        
    }
    
    @objc func action_Rear(sender: UIButton){
        imageType = "Rear"
        chooseImageFromGalleryAndCamera()
    }
    
    
    
    
    //MARK:- Action
    @IBAction fileprivate func Next_Action(_ sender: Any) {
      
        upload_DataAddVehicleVc()
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func VehicleServiceType_Action(_ sender: Any) {
      //  setupServiceTypeDropDown()
    }
    
    @IBAction func VehicleColor_Action(_ sender: Any) {
        
//        if ColoursArray.count > 0 {
//            dropVehicleColor.show()
//        }
//        else{
//            setupVehicleColorDropDown()
//        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"BrandListController") as! BrandListController

        vc.modalPresentationStyle = .overFullScreen
        vc.VehicleID_Selected = 0
        vc.DataPassColors = { (starttime , endTime) in
            self.txt_VehicleColor.text = starttime
            self.int_ColoursID = endTime
        }

        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func VehicleName_Action(_ sender: Any) {
//        if CarBrandsArray.count > 0 {
//            dropVehicleName.show()
//        }
//        else{
//            setupVehicleTypeDropDown()
//        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"BrandListController") as! BrandListController

        vc.modalPresentationStyle = .overFullScreen
        vc.VehicleID_Selected = self.VehicleID_Selected
        vc.DataPassBrand = { (starttime , endTime) in
            
            self.txt_VehicleName.text = starttime
            self.int_CarBrandID = endTime
            
        }

        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func bakkie_Action(_ sender: Any) {
        btn_Bakkie.setTitleColor(UIColor.white, for: .normal)
        btn_Bakkie.titleLabel?.font =  .BoldFont(16)
        btn_Bakkie.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Scooter.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Scooter.titleLabel?.font =  .BoldFont(16)
        btn_Scooter.backgroundColor = UIColor.white
        
        btn_Car.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Car.titleLabel?.font =  .BoldFont(16)
        btn_Car.backgroundColor = UIColor.white
        
        lbe_VehicleName.text = "Bakkie Brand"
        lbe_VehicleColor.text = "Bakkie Color"
        VehicleID_Selected = 3
        self.CarBrandsArray.removeAll()
        self.CarBrandsArrayID.removeAll()
        self.txt_VehicleName.text = ""
        self.int_CarBrandID = 0
        self.txt_VehicleColor.text = ""
        self.int_ColoursID = 0
        self.txt_VehicleType.text = ""
        self.txt_VehicleReg.text = ""


    }
    
    @IBAction func car_Action(_ sender: Any) {
        btn_Car.setTitleColor(UIColor.white, for: .normal)
        btn_Car.titleLabel?.font =  .BoldFont(16)
        btn_Car.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Scooter.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Scooter.titleLabel?.font =  .BoldFont(16)
        btn_Scooter.backgroundColor = UIColor.white
        
        btn_Bakkie.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Bakkie.titleLabel?.font =  .BoldFont(16)
        btn_Bakkie.backgroundColor = UIColor.white
        
      
        lbe_VehicleName.text = "Car Brand"
        lbe_VehicleColor.text = "Car Color"
        VehicleID_Selected = 2
        self.CarBrandsArray.removeAll()
        self.CarBrandsArrayID.removeAll()
        self.txt_VehicleName.text = ""
        self.int_CarBrandID = 0
        self.txt_VehicleColor.text = ""
        self.int_ColoursID = 0
        self.txt_VehicleType.text = ""
        self.txt_VehicleReg.text = ""

        
    }
    @IBAction func scooter_Action(_ sender: Any) {
        
        btn_Scooter.setTitleColor(UIColor.white, for: .normal)
        btn_Scooter.titleLabel?.font =  .BoldFont(16)
        btn_Scooter.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        
        btn_Car.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Car.titleLabel?.font =  .BoldFont(16)
        btn_Car.backgroundColor = UIColor.white
        
        btn_Bakkie.setTitleColor( #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1), for: .normal)
        btn_Bakkie.titleLabel?.font =  .BoldFont(16)
        btn_Bakkie.backgroundColor = UIColor.white
        
        lbe_VehicleName.text = "Scooter Brand"
        lbe_VehicleColor.text = "Scooter Color"
        VehicleID_Selected = 1
        self.CarBrandsArray.removeAll()
        self.CarBrandsArrayID.removeAll()
        self.txt_VehicleName.text = ""
        self.int_CarBrandID = 0
        self.txt_VehicleColor.text = ""
        self.int_ColoursID = 0
        self.txt_VehicleType.text = ""
        self.txt_VehicleReg.text = ""

    }
    
    
    //MARK:- Vehicle Brands Drop Down
    func setupVehicleTypeDropDown() {
    
        if CarBrandsArray.count > 0 {
            dropVehicleName.anchorView = btn_VehicleName
            dropVehicleName.bottomOffset = CGPoint(x: 0, y: btn_VehicleName.bounds.height)
        
            dropVehicleName.dataSource = CarBrandsArray
        
            dropVehicleName.selectionAction = { [weak self] (index, item) in
                self!.txt_VehicleName.text = item
                self!.int_CarBrandID = self!.CarBrandsArrayID[index]

            }
        }
        else{
            getAllBrandsAPI(true)
        }
        
    }
    
    //MARK:- Vehicle Color Drop Down

    func setupVehicleColorDropDown() {
           
        if ColoursArray.count > 0 {
            dropVehicleColor.anchorView = btn_VehicleColor
            dropVehicleColor.bottomOffset = CGPoint(x: 0, y: btn_VehicleColor.bounds.height)
        
            dropVehicleColor.dataSource = ColoursArray
        
            dropVehicleColor.selectionAction = { [weak self] (index, item) in
                self!.txt_VehicleColor.text = item
                self!.int_ColoursID = (self?.ColoursArrayID[index])!
            }
        }
        else{
            getAllVehicleColoursAPI(true)
        }
       
    }
    


    func upload_DataAddVehicleVc(){
        
        if int_CarBrandID == 0 {
            MessageAlert(title:"",message: "Please select vehicle Brand")
            return
        }
        
        if int_ColoursID == 0 {
            MessageAlert(title:"",message: "Please select vehicle Colours")
            return
        }
        
        if txt_VehicleReg.text?.count == 0 {
            MessageAlert(title:"",message: "Please enter vehicle Registration number")
            return
        }
        
        if isFront == false {
            MessageAlert(title:"",message: "Please add Vehicle Front picture")
            return
        }
        
        if isRear == false {
            MessageAlert(title:"",message: "Please add Vehicle Rear picture")
            return
        }


        var dict: Dictionary<String, AnyObject> = [:]
            dict["VehicleTypeId"] = VehicleID_Selected as AnyObject
            dict["DriverId"] = int_UserId as AnyObject
            dict["BrandId"] = int_CarBrandID as AnyObject
            dict["VehicleColorId"] = int_ColoursID as AnyObject
            dict["RegisterationNumber"] = txt_VehicleReg.text as AnyObject

      
        SignUpAddVehicle_MultipartFormData(parameters:dict,FrontSideImage : self.img_Front.image!,BackSideImage : self.img_Rear.image!,isLoader: true, loaderMessage: "SignUp"){ (obj,er) in

            if obj?["status"] as? Bool ?? false {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CompleteAcController") as! CompleteAcController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.MessageAlert(title:"",message: obj?["message"] as? String ?? "There was an error connecting to the server.try again")
            }
        }
    }

}
extension AddVehicleVc : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}


extension AddVehicleVc {

    func getVehicleAPI(_ loader:Bool){
        
        VehicleTypesRequest.shared.getVehicleTypesData(requestParams: [:],loader) { (object,status)  in
            if status {

                    if let data = object?.data.list as NSArray?{
                        self.VehicleArray = data as NSArray
                    }
            
                }
            }
    }
    
    func getAllVehicleColoursAPI(_ loader:Bool){
        
        VehicleColoursRequest.shared.getVehicleColoursData(requestParams: [:],loader) { (object,status)  in
            if status {

            self.ColoursArray.removeAll()
            self.ColoursArrayID.removeAll()
            
            for i in 0..<(object?.data?.list?.count ?? 0)
            {
                let dict = object?.data?.list?[i]
                self.ColoursArray.append(dict?.vehicleColorName ?? "")
                self.ColoursArrayID.append(dict?.vehicleColorID ?? 0)
            }
            self.setupVehicleColorDropDown()
            self.dropVehicleColor.show()
        }
    }
    }
    
    
    func getAllBrandsAPI(_ loader:Bool){
        
        VehicleBrandsRequest.shared.getVehicleBrandsData(requestParams: [:],loader) { (object,status)  in
            if status {
            self.CarBrandsArray.removeAll()
            self.CarBrandsArrayID.removeAll()
            if self.VehicleID_Selected == 1 {
                for i in 0..<(object?.scooterBrands?.scooterBrands?.count ?? 0)
                {
                    let dict = object?.scooterBrands?.scooterBrands?[i]
                    self.CarBrandsArray.append(dict?.brandName ?? "")
                    self.CarBrandsArrayID.append(dict?.brandID ?? 0)
                }
            }
            
            if self.VehicleID_Selected == 2 {
                for i in 0..<(object?.carBrands?.carBrands?.count ?? 0)
                {
                    let dict = object?.carBrands?.carBrands?[i]
                    self.CarBrandsArray.append(dict?.brandName ?? "")
                    self.CarBrandsArrayID.append(dict?.brandID ?? 0)
                }
            }
            
            if self.VehicleID_Selected == 3 {
                for i in 0..<(object?.bakkieBrands?.bakkieBrands?.count ?? 0)
                {
                    let dict = object?.bakkieBrands?.bakkieBrands?[i]
                    self.CarBrandsArray.append(dict?.brandName ?? "")
                    self.CarBrandsArrayID.append(dict?.brandID ?? 0)
                }
            }
          
            self.setupVehicleTypeDropDown()
            self.dropVehicleName.show()
        }
    }
}
}



extension AddVehicleVc : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
                

    @objc func chooseImageFromGalleryAndCamera()
            {
                let alert = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
                
                alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{
                    (UIAlertAction)in
                    print("User click Approve button")
                    self.handelUploadCamera()
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{
                    (UIAlertAction)in
                    print("User click Edit button")
                    self.handelUploadTap()
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{
                    (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
                
            }

    @objc func handelUploadTap() {
                let photos = PHPhotoLibrary.authorizationStatus()
                if photos == .notDetermined {
                    PHPhotoLibrary.requestAuthorization({status in
                        if status == .authorized{
                    DispatchQueue.main.async {

                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                                
                                self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                                self.imagePicker.allowsEditing = true
                                
                                self.imagePicker.mediaTypes = [kUTTypeImage as String]
                                self.present(self.imagePicker, animated: true, completion: nil)
                        }
                            }
                        }
                        
                    })
                }
                else if photos == .authorized {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){

                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                        imagePicker.allowsEditing = true
                        
                        imagePicker.mediaTypes = [kUTTypeImage as String]
                        self.present(imagePicker, animated: true, completion: nil)
                        
                    }
                }
            }

    @objc func handelUploadCamera()  {
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                    imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                    imagePicker.mediaTypes = [kUTTypeImage as String]
                    imagePicker.allowsEditing = true
                    present(imagePicker, animated: true, completion: nil)
                }
            }
            // MARK: - Image Picker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController,
                                       didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
                
    if let editedImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage) {
                    self.dismiss(animated: true, completion: nil)
                    
                    if imageType == "Front" {
                        self.img_Front.image = editedImage
                        isFront = true
                    }
                    if imageType == "Rear" {
                        self.img_Rear.image = editedImage
                        isRear = true

                    }
                 
                }
                else if let origionalImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) {
                    self.dismiss(animated: true, completion: nil)
                    if imageType == "Front" {
                        self.img_Front.image = origionalImage
                        isFront = true
                    }
                    if imageType == "Rear" {
                        self.img_Rear.image = origionalImage
                        isRear = true
                    }
                 }
                self.dismiss(animated: true, completion: nil)
            }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
                dismiss(animated: true, completion: nil)
       }
    }

extension AddVehicleVc {

    func SignUpAddVehicle_MultipartFormData(parameters : [String : AnyObject],FrontSideImage : UIImage,BackSideImage : UIImage,isLoader : Bool, loaderMessage : String, completion: @escaping ( _ success: [String : AnyObject]?, _ error : Error?) -> Void) {
    
        if AlamofireRequest.shared.InterNetConnection()
        {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            let urlString = "BaseURL".SignInAddVehicleDetailsURL
            print("URL - ",urlString)
            
            let urL = URL(string: urlString)
            let bearer : String = "Bearer \(str_AccessToken)"
            
            let headers: HTTPHeaders = [
                "Authorization": bearer,
                "Content-type": "multipart/form-data"
            ]
            
            AF.upload(
                multipartFormData: { multipartFormData in
                    let image1 = FrontSideImage.jpegData(compressionQuality: 0.5)!
                    let image2 = BackSideImage.jpegData(compressionQuality: 0.5)!
                    
                    for key in parameters.keys{
                        let name = String(key)
                        if let val = parameters[name] as? String{
                            multipartFormData.append(val.data(using: .utf8)!, withName: name)
                        }
                        if let new = parameters[name] as? Int {
                            let value = "\(new)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: name)
                        }
                    }

                    if image1 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        
                        name = name + ".jpeg"
                        
                        multipartFormData.append(image1, withName: "FrontSideImage", fileName: name, mimeType:"image/jpeg")
                    }
                    if image2 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        name = name + ".jpeg"

                        multipartFormData.append(image2, withName: "BackSideImage", fileName: name, mimeType:"image/jpeg")
                    }
                },
                to: urL!, method: .post , headers: headers)
                
                .response { response in
                    
                    debugPrint(response)
                    print("Response Code:---->",response.response?.statusCode as Any)
                    Indicator.shared.stopAnimating()
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        print("Response: \(json ?? "")")
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with:data, options: [])
                            print(jsonResponse)
                            completion(jsonResponse as? [String : AnyObject] , nil)
                            
                        } catch let parsingError {
                            print("Error", parsingError)
                            completion(nil , parsingError)
                        }
                    }
                }
        }
        else{
            AlamofireRequest.shared.MsgAlert()
        }
    }

}
class RectangularDashedView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0
    
    var dashBorder: CAShapeLayer?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}

