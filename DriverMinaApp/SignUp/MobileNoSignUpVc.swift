//
//  MobileNoSignUpVc.swift
//  DriverMinaApp
//
//  Created by Apple on 10/02/21.
//

import UIKit
import DropDown
class MobileNoSignUpVc: UIViewController {

    @IBOutlet weak var txt_Phone : UITextField!
    
    @IBOutlet weak var lbe_Code : UILabel!
    @IBOutlet weak var img_CountryFlag : UIImageView!

    @IBOutlet weak var btn_Continue : UIButton!
    @IBOutlet weak var btn_CountryCode : UIButton!

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    
    let dropCountryCode = DropDown()
    var phoneCode =  "27"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topViewLayout()
        CustomizationTextFeild()
        setDropsColor()
        countryCodeDropDown()
    }
    
    func topViewLayout(){
        if !MobileNoSignUpVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    func setDropsColor() {
        DropDown.appearance().backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().cornerRadius = 10
        DropDown.appearance().textColor = UIColor.white
        DropDown.appearance().textFont = .SemiBoldFont(16)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        txt_Phone.delegate = self
        txt_Phone.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Continue.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    
    func countryCodeDropDown() {

        dropCountryCode.anchorView = btn_CountryCode
        dropCountryCode.bottomOffset = CGPoint(x: 0, y: btn_CountryCode.bounds.height)
        dropCountryCode.dataSource = ["+27","+91"]
        dropCountryCode.selectionAction = { [weak self] (index, item) in
            self!.lbe_Code.text = item
            
            if item == "+91"{
                self?.img_CountryFlag.image = UIImage(named: "india_af")!
            }
            else {
                self?.img_CountryFlag.image = UIImage(named: "south_af")!
            }
        }
    }
    
    @IBAction fileprivate func btn_CodeAction(_ sender: Any) {
        dropCountryCode.show()
    }
    
    
    @IBAction fileprivate func btnContinueAction(_ sender: Any) {
        
        let trimmedPhone = txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (trimmedPhone?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter Mobile number")
            return
        }
        
        self.phoneCode = self.lbe_Code.text?.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range: nil) ?? "27"
        
        var dict: Dictionary<String, Any> = [:]
            dict["dialCode"] = phoneCode
            dict["phoneNo"] = trimmedPhone
        
            SignupRequest(Params:dict)
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func SignupRequest(Params:[String: Any]){
        
        SignInPhoneRequest.shared.signInPhoneData(requestParams: Params) { (obj,status,msg) in
        
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationOTPController") as! VerificationOTPController
                    vc.str_DialCode =  self.phoneCode
                    vc.str_PhoneNo = Params["phoneNo"] as! String
                    self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension MobileNoSignUpVc : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " ") {
        
                return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

}
