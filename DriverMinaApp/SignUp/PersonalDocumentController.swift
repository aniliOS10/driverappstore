//
//  PersonalDocumentController.swift
//  DriverMinaApp
//
//  Created by Apple on 04/02/21.
//

import UIKit
import SDWebImage
import Photos
import MobileCoreServices
import DropDown
import Alamofire
import Reachability
import Lightbox
class PersonalDocumentController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    
    @IBOutlet weak var txt_DOB : UITextField!
    @IBOutlet weak var txt_Address : UITextField!
    @IBOutlet weak var txt_Address1 : UITextField!
    @IBOutlet weak var txt_City : UITextField!
    
    @IBOutlet weak var txt_SelfieName : UITextField!
    @IBOutlet weak var txt_ProofName : UITextField!
    
    @IBOutlet weak var lbe_IdType : UILabel!
    @IBOutlet weak var lbe_State : UILabel!
    @IBOutlet weak var lbe_IdName : UILabel!
    @IBOutlet weak var lbe_IdPersonal : UILabel!
    @IBOutlet weak var lbe_IdProof : UILabel!
    
    @IBOutlet weak var btn_Next : UIButton!
    @IBOutlet weak var btn_Dob : UIButton!
    @IBOutlet weak var btn_IdType : UIButton!
    @IBOutlet weak var btn_State : UIButton!
    @IBOutlet weak var btn_IdUpload : UIButton!
    @IBOutlet weak var btn_IdPersonal : UIButton!
    @IBOutlet weak var btn_IdProof : UIButton!
    
    @IBOutlet weak var btn_IdSelfie_Show : UIButton!
    @IBOutlet weak var btn_IdPersonal_Show : UIButton!
    @IBOutlet weak var btn_IdProof_Show : UIButton!
    

    @IBOutlet weak var img_User : UIImageView!

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    
    var imagePicker = UIImagePickerController()
    var dateText = Date()

    
    let dropStates = DropDown()
    let dropID = DropDown()

    var stateId = Int()

    var statesArray = [String]()
    var statesArrayID = [Int]()
    
    var str_AccessToken = ""
    var imageType = ""
    var int_UserId = 3
    
    var img_ProfilePic = UIImage()
    var img_PersonalId = UIImage()
    var img_Selfie = UIImage()
    var img_ResidentProof = UIImage()

    var isProfile = false
    var isPersonalId = false
    var isSelfie = false
    var isResidentProof = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        topViewLayout()
        
//      CustomizationTextFeild()
//      CustomizationButton()
        
        setDropsColor()
        setupIDTypeDropDown()
    }
    
    func topViewLayout(){
        if !CreateAcController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    func setDropsColor() {
        DropDown.appearance().backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().cornerRadius = 10
        DropDown.appearance().textColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_DOB.delegate = self
        txt_Address.delegate = self
        txt_Address1.delegate = self
        txt_City.delegate = self

        txt_DOB.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Address.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Address1.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_City.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Next.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
    }
    
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction fileprivate func Next_Action(_ sender: Any) {
        upload_Data()
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Profile_Image_Update(_ sender: Any) {
           imageType = "User_Profile"
           chooseImageFromGalleryAndCamera()
    }
    
    @IBAction func Personal_Image_Update(_ sender: Any) {
        
        imageType = "Selfie"
        chooseImageFromGalleryAndCamera()
          
    }
    
    @IBAction func ID_Image_Update(_ sender: Any) {
           imageType = "PersonalId"
           chooseImageFromGalleryAndCamera()
    }
    
    @IBAction func Res_Image_Update(_ sender: Any) {
           imageType = "ResidentProof"
           chooseImageFromGalleryAndCamera()
    }
    
    
    @IBAction func Selfie_Image_Show(_ sender: Any) {
        
        imageTapped(tappedImage: img_Selfie)
    }
    
    @IBAction func ID_Image_Show(_ sender: Any) {
        imageTapped(tappedImage: img_PersonalId)
        
    }
    
    @IBAction func Res_Image_Show(_ sender: Any) {
        imageTapped(tappedImage: img_ResidentProof)

    }
    

    //MARK:-  States Drills Api
    
    @IBAction func ID_Type_Action(_ sender: Any) {

        dropID.show()
    }
    
    func setupIDTypeDropDown() {
    
        dropID.anchorView = btn_IdType
        dropID.bottomOffset = CGPoint(x: 0, y: btn_IdType.bounds.height)
    
        dropID.dataSource = ["Licence"]
    
        dropID.selectionAction = { [weak self] (index, item) in
            self!.lbe_IdType.text = item
        }
    }
    
    @objc func chooseImageFromGalleryAndCamera()
    {
        let alert = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{
            (UIAlertAction)in
            print("User click Approve button")
            self.handelUploadCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{
            (UIAlertAction)in
            print("User click Edit button")
            self.handelUploadTap()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{
            (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func handelUploadTap() {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
            DispatchQueue.main.async {

        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                        
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                        self.imagePicker.allowsEditing = false
                        
                        self.imagePicker.mediaTypes = [kUTTypeImage as String]
                        self.present(self.imagePicker, animated: true, completion: nil)
                }
                    }
                }
                
            })
        }
        else if photos == .authorized {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
    
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: nil)
                
            }
        }
    }
    
    @objc func handelUploadCamera()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera;
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    // MARK: - Image Picker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        if let editedImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage) {
            self.dismiss(animated: true, completion: nil)
            
            var fileName = "File"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                       fileName = url.lastPathComponent
                   //var fileType = url.pathExtension
                }
    
            if imageType == "User_Profile" {
                self.img_User.image = editedImage
                img_ProfilePic = editedImage
                isProfile = true

            }
            if imageType == "PersonalId" {
                 img_PersonalId = editedImage
                 isPersonalId = true
                 btn_IdPersonal_Show.isHidden = false
                 lbe_IdName.text = fileName
                 lbe_IdName.textColor = UIColor.black


            }
            if imageType == "Selfie" {
                 img_Selfie = editedImage
                 isSelfie = true
                 btn_IdSelfie_Show.isHidden = false
                 txt_SelfieName.text = fileName
                 txt_SelfieName.textColor = UIColor.black


            }
            if imageType == "ResidentProof" {
                img_ResidentProof = editedImage
                isResidentProof = true
                btn_IdProof_Show.isHidden = false
                txt_ProofName.text = fileName
                txt_ProofName.textColor = UIColor.black
                

            }
        }
        else if let origionalImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) {
            self.dismiss(animated: true, completion: nil)
            
            var fileName = "File"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                       fileName = url.lastPathComponent
                   //var fileType = url.pathExtension
                }
            
            if imageType == "User_Profile" {
                self.img_User.image = origionalImage
                img_ProfilePic = origionalImage
                isProfile = true

            }
            if imageType == "PersonalId" {
                img_PersonalId = origionalImage
                isPersonalId = true
                btn_IdPersonal_Show.isHidden = false
                lbe_IdName.text = fileName
                lbe_IdName.textColor = UIColor.black
            }
            if imageType == "Selfie" {
                img_Selfie = origionalImage
                isSelfie = true
                btn_IdSelfie_Show.isHidden = false
                txt_SelfieName.text = fileName
                txt_SelfieName.textColor = UIColor.black
            }
            if imageType == "ResidentProof" {
                img_ResidentProof = origionalImage
                isResidentProof = true
                btn_IdProof_Show.isHidden = false
                txt_ProofName.text = fileName
                txt_ProofName.textColor = UIColor.black
            }
         }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func DateOfBrith(_ sender: Any) {
        addPicker()
    }
    
    func addPicker(){
       
        let alert = UIAlertController(style: .alert, title: "Select Date of Birth")
        
        if #available(iOS 13.4, *) {
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
            
            alert.setTitle(font: .RegularFont(20), color: UIColor.black)

           alert.view.tintColor = UIColor.black
            
        }
        else{
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
            
            alert.setTitle(font: .SemiBoldFont(20), color: UIColor.white)
            
            alert.view.tintColor = UIColor.white

        }
        
        let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        dateText = date ?? Date()
        
        
        if self.txt_DOB.text == ""
        {
            dateText = date ?? Date()
        }
        else
        {
            
            let isoDate = txt_DOB.text
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateText = dateFormatter.date(from:isoDate!)!
            
        }
        
        alert.addDatePicker(mode: .date, date: dateText, minimumDate: nil, maximumDate: date) { date in
            self.dateText = date
            }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            
            self.txt_DOB.text = convertToDDMMyyyy(date:self.dateText)
            
            }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        
        alert.show()
        
    }
    
    
    func upload_Data(){
        
        if self.txt_DOB.text?.count == 0 {
            MessageAlert(title:"",message: "Please enter DOB")
            return
        }
        
        if isProfile == false {
            MessageAlert(title:"",message: "Please add profile picture")
            return
        }
        if isPersonalId == false {
            MessageAlert(title:"",message: "Please add licence")
            return
        }
        if isSelfie == false {
            MessageAlert(title:"",message: "Please Upload Selfie (Hold license to chest)")
            return
        }
        if isResidentProof == false {
            MessageAlert(title:"",message: "Please add Residence proof")
            return
        }


        var dict: Dictionary<String, AnyObject> = [:]
            dict["DOB"] = self.txt_DOB.text as AnyObject
            dict["DocTypeId"] = 1 as AnyObject
            dict["DriverId"] = int_UserId as AnyObject


        SignUpPersonal_MultipartFormData(parameters:dict, ResidentProof: img_ResidentProof, Selfie: img_Selfie, ProfilePic: img_ProfilePic, PersonalId: img_PersonalId, isLoader: true, loaderMessage: "SignUp"){ (obj,er) in

            if obj?["status"] as? Bool ?? false {

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleVc") as! AddVehicleVc
                vc.int_UserId = self.int_UserId
                vc.str_AccessToken = self.str_AccessToken
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.MessageAlert(title:"",message: obj?["message"] as? String ?? "There was an error connecting to the server.try again")
            }
        }
    }
    
    
  @objc func imageTapped(tappedImage: UIImage)
  {
     // let tappedImage = tapGestureRecognizer.view as! UIImageView

      let images = [
          LightboxImage(
            image:tappedImage,
              text: ""
          )
      ]
      let controller = LightboxController(images: images)
      controller.dynamicBackground = true
      present(controller, animated: true, completion: nil)
  }
}


extension PersonalDocumentController : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}


extension PersonalDocumentController {

    func SignUpPersonal_MultipartFormData(parameters : [String : AnyObject],ResidentProof : UIImage,Selfie : UIImage,ProfilePic : UIImage,PersonalId : UIImage,isLoader : Bool, loaderMessage : String, completion: @escaping ( _ success: [String : AnyObject]?, _ error : Error?) -> Void) {
        
        
        if AlamofireRequest.shared.InterNetConnection()
        {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            let urlString = "BaseURL".AddDriverDocumentsURL
            print("URL - ",urlString)
            
            let urL = URL(string: urlString)
            let bearer : String = "Bearer \(str_AccessToken)"
            
            let headers: HTTPHeaders = [
                "Authorization": bearer,
                "Content-type": "multipart/form-data"
            ]
            
            AF.upload(
                multipartFormData: { multipartFormData in
                    let image1 = ResidentProof.jpegData(compressionQuality: 0.4)!

                    let image2 = Selfie.jpegData(compressionQuality: 0.4)!

                    let image3 = ProfilePic.jpegData(compressionQuality: 0.7)!

                    let image4 = PersonalId.jpegData(compressionQuality: 0.4)!

                    for key in parameters.keys{
                        let name = String(key)
                        if let val = parameters[name] as? String{
                            multipartFormData.append(val.data(using: .utf8)!, withName: name)
                        }
                        if let new = parameters[name] as? Int {
                            let value = "\(new)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: name)
                        }

                    }

                   
                    if image1 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        name = name + ".jpeg"
                        multipartFormData.append(image1, withName: "ResidentProof", fileName: name, mimeType:"image/jpeg")
                    }
                    if image2 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        
                        name = name + ".jpeg"


                        multipartFormData.append(image2, withName: "Selfie", fileName: name, mimeType:"image/jpeg")
                    }
                    if image3 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))

                        name = "mio" + ".jpeg"

                        multipartFormData.append(image3, withName: "ProfilePic", fileName: name, mimeType:"image/jpeg")
                    }
                    if image4 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))

                        name = name + ".jpeg"

                        multipartFormData.append(image4, withName: "PersonalId", fileName: name, mimeType:"image/jpeg")
                    }
                },
                to: urL!, method: .post , headers: headers)
                
                .response { response in
                    
                    debugPrint(response)
                    print("Response Code:---->",response.response?.statusCode as Any)
        
                    Indicator.shared.stopAnimating()
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        print("Response: \(json ?? "")")
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with:data, options: [])
                            print(jsonResponse)
                            completion(jsonResponse as? [String : AnyObject] , nil)
                            
                        } catch let parsingError {
                            print("Error", parsingError)
                            completion(nil , parsingError)
                        }
                    }
                }
        }
        else{
            AlamofireRequest.shared.MsgAlert()
        }
    }

}
