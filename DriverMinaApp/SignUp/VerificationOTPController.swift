//
//  VerificationOTPController.swift
//  DriverMinaApp
//
//  Created by Apple on 03/02/21.
//

import UIKit
import SVPinView
import Reachability
import IQKeyboardManagerSwift

class VerificationOTPController: UIViewController {

    @IBOutlet weak var view_Pin: SVPinView!
    @IBOutlet weak var btn_Submit : UIButton!
    @IBOutlet weak var lbe_Phone : UILabel!

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    var str_OTP = ""
    var str_DialCode = "27"
    var str_PhoneNo = ""
    var OTPValueCheck = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbe_Phone.text = "+" + str_DialCode + " " + str_PhoneNo
        topViewLayout()
        configurePinView()
    }
    
    func topViewLayout()
    {
        if !VerificationOTPController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    //MARK:-   Configure PinView Function
    func configurePinView() {
        self.OTPValueCheck = ""
        
        view_Pin.borderLineColor = UIColor.gray
        view_Pin.borderLineThickness = 1.0

        view_Pin.activeBorderLineColor = UIColor.black
        view_Pin.activeBorderLineThickness = 1.7
        
        view_Pin.keyboardType = .numberPad
        view_Pin.shouldSecureText = false
        
        view_Pin.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        view_Pin.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            
            if pin == self.str_OTP{
                self.OTPValueCheck = String(pin)
            }
            else{
               self.OTPValueCheck = String(pin)
            }
        }
    }
    
    
    //MARK:-  Dismiss Keyboard Action
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    //MARK:-    touches Began KEYBOARD Hide
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction fileprivate func resendOTP_Action(_ sender: Any) {
        
        var dict: Dictionary<String, Any> = [:]
            dict["dialCode"] = str_DialCode
            dict["phoneNo"] = str_PhoneNo
        
        SignInPhoneRequest.shared.signInPhoneData(requestParams: dict) { (obj,status,msg) in
            if status == false {
                self.MessageAlert(title:"",message:msg)
            }
            else
            {
                self.MessageAlert(title:"",message:msg)
                self.configurePinView()
            }
        }
        
    }
    
    
    @IBAction fileprivate func btnContinueAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if String(self.OTPValueCheck).count < 4{
            self.MessageAlert(title: "", message:"Please verify your OTP")
            return
        }

        let params = ["dialCode":str_DialCode,"phoneNumber":str_PhoneNo,"code": String(self.OTPValueCheck)] as [String : Any]

        self.callConfirmPhoneApi(param:params)
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Verification OTP
    func callConfirmPhoneApi(param : [String : Any]){
        
        SignInVerifyPhoneRequest.shared.verifyPhoneData(requestParams: param){ (obj,status,msg) in
            if status {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateAcController") as! CreateAcController
                vc.str_DialCode = self.str_DialCode
                vc.str_PhoneNo = param["phoneNumber"] as? String ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
               self.MessageAlert(title: "", message:msg)
            }
        }
    }
}
