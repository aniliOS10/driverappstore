//
//  SelectPlansVC.swift
//  DriverMinaApp
//
//  Created by Apple on 17/03/21.
//

import UIKit
import UPCarouselFlowLayout

class SelectPlansVC: UIViewController {

    @IBOutlet fileprivate var Collectionview : UICollectionView!
    @IBOutlet weak var btn_NavConst: NSLayoutConstraint!

    var currentIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentIndex = 0
        setupLayout()
    }
  
    @IBAction fileprivate func CloseAction (_ sender : UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
    
    // collection View Flow Layout
    fileprivate func setupLayout() {
        self.Collectionview.dataSource  = self
        self.Collectionview.delegate = self

        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width - 72
        var screenHeight = screenSize.height - 160
        
        if !SelectPlansVC.hasSafeArea{
            screenHeight = screenSize.height - 120
            if btn_NavConst != nil {
                btn_NavConst.constant = 22
           }
        }
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: screenWidth, height: screenHeight )
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 0)
        layout.sideItemScale = 0.9
        layout.scrollDirection = .horizontal
        Collectionview.collectionViewLayout = layout
        
    }
}

extension SelectPlansVC : UICollectionViewDataSource ,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 3
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroductionCell", for: indexPath) as? IntroductionCell
        
            cell?.contentView.backgroundColor = UIColor.clear
        
        if indexPath.row == 0 {
            cell?.lbe_Price.text = "R 45"
            cell?.lbe_Name.text = "Normal".capitalized
            cell?.lbe_Title.text = "Same day delivery"

        }
        if indexPath.row == 1 {
            cell?.lbe_Price.text = "R 80"
            cell?.lbe_Name.text = "Express".capitalized
            cell?.lbe_Title.text = "Within 2 hrs delivery"
        }
        if indexPath.row == 2 {
            cell?.lbe_Price.text = "R 300"
            cell?.lbe_Name.text = "Bakkie".capitalized
            cell?.lbe_Title.text = "Immediate delivery"
        }
        
        
        cell?.btn_Type.tag = indexPath.row
        cell?.btn_Type.addTarget(self, action: #selector(action_Types), for: .touchUpInside)
        
        return cell ?? UICollectionViewCell()
        
    }
    
    @objc func action_Types(sender: UIButton){
        let buttonTag = sender.tag
        let defaults = UserDefaults.standard
        defaults.set(buttonTag + 1, forKey: "Type")
        NotificationCenter.default.post(name: Notification.Name("Type_Action"), object:nil)
        dismiss(animated: true, completion: nil)
    }
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width - 72
        var screenHeight = screenSize.height - 160
        
        if !SelectPlansVC.hasSafeArea{
            screenHeight = screenSize.height - 120
        }

        return CGSize(width: screenWidth, height: screenHeight )
    }

   
    
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//
////        if section == 0 {
////            return UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
////
////        }
////        if section == 1 {
////            return UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
////
////        }
//
//        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
//    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
//        if let ip = self.Collectionview!.indexPathForItem(at: center) {
//            self.pageController.currentPage = ip.row
//        }
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
    
    
}

