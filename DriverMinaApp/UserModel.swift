//
//  UserModel.swift
//  DriverMinaApp
//
//  Created by Apple on 09/03/21.
//

import UIKit
import Foundation

class UserModel: NSObject, NSCoding{
    
    var accessToken: String?
    var email: String?
    var isSocialUser = Bool()
    var isEmailConfirmed = Bool()
    var isPhoneNoVerified = Bool()
    var firstName = ""
    var lastName  = ""
    var city = ""
    var country = ""
    var currentAddress = ""
    var screenId = 0
    var userID = 0
    var vehicleTypeId = 0
    var pic = ""
    var state = ""
    var phoneNumber = ""
    var timeZoneCode = TimeZone.current
    
    override init() {
        
    }
    
    init(model: [String : Any]) {
        
        
        if let smsNotificationStatus = model["isEmailConfirmed"] as? Bool{
            self.isEmailConfirmed = smsNotificationStatus
        }
        if let isPhoneNoVerifiedD = model["isPhoneNoVerified"] as? Bool{
            self.isPhoneNoVerified = isPhoneNoVerifiedD
        }
        if let isSocialUserData = model["isSocialUser"] as? Bool{
            self.isSocialUser = isSocialUserData
        }
        if let accessToken = model["accessToken"] as? String{
            self.accessToken = accessToken
        }
        if let email = model["email"] as? String{
            self.email = email
        }
        if let firstName = model["firstName"] as? String{
            self.firstName = firstName
        }
        if let lastName = model["lastName"] as? String{
            self.lastName = lastName
        }
        if let pic = model["profilePic"] as? String{
            self.pic = pic
        }
        if let city = model["city"] as? String{
            self.city = city
        }
        if let country = model["country"] as? String{
            self.country = country
        }
        if let currentAddress = model["currentAddress"] as? String{
            self.currentAddress = currentAddress
        }
        if let state = model["state"] as? String{
            self.state = state
        }
        if let screenIds = model["screenId"] as? Int{
            self.screenId = screenIds
        }
        if let screenIds = model["userID"] as? Int{
            self.userID = screenIds
        }
        if let screenIds = model["vehicleTypeId"] as? Int{
            self.vehicleTypeId = screenIds
        }
        if let screenIds = model["phoneNumber"] as? String{
            self.phoneNumber = screenIds
        }
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        var mutDict : [String : Any] = [:]
        
        mutDict["isEmailConfirmed"] = aDecoder.decodeBool(forKey:"isEmailConfirmed")
        mutDict["isPhoneNoVerified"] = aDecoder.decodeBool(forKey:"isPhoneNoVerified")
        mutDict["isSocialUser"] = aDecoder.decodeBool(forKey:"isSocialUser")
        
        if  let token = aDecoder.decodeObject(forKey:"accessToken")
        {
            mutDict["accessToken"] = token
        }
        
        if  let email = aDecoder.decodeObject(forKey:"email")
         {
            mutDict["email"] = email
        }
        
        if  let firstName = aDecoder.decodeObject(forKey:"firstName")
        {
            mutDict["firstName"] = firstName
        }
        
        if  let lastName = aDecoder.decodeObject(forKey:"lastName")
        {
            mutDict["lastName"] = lastName
        }
        
        if  let pic = aDecoder.decodeObject(forKey:"profilePic")
        {
            mutDict["profilePic"] =  pic
        }
        
      
        if  let city = aDecoder.decodeObject(forKey:"city")
              {
                  mutDict["city"] =  city
              }
        
     
        if  let country = aDecoder.decodeObject(forKey:"country")
            {
                 mutDict["country"] =  country
             }
        
        if  let currentAddress = aDecoder.decodeObject(forKey:"currentAddress")
           {
               mutDict["currentAddress"] =  currentAddress
           }
     
        
        if  let state = aDecoder.decodeObject(forKey:"state")
              {
                  mutDict["state"] =  state
              }
        

        mutDict["screenId"] =  aDecoder.decodeInteger(forKey:"screenId")
        mutDict["userID"] =  aDecoder.decodeInteger(forKey: "userID")
        mutDict["vehicleTypeId"] =  aDecoder.decodeInteger(forKey: "vehicleTypeId")
        mutDict["phoneNumber"] =  aDecoder.decodeObject(forKey: "phoneNumber")

        

        self.init(model: mutDict)
    }
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(isEmailConfirmed, forKey: "smsNotificationStatus")
        aCoder.encode(isPhoneNoVerified, forKey: "emailNotificationStatus")
        aCoder.encode(isSocialUser, forKey: "isSocialUser")
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(pic, forKey: "profilePic")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(country, forKey: "country")
        aCoder.encode(currentAddress, forKey: "currentAddress")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(screenId, forKey: "screenId")
        aCoder.encode(userID, forKey: "userID")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(vehicleTypeId, forKey: "vehicleTypeId")
    }
}
