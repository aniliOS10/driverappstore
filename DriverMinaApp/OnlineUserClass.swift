//
//  OnlineUserClass.swift
//  DriverMinaApp
//
//  Created by Apple on 09/03/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit
import Starscream

class OnlineUserClass: NSObject, CLLocationManagerDelegate,GMSMapViewDelegate, WebSocketDelegate,StreamDelegate {

    static let shared = OnlineUserClass()
    var socket: WebSocket!

    let marker: GMSMarker = GMSMarker()
    var locationManagerOnline = CLLocationManager()
    var currentLocationOnline: CLLocation!
    static let geoCoderOnline = CLGeocoder()
    let sessionManager = SessionManager()

    var latitudeOn = 0.0
    var longitudeOn = 0.0
    var str_Address = ""
    var delegateCall = false
    var isDataSend = false

    
    func callLocationForOnline(){
        locationManagerOnline = CLLocationManager()
        locationManagerOnline.delegate = self
        locationManagerOnline.desiredAccuracy = kCLLocationAccuracyBest
        locationManagerOnline.requestAlwaysAuthorization()
        locationManagerOnline.requestWhenInUseAuthorization()
        locationManagerOnline.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.first else {
            return
        }
    
        DispatchQueue.main.async {
            
            let locationData = locations.last! as CLLocation
            var lat = "0.0"
            var long = "0.0"

            if self.delegateCall{
            self.latitudeOn = locationData.coordinate.latitude
            self.longitudeOn = locationData.coordinate.longitude
                
            OnlineUserClass.geoCoderOnline.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in

                    placemarks?.forEach { (placemark) in
                        if let city = placemark.locality {
                            self.str_Address = placemark.name ?? ""
                            self.str_Address = self.str_Address + " " + city
                        }
                    }
                })
            }
            
            lat = String(locationData.coordinate.latitude)
            long = String(locationData.coordinate.longitude)
            
            UserDefaults.standard.set(lat, forKey: "latitude_current")
            UserDefaults.standard.set(long, forKey: "longitude_current")
        }
    }
       
    //MARK:-  OnlineUserRequest Api
    func OnlineUserRequest(userOnline: Bool) -> Void {
        
        if userOnline {

            if !delegateCall {
                callLocationForOnline()
                isDataSend = true
                delegateCall = true
                sokectData()
            }
            
            if self.latitudeOn != 0.0 {
                var userModel = UserModel()
                let sessionManager = SessionManager()
                userModel = sessionManager.getUserDetails()
                
                var dict: Dictionary<String, Any> = [:]
                    dict["location"] = self.str_Address
                    dict["lat"] = latitudeOn
                    dict["lon"] = longitudeOn
                    dict["gpstimestamp"] = self.str_Address
                    dict["driver_id"] = userModel.userID
                    dict["driverPosition"] = self.str_Address
                    dict["markerPreLoc"] =  self.str_Address
                    dict["marker"] =  self.str_Address
                    dict["time"] =  convertTimeFormater(date: Date())
                    dict["date"] =  convertToDDMMyyyy(date: Date())
                    dict["online"] =  true
                
                    DispatchQueue.global(qos: .background).async {
                        self.currentLocationData(requestParams: dict) {(object,status,mess,str)  in
                    }
                     
//                    DispatchQueue.global(qos: .background).async {
//                     //   self.GetRequestNoticeForDrivers(getRequest: true)
//                }
            }
        }
    }
    else{
            if !isDataSend {
               callLocationForOnline()
            }
            else{
                socket.disconnect()
            }
            delegateCall = false
            self.latitudeOn =  0.0
        }
    }
    
    
  func sokectData(){
    
        let request = URLRequest(url: URL(string: "wss://wsmamina.azurewebsites.net/GetRequestNoticeForTopDrivers")!)
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
    

    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            print("websocket is connected:")
            var userModel = UserModel()
            let sessionManager = SessionManager()
            userModel = sessionManager.getUserDetails()
            socket.write(string:String(userModel.userID))
       
        case .disconnected(let reason, let code):
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
            string_To_Json(string)
    
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            break
        case .error(let error):
            print(error!)
            sokectData()
            break
        }
    }
    
    
    func string_To_Json(_ str:String){
        
        let data = Data(str.utf8)
        do {
            if let arrayData = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                    if arrayData.count > 0 {
                        
                        print("Request Found",arrayData)
                        if !self.sessionManager.getFirstRequestData_BOOL(){
                            if UserDefaults.standard.bool(forKey: "Login")
                            {
                            DispatchQueue.main.async {

                            let appDelegate = UIApplication.shared.delegate as? AppDelegate
                            appDelegate?.playSound()
                                
                            DispatchQueue.main.async {

                            var imageDataDict:[String: Any] = arrayData[0] as! [String : Any]
                            imageDataDict["array"] = arrayData
                            imageDataDict["lat"] = self.latitudeOn
                            imageDataDict["log"] = self.longitudeOn

                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "New_Request_Show"), object: nil, userInfo: imageDataDict)
                            
                            }
                        }
                    }
                }
                else{
                    if UserDefaults.standard.bool(forKey: "Login")
                        {
                          if arrayData.count > 0 {
                            DispatchQueue.main.async {
                                for i in 0..<arrayData.count
                                {
                                    var imageDataDict:[String: Any] = arrayData[i] as! [String : Any]
                                    imageDataDict["lat"] = self.latitudeOn
                                    imageDataDict["log"] = self.longitudeOn

                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Parcel_Array_Update"), object: nil, userInfo: imageDataDict)
                                    }
                                   }
                                }
                            }
                        }
                    }
                }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    
    //MARK:- Send Current Location By Driver
    func currentLocationData(requestParams : [String:Any], completion: @escaping (_ object:[String:Any], _ status : Bool, _ mess: String, _ str: String) -> Void) {
            let url = "BaseURL".DriverOnlineURL
            var userModel = UserModel()
            let sessionManager = SessionManager()
            userModel = sessionManager.getUserDetails()

        AlamofireRequest.shared.PostBodyForRawData_OnLine(urlString: url, parameters: requestParams, authToken: userModel.accessToken, isLoader: false, loaderMessage: "") { (data,error) in
                
                if error == nil{
                    if let status = data?["status"] as? Bool{
                        
                        print(data?["message"] ?? "")
                        print(convertTimeFormaterS(date: Date()))
                        
                        completion(data!,status,"","")
                    }
                    else{
                        completion(data!,false,"","")
                    }
                }
            }
        }
    

    //MARK:-  Add Driver Online Offline Api
    func AddDriver_OnlineRequest(userOnline: Bool) -> Void {
        let url = "BaseURL".AddDriverWorkStatus
       // print("URL - ",url)
        var userModel = UserModel()
        let sessionManager = SessionManager()
        userModel = sessionManager.getUserDetails()
        
        var urlString = String()
        let boolAsString = String(userOnline)

        urlString  =  String(format: "%@?driverId=%d&Online=%@",url,userModel.userID,boolAsString)
    
        AlamofireRequest.shared.PostBodyFrom_onLine(urlString:urlString,isLoader:false, param:nil,loaderMessage: "", auth:userModel.accessToken, isNetwork: true, true) { (data,error) in
            
            if error == nil{
                if let status = data?["status"] as? Bool{
                    if userOnline {
                        ToastMessage.showPositiveMessage(message: "Open hours")
                        if status {
                            self.OnlineUserRequest(userOnline: true)
                        }
                    }
                    else{
                        ToastMessage.showNegativeMessage(message: "Closed hours")
                    }
                }
            }
        }
    }
    
    
    
    
    //MARK:-  Get New Request Api
//    func GetRequestNoticeForDrivers(getRequest: Bool) -> Void {
//        let url = "BaseURL".GetRequestNoticeForTopDrivers
//        var userModel = UserModel()
//        let sessionManager = SessionManager()
//        userModel = sessionManager.getUserDetails()
//        var urlString = String()
//        urlString  =  String(format: "%@?driverId=%d",url,userModel.userID)
//
//        AlamofireRequest.shared.PostBodyFrom_onLine(urlString:urlString,isLoader:false, param:nil,loaderMessage: "", auth:userModel.accessToken,isNetwork: false,false) { (data,error) in
//
//            if error == nil{
//
//                if let status = data?["status"] as? Bool{
//                    if let arrayData = data?["data"] as? NSArray {
//                        if arrayData.count > 0 {
//
//                            print("Request Found",arrayData)
//                            if !self.sessionManager.getFirstRequestData_BOOL(){
//                                if UserDefaults.standard.bool(forKey: "Login")
//                                {
//                                DispatchQueue.main.async {
//
//                                let appDelegate = UIApplication.shared.delegate as? AppDelegate
//                                appDelegate?.playSound()
//
//                                DispatchQueue.main.async {
//
//                                var imageDataDict:[String: Any] = arrayData[0] as! [String : Any]
//                                imageDataDict["array"] = arrayData
//                                imageDataDict["lat"] = self.latitudeOn
//                                imageDataDict["log"] = self.longitudeOn
//
//                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "New_Request_Show"), object: nil, userInfo: imageDataDict)
//
//                                }
//                            }
//                        }
//                    }
//                    else{
//                        if UserDefaults.standard.bool(forKey: "Login")
//                            {
//                              if arrayData.count > 0 {
//                                DispatchQueue.main.async {
//                                    for i in 0..<arrayData.count
//                                    {
//                                        var imageDataDict:[String: Any] = arrayData[i] as! [String : Any]
//                                        imageDataDict["lat"] = self.latitudeOn
//                                        imageDataDict["log"] = self.longitudeOn
//
//                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Parcel_Array_Update"), object: nil, userInfo: imageDataDict)
//                                        }
//                                       }
//                                    }
//                                }
//                            }
//                        }
//                        else{
//
//                            var stringDate = ""
//                            stringDate = String(format: "No new requests found near your location.-- %@",convertTimeFormaterS(date: Date()))
//                            print(stringDate)
//                        }
//                    }
//                    else{
//
//
//                        var stringDate = ""
//                        stringDate = String(format: "No new requests found near your location.-- %@",convertTimeFormaterS(date: Date()))
//                        print(stringDate)
//                    }
//                }
//            }
//        }
//    }
}
