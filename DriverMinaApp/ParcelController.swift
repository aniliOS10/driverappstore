//
//  ParcelController.swift
//  DriverMinaApp
//
//  Created by Apple on 03/03/21.
//

import UIKit

class ParcelController: UIViewController {

    
    @IBOutlet weak var viewTop_Constraint: NSLayoutConstraint!
    @IBOutlet weak var tableView_Parcel: UITableView?
    let sessionManager = SessionManager()
    var userModel = UserModel()
    var int_IndexCount = 1
    var arrayDataGet = [[String: Any]]()
    var noDuplicatesArray = [[String: Any]]()
    var latitudeOn = 0.0
    var longitudeOn = 0.0
    
    
    // MARK: viewDidLoad-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sessionManager.saveFirstRequestData_BOOL(isBool:true)
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height - 100 - 247 - 80
        self.viewTop_Constraint.constant = CGFloat(screenHeight)
        self.view.layoutIfNeeded()
        notificationFor_Parcel_Array_Update()
        if int_IndexCount > 1 {
            updateTableViewUI()
        }
        
        // MARK: Duplicates data Remove
        noDuplicatesArray = self.noDuplicates(arrayDataGet)
        int_IndexCount = noDuplicatesArray.count
        if int_IndexCount > 1 {
            updateTableViewUI()
        }
        
        tableView_Parcel?.reloadData()
    }
    
    func noDuplicates(_ arrayOfDicts: [[String: Any]]) -> [[String: Any]] {
        var noDuplicates = [[String: Any]]()
        var usedNames = [String]()
        
        for dict in arrayOfDicts {
            var name = "0"
            var i = Int()
            i = dict["requestId"] as? Int ?? 0
            name = String(i)
            if !usedNames.contains(name) {
                noDuplicates.append(dict)
                usedNames.append(name)
            }
            else{
                print("No Data")
            }
        }
        return noDuplicates
    }
    
  
    func notificationFor_Parcel_Array_Update(){

        NotificationCenter.default.addObserver(self, selector: #selector(self.Parcel_Array_Update), name: NSNotification.Name(rawValue: "Parcel_Array_Update"), object: nil)
    }
    
    @objc func Parcel_Array_Update(notification: NSNotification) {
        
        if let dict = notification.userInfo{
            
            var dictData = NSDictionary()
            dictData = dict as NSDictionary
            latitudeOn = dictData["lat"] as! Double
            longitudeOn =  dictData["log"] as! Double
            noDuplicatesArray.append(dictData as! [String : Any])
            // MARK: Duplicates data Remove
            noDuplicatesArray = self.noDuplicates(noDuplicatesArray)

            int_IndexCount = noDuplicatesArray.count
            if int_IndexCount > 1 {
                updateTableViewUI()
            }
            tableView_Parcel?.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.sessionManager.saveFirstRequestData_BOOL(isBool:false)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction fileprivate func Count_Action(_ sender: Any) {
    }
    
    
    func updateTableViewUI(){
        
        let screenSize = UIScreen.main.bounds
        var screenHeight = screenSize.height - 100 - 75
        screenHeight = screenHeight - CGFloat((245 * int_IndexCount))
        
        if screenHeight > 40 {
        if int_IndexCount < 10 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                self.viewTop_Constraint.constant = CGFloat(screenHeight)
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    else{
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.viewTop_Constraint.constant = CGFloat(1)
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
        tableView_Parcel?.reloadData()
    }
}


//MARK: - TableViewDataSource Delegate
extension ParcelController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noDuplicatesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "NewRequestTableViewCell", for: indexPath) as? NewRequestTableViewCell else {
            return NewRequestTableViewCell()
        }
        
        cellOf.selectionStyle = .none
        
        var dict = NSDictionary()
        if noDuplicatesArray.count > indexPath.row {
            dict = noDuplicatesArray[indexPath.row] as NSDictionary
            cellOf.lbe_Pickup.text = dict["senderAddress"] as? String
            cellOf.lbe_Delivery.text = dict["receiverAddress"] as? String
            cellOf.lbe_Distance.text = dict["distance"] as? String
            cellOf.lbe_Duration.text = dict["duration"] as? String
            cellOf.lbe_SerType.text = dict["deliveryType"] as? String
            
            if cellOf.lbe_SerType.text == "Normal"{
                cellOf.lbe_SerType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
            }
            else if cellOf.lbe_SerType.text == "Express"{
                cellOf.lbe_SerType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
            }
        }
        
        
        cellOf.btn_Details.tag = indexPath.row
        cellOf.btn_Details.addTarget(self, action: #selector(action_ParcelDetails), for: .touchUpInside)
        
        cellOf.btn_Accept.tag = indexPath.row
        cellOf.btn_Accept.addTarget(self, action: #selector(action_ParcelAccepted), for: .touchUpInside)
        
        cellOf.btn_Reject.tag = indexPath.row
        cellOf.btn_Reject.addTarget(self, action: #selector(action_ParcelReject), for: .touchUpInside)
        return cellOf
    }
    
    
    @objc func action_ParcelDetails(sender: UIButton){
        
    if noDuplicatesArray.count > sender.tag {
        var dict = NSDictionary()
            dict = noDuplicatesArray[sender.tag] as NSDictionary
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ParcelDetailsRequestController") as! ParcelDetailsRequestController
        vc.requestID = dict["requestId"] as? Int ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc func action_ParcelAccepted(sender: UIButton){
        
        userModel = sessionManager.getUserDetails()
        
        var dict = NSDictionary()
        if noDuplicatesArray.count > sender.tag {
            dict = noDuplicatesArray[sender.tag] as NSDictionary
            
        var Data: Dictionary<String, Any> = [:]
            Data["driverId"] = userModel.userID
            Data["requestId"] = dict["requestId"] as? Int
            Data["driverLat"] = latitudeOn
            Data["driverLong"] =  longitudeOn
            Data["hasAccepted"] = true
            
            parcelAccepted_API(Params:Data, index: sender.tag, isAccepted: true)
        }
    }
    
    @objc func action_ParcelReject(sender: UIButton){
        
        userModel = sessionManager.getUserDetails()
        
        var dict = NSDictionary()
        if noDuplicatesArray.count > sender.tag {
            dict = noDuplicatesArray[sender.tag] as NSDictionary
            
        var Data: Dictionary<String, Any> = [:]
            Data["driverId"] = userModel.userID
            Data["requestId"] = dict["requestId"] as? Int
            Data["driverLat"] = latitudeOn
            Data["driverLong"] =  longitudeOn
            Data["hasAccepted"] = false
            
            parcelAccepted_API(Params:Data, index: sender.tag, isAccepted: false)
        }
    }
}

    
//MARK: - TableViewDelegate Delegate
extension ParcelController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 237
     }
}


//MARK: - API -
extension ParcelController{


    func parcelAccepted_API(Params:[String: Any], index: Int,isAccepted:Bool){
        
        
        ParcelAccepted.shared.ParcelAcceptedData(requestParams: Params, true) { (obj,status,mess) in
        
            
        if isAccepted {
    
            if status{
                DispatchQueue.main.async {
                let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                    
                    if self.noDuplicatesArray.count == 1 {
                        self.sessionManager.saveFirstRequestData_BOOL(isBool:false)
                        self.dismiss(animated: true, completion: nil)
                    }
                    else{
                        if self.noDuplicatesArray.count > index {
                            self.noDuplicatesArray.remove(at: index)
                            self.tableView_Parcel?.reloadData()
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)

                }
            }
            else{
                
                if let code = obj?["code"] as? Int{
                    if code == 201{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                                if self.noDuplicatesArray.count == 1 {
                                    self.sessionManager.saveFirstRequestData_BOOL(isBool:false)
                                    self.dismiss(animated: true, completion: nil)
                                }
                                else{
                                    if self.noDuplicatesArray.count > index {
                                        self.noDuplicatesArray.remove(at: index)
                                        self.tableView_Parcel?.reloadData()
                                    }
                                }
                            }))
                         
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                                if self.noDuplicatesArray.count == 1 {
                                    self.sessionManager.saveFirstRequestData_BOOL(isBool:false)
                                    self.dismiss(animated: true, completion: nil)
                                }
                                else{
                                    if self.noDuplicatesArray.count > index {
                                        self.noDuplicatesArray.remove(at: index)
                                        self.tableView_Parcel?.reloadData()
                                    }
                                }
                            }))
                         
                            self.present(alert, animated: true, completion: nil)
                        }

                    }
                }
                else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                            if self.noDuplicatesArray.count == 1 {
                                self.sessionManager.saveFirstRequestData_BOOL(isBool:false)
                                self.dismiss(animated: true, completion: nil)
                            }
                            else{
                                if self.noDuplicatesArray.count > index {
                                    self.noDuplicatesArray.remove(at: index)
                                    self.tableView_Parcel?.reloadData()
                                }
                            }
                        }))
                     
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        else{
            if self.noDuplicatesArray.count == 1 {
                self.sessionManager.saveFirstRequestData_BOOL(isBool:false)
                self.dismiss(animated: true, completion: nil)
            }
            else{
                if self.noDuplicatesArray.count > index {
                    self.noDuplicatesArray.remove(at: index)
                    self.tableView_Parcel?.reloadData()
                }
            }
        }
    }
}
}
