//
//  SettingVc.swift
//  DriverMinaApp
//
//  Created by Apple on 23/04/21.
//

import UIKit

class SettingVc: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        topViewLayout()
    }
    
    func topViewLayout(){
        if !SettingVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction fileprivate func ChangePass_Action(_ sender: Any) {

        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"ChangePasswordVc") as! ChangePasswordVc
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
