//
//  OrderTrackProgressController.swift
//  DriverMinaApp
//
//  Created by Apple on 23/03/21.
//

import UIKit
import MapKit
import CoreLocation

class OrderTrackProgressController: UIViewController,MKMapViewDelegate{
    
    
    var requestID = 0
    var orderNumber = 0
    let sessionManager = SessionManager()
    var acceptOrderArray : [GetJobProgressModel] = []
    var objectParcelDetails:GetParcelDetailsClass?

    @IBOutlet weak var lbe_Delivery: UILabel!
    @IBOutlet weak var lbe_Distance: UILabel!
    @IBOutlet weak var lbe_Duration: UILabel!
    @IBOutlet weak var lbe_SType: UILabel!
    @IBOutlet weak var lbe_Status: UILabel!
    @IBOutlet weak var lbe_DateTime: UILabel!
    @IBOutlet weak var lbe_OrderNo: UILabel!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    
    @IBOutlet weak var btn_GoForDelivery: UIButton!
    @IBOutlet weak var lbe_GoForDelivery: UILabel!
    @IBOutlet weak var view_GoForDelivery: UIView!

    @IBOutlet weak var btn_SendOTP: UIButton!
    @IBOutlet weak var lbe_SendOTP: UILabel!
    @IBOutlet weak var view_SendOTP: UIView!
    @IBOutlet weak var lbe_textSendOTP: UILabel!

    @IBOutlet weak var btn_Delivered: UIButton!
    @IBOutlet weak var view_Delivered: UIView!
    @IBOutlet weak var lbe_Delivered: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        dataShowOnView()
        ParcelDetailsAPIData(true)
        
       NotificationCenter.default.addObserver(self, selector: #selector(self.Verify_OTP_DataReload), name: NSNotification.Name(rawValue: "Verify_OTP_DataReload"), object: nil)
    }
    
    func topViewLayout(){
        if !ParcelDetailsRequestController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    @objc func Verify_OTP_DataReload(_ notification: NSNotification) {
              ParcelDetailsAPIData(true)
    }
    
    func ParcelDetailsAPIData(_ loader:Bool){
        
        GetParcelDetailsRequest.shared.getParcelDetailsRequestData(requestId:requestID,loader) { (object,status)  in

            if status {
                if object?.data != nil {
                
                    DispatchQueue.main.async { [unowned self] in
                    self.objectParcelDetails = object?.data
                        if self.objectParcelDetails?.listDeliveryDetails != nil{
                            orderTrack_ListShow()
                    }
                }
            }
        }
        else{
                self.MessageAlert(title: "", message:"Something went wrong")
            }
        }
    }
    
    func dataShowOnView(){
        if self.acceptOrderArray.count > 0 {
            let data : GetJobProgressModel = self.acceptOrderArray[0]
            self.lbe_Duration.text = data.duration
            self.lbe_Distance.text = data.distance
            self.lbe_SType.text = data.deliveryType
            self.lbe_OrderNo.text = String(orderNumber)
            self.lbe_DateTime.text = data.notifyDate + ", " + data.notifyTime
         // self.lbe_Pickup.text = data.senderAddress
            self.lbe_Delivery.text = data.receiverAddress
            self.lbe_Status.text = data.status
            self.lbe_textSendOTP.text = String(format: "Generate code for delivered- %@", data.receiverPhoneNumber)
            if data.deliveryType == "Normal"{
                lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
            }
            else if data.deliveryType == "Express"{
                lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func Direction_Action(_ sender: Any) {
        alertSheet()
    }
    
    
    @IBAction fileprivate func ParcelDetails_Action(_ sender: Any) {
        
        let storyBoard_Home = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard_Home.instantiateViewController(withIdentifier: "ParcelDetailsRequestController") as! ParcelDetailsRequestController;        controller.requestID = requestID
               controller.isHide_Action = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    @IBAction fileprivate func goForDelivery_Action(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "Call_Api_Progress")

        var dict: Dictionary<String, Any> = [:]
            dict["driverId"] = sessionManager.getUserDetails().userID
            dict["requestId"] = requestID
            dict["statusId"] = 4
            dict["driverLat"] = Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            dict["driverLong"] = Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            print(dict)

        DeliveryStatusDetailsRequest.shared.deliveryStatusData(requestParams: dict, true) { (obj,status,msg,strAny) in
        
            if status {
                self.alertViewSuccess(title: "Done", mess: msg)
            }
            else{
                
            }
        }
    }
    
    @IBAction fileprivate func sendOTP_Action(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "Call_Api_Progress")

        var dict: Dictionary<String, Any> = [:]
            dict["driverId"] = sessionManager.getUserDetails().userID
            dict["requestId"] = requestID
            dict["statusId"] = 5
            dict["driverLat"] = Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            dict["driverLong"] = Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")

        DeliveryStatusDetailsRequest.shared.deliveryStatusData(requestParams: dict, true) { (obj,status,msg,strAny) in
        
            if status {
                if self.acceptOrderArray.count > 0 {
                let data : GetJobProgressModel = self.acceptOrderArray[0]
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "RecieverVerifyController") as! RecieverVerifyController
                    vc.orderNumber = data.orderId
                    vc.phoneNumber = data.receiverPhoneNumber
                    vc.requestID = data.requestId
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                let window = appDelegate?.window
                window?.rootViewController?.present(vc, animated: true, completion: nil)
                }
            }
            else{
                
            }
        }
    }
    
    @IBAction fileprivate func delivered_Action(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "Call_Api_Progress")

        var dict: Dictionary<String, Any> = [:]
            dict["driverId"] = sessionManager.getUserDetails().userID
            dict["requestId"] = requestID
            dict["statusId"] = 6
            dict["driverLat"] = Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            dict["driverLong"] = Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            print(dict)

        DeliveryStatusDetailsRequest.shared.deliveryStatusData(requestParams: dict, true) { (obj,status,msg,strAny) in
            
            if status {
                self.alertViewSuccess(title: "Done", mess: msg)
            }
            else{
                
            }
        }
    }
    
    
    func alertViewSuccess(title:String,mess:String){
           let alert = UIAlertController(title: title, message:mess, preferredStyle: .alert)
           
           let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
                 self.ParcelDetailsAPIData(true)

           })
           alert.addAction(ok)
           
           DispatchQueue.main.async(execute: {
               self.present(alert, animated: true)
           })
       }
    
    
    func orderTrack_ListShow(){
        
        for i in 0..<(self.objectParcelDetails?.listDeliveryDetails!.count)!
        {
           let data  = self.objectParcelDetails?.listDeliveryDetails![i]
            
            if data?.statusID == 1 {
                
            }
            if data?.statusID == 2 {
                
            }
            if data?.statusID == 3 {
                
                btn_SendOTP.isUserInteractionEnabled = false
                btn_SendOTP.alpha = 0.5

                btn_Delivered.isUserInteractionEnabled = false
                btn_Delivered.alpha = 0.5
                
            }
            if data?.statusID == 4 {
               
                btn_SendOTP.alpha = 1
                btn_SendOTP.isUserInteractionEnabled = true

                lbe_GoForDelivery.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).bluePlan(Alpha: 1)
                view_GoForDelivery.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).bluePlan(Alpha: 1)
                
                btn_GoForDelivery.isUserInteractionEnabled = false
                btn_GoForDelivery.alpha = 0.5
                
                lbe_textSendOTP.textColor = UIColor.black

            }
            if data?.statusID == 5 {
                
                btn_Delivered.alpha = 1
                
                lbe_GoForDelivery.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).bluePlan(Alpha: 1)
                view_GoForDelivery.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
                
                lbe_SendOTP.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
                view_SendOTP.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
            
                btn_SendOTP.isUserInteractionEnabled = false
                btn_SendOTP.alpha = 0.5
                
                btn_Delivered.isUserInteractionEnabled = true
                btn_Delivered.alpha = 1
                
                btn_GoForDelivery.isUserInteractionEnabled = false
                btn_GoForDelivery.alpha = 0.5
                
            }
            if data?.statusID == 6 {
               
                btn_Delivered.alpha = 1
                btn_Delivered.isUserInteractionEnabled = true

                lbe_GoForDelivery.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
                view_GoForDelivery.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
                
                lbe_SendOTP.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
                view_SendOTP.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
                
                view_Delivered.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).bluePlan(Alpha: 1)
            
                btn_SendOTP.isUserInteractionEnabled = false
                btn_SendOTP.alpha = 0.5
                
                btn_GoForDelivery.isUserInteractionEnabled = false
                btn_GoForDelivery.alpha = 0.5
                
                lbe_Delivered.textColor = UIColor.black

            }
        }
    }
    
    //MARK:-   Map Alert Function
    
    func alertSheet(){
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Choose application", message: "" , preferredStyle: .actionSheet)
        
            let MapButton = UIAlertAction(title: "Maps", style: .default) { _ in
                
                self.openAppleMap()
            
            }
            actionSheetControllerIOS8.addAction(MapButton)
            
            let GoogleButton = UIAlertAction(title: "Google Maps", style: .default)
            { _ in
                self.googleMap()
                
            }
            actionSheetControllerIOS8.addAction(GoogleButton)
            
            
            let CancelActionButton = UIAlertAction(title: "Cancel", style: .cancel)
            { _ in
                
            }
            actionSheetControllerIOS8.addAction(CancelActionButton)
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
            
            
            
        }else{
            openAppleMap()
        }
    }
    
    
    func googleMap(){
        
        if self.acceptOrderArray.count > 0 {
             let data : GetJobProgressModel = self.acceptOrderArray[0]
             let  tlatitude = Double(data.receiverLat)
             let  tlongitude = Double(data.receiverLong)
            
            let urlbase = URL(string: "comgooglemaps://?saddr=&daddr=\(tlatitude),\(tlongitude)")
           
            UIApplication.shared.open(urlbase!, options: [:]
               , completionHandler: nil)
            
        }
    }

    func openAppleMap() {
        
    if self.acceptOrderArray.count > 0 {
         let data : GetJobProgressModel = self.acceptOrderArray[0]
         let  tlatitude = Double(data.receiverLat)
         let  tlongitude = Double(data.receiverLong)
         let center = CLLocationCoordinate2D(latitude:tlatitude, longitude:tlongitude)
        openMapsAppWithDirections(to: center, destinationName: data.receiverAddress)
    }
}
    
    
    func openMapsAppWithDirections(to coordinate: CLLocationCoordinate2D, destinationName name: String) {
      let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
      let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = name
      mapItem.openInMaps(launchOptions: options)
    }
    
}

