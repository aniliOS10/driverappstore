//
//  SessionManager.swift
//  DriverMinaApp
//
//  Created by Apple on 09/03/21.
//

import Foundation
class SessionManager{
    
    
    let USER_MODEL = "userModel"
    let PersonalVehicleDetails = "PersonalVehicleDetails"
    let IS_LOGIN = "isLoggedIn"
    let IS_NEW_REQUEST_ClASS_SHOW = "isNewRequestClass"
    let defaults = UserDefaults.standard

    required init() {
        
    }
    
    func saveFirstRequestData_BOOL(isBool: Bool) -> Void{
        defaults.set(isBool, forKey: IS_NEW_REQUEST_ClASS_SHOW)
        defaults.synchronize()
    }
    
    func getFirstRequestData_BOOL() -> Bool {
        return defaults.bool(forKey: IS_NEW_REQUEST_ClASS_SHOW)
    }
    
    func createLoginSession(userModel: UserModel) -> Void {
        do {
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: userModel, requiringSecureCoding: false)
            
            defaults.set(encodedData, forKey: USER_MODEL)
            defaults.set(true, forKey: IS_LOGIN)
            defaults.synchronize()
    
           } catch {
               fatalError("Can't encode data: \(error)")
           }
    }
    
    
    func getUserDetails() -> UserModel {

        var userModel = UserModel()
            do {
                if let decoded  = defaults.object(forKey: USER_MODEL) as? Data{
                let unarchivedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! UserModel
                    userModel = unarchivedData
                }
                } catch {
                    print("didn't work")
            }
            return userModel
        }
    
    
    func createPersonal_VehicleDetails_Session(userModel: Personal_VehicleDetails) -> Void {
        do {
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: userModel, requiringSecureCoding: false)
            
            defaults.set(encodedData, forKey: PersonalVehicleDetails)
            defaults.synchronize()
    
           } catch {
               fatalError("Can't encode data: \(error)")
           }
    }
    
    
    func getPersonalVehicleDetails() -> Personal_VehicleDetails {

        var userModel = Personal_VehicleDetails()
            do {
                if let decoded  = defaults.object(forKey: PersonalVehicleDetails) as? Data{
                let unarchivedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! Personal_VehicleDetails
                    userModel = unarchivedData
                }
                } catch {
                    print("didn't work")
            }
            return userModel
        }
}
