//
//  ParcelDetailsRequestController.swift
//  DriverMinaApp
//
//  Created by Apple on 03/03/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit
import Lightbox

class ParcelDetailsRequestController: UIViewController,GMSMapViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate {

    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var table_HeightConst: NSLayoutConstraint!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var _scrollView: UIScrollView!

    @IBOutlet weak var lbe_Pickup: UILabel!
    @IBOutlet weak var lbe_Delivery: UILabel!
    @IBOutlet weak var lbe_Distance: UILabel!
    @IBOutlet weak var lbe_Duration: UILabel!
    @IBOutlet weak var lbe_Price: UILabel!
    @IBOutlet weak var lbe_SType: UILabel!
    @IBOutlet weak var lbe_PaymentBy: UILabel!
    @IBOutlet weak var lbe_ParcelName: UILabel!
    @IBOutlet weak var lbe_ParcelNotes: UILabel!
    
    @IBOutlet weak var btn_Confirm: UIButton!
    @IBOutlet weak var btn_Cancel: UIButton!

    
    @IBOutlet weak var mapView_Parcel: GMSMapView!

    
    var titleArray = [String]()
    var objectParcelDetails:GetParcelDetailsClass?
    var centerMapCoordinate:CLLocationCoordinate2D!
    
    var requestID = 0
    var isHide_Action = false

    let imgProfile = UIImageView()
    let imgProfileA = UIImageView()
    let sessionManager = SessionManager()
    var userModel = UserModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _scrollView.isHidden = true
        self.mapView_Parcel.delegate = self

        getParcelDetailsAPIData(true)
        topViewLayout()
        titleArray = ["Date & Time","Sender Name","Sender Phone","Reciever Name","Reciever Phone"]
        
        table_HeightConst.constant = CGFloat(titleArray.count * 51)
        self.view.layoutIfNeeded()
        
        if isHide_Action {
            btn_Confirm.isHidden = true
            btn_Cancel.isHidden = true
        }
    }
    
    func topViewLayout(){
        if !ParcelDetailsRequestController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @IBAction fileprivate func action_ParcelReject(_ sender: Any) {

    }
    
    @IBAction fileprivate func action_ParcelConfirm(_ sender: Any) {

        userModel = sessionManager.getUserDetails()

        var Data: Dictionary<String, Any> = [:]
            Data["driverId"] = userModel.userID
            Data["requestId"] = requestID
            Data["driverLat"] =  Double(UserDefaults.standard.string(forKey: "latitude_current") ?? "0.0")
            Data["driverLong"] =  Double(UserDefaults.standard.string(forKey: "longitude_current") ?? "0.0")
            Data["hasAccepted"] = true
            
            parcelAccepted_API(Params:Data, index:0, isAccepted: true)
    }

    @IBAction fileprivate func Back_Action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction fileprivate func image_Action(_ sender: Any) {
        self.imageTapped()
    }
    
    
    
    func parcelAccepted_API(Params:[String: Any], index: Int,isAccepted:Bool){
        
        ParcelAccepted.shared.ParcelAcceptedData(requestParams: Params, true) { (obj,status,mess) in
        
        if isAccepted {
    
            if status{
                DispatchQueue.main.async {
                let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                    
                }))
                self.present(alert, animated: true, completion: nil)

                }
            }
            else{
                
                if let code = obj?["code"] as? Int{
                    if code == 201{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                          
                            }))
                         
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                            }))
                         
                            self.present(alert, animated: true, completion: nil)
                        }

                    }
                }
                else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title:"", message:mess, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                        }))
                     
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        else{
        }
    }
}
    
    @objc func imageTapped()
    {

        if self.objectParcelDetails?.uovm != nil{

            DispatchQueue.main.async {
                let images = [
                    LightboxImage(
                        image:self.imgProfile.image!,
                        text: ""
                    ),
                    LightboxImage(
                        image:self.imgProfileA.image!,
                        text: ""
                    )
                ]
                
                let controller = LightboxController(images: images)
                    controller.dynamicBackground = true
                if let topController = UIApplication.topViewController() {
                    print(topController)
                    topController.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    

    func getParcelDetailsAPIData(_ loader:Bool){
        
        GetParcelDetailsRequest.shared.getParcelDetailsRequestData(requestId:requestID,loader) { (object,status)  in

            if status {
                if object?.data != nil {
                    
                    DispatchQueue.main.async {
                        self._scrollView.isHidden = false
                    }
                    DispatchQueue.main.async { 
                        self.objectParcelDetails = object?.data
                        if self.objectParcelDetails?.uovm != nil{
                            self.dataShow_OnView()
                    }
                }
            }
        }
        else{
                self._scrollView.isHidden = true
                self.MessageAlert(title: "", message:"Something went wrong")
            }
        }
    }
    
    func dataShow_OnView(){
        
        
        if let imgDataB = (self.objectParcelDetails?.uovm?.parcelImgBefore){
            let img = GlobalConstants.ImageBaseURL  + imgDataB
            let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            var strURL = String()
                strURL = escapedAddress ?? ""
            print(strURL)
            imgProfile.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))
        }
        
        if let imgDataA = (self.objectParcelDetails?.uovm?.parcelImgAfter){
            let img = GlobalConstants.ImageBaseURL  + imgDataA
            let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            var strURL = String()
                strURL = escapedAddress ?? ""
            print(strURL)
            imgProfileA.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))
        }
        
        
        lbe_Pickup.text = self.objectParcelDetails?.uovm?.sourceAddress
        lbe_Delivery.text = self.objectParcelDetails?.uovm?.destinationAddress
        lbe_SType.text = self.objectParcelDetails?.uovm?.deliveryType
        
        if self.objectParcelDetails?.uovm?.deliveryType == "Normal"{
            lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        }
        else if self.objectParcelDetails?.uovm?.deliveryType == "Express"{
            lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
        }
        
        lbe_Price.text =  String((self.objectParcelDetails?.uovm?.price ?? 45))
        lbe_Price.text = "R " + lbe_Price.text!
        lbe_Duration.text = self.objectParcelDetails?.uovm?.duration
        lbe_Distance.text = self.objectParcelDetails?.uovm?.distance
        lbe_ParcelName.text = self.objectParcelDetails?.uovm?.parcelName
        lbe_ParcelNotes.text = self.objectParcelDetails?.uovm?.parcelNotes
        self.tableViewList.reloadData()
        
        
        guard let latS = self.objectParcelDetails?.uovm?.sourceLat,
              let logS = self.objectParcelDetails?.uovm?.sourceLong else { return }
        
        guard let destinationLat = self.objectParcelDetails?.uovm?.destinationLat,
              let destinationLong = self.objectParcelDetails?.uovm?.destinationLong else { return }
        
        let cl_lat =   CLLocationCoordinate2D(latitude: Double(latS) ?? 00.00, longitude: Double(logS) ?? 00.00).latitude
        
        let cl_long = CLLocationCoordinate2D(latitude: Double(latS) ?? 00.00, longitude: Double(logS) ?? 00.00).longitude
        
        
        let cl_lat_2 =   CLLocationCoordinate2D(latitude: Double(destinationLat) ?? 00.00, longitude: Double(destinationLat) ?? 00.00).latitude
        
        let cl_long_2 = CLLocationCoordinate2D(latitude: Double(destinationLat) ?? 00.00, longitude: Double(destinationLong) ?? 00.00).longitude

        let camera = GMSCameraPosition.camera(withLatitude:cl_lat,longitude:cl_long, zoom:12.5)
    
        self.mapView_Parcel.animate(to: camera)
        
        ParcelDetails_DrawLine(SourceLat: latS, SourceLong:logS, DestinationLat: destinationLat, DestinationLong:destinationLong)
    }
    

  func ParcelDetails_DrawLine(SourceLat:String , SourceLong: String , DestinationLat: String , DestinationLong : String)
    {
        let Pickup = GMSMarker()
        Pickup.position = CLLocationCoordinate2D(latitude: Double(SourceLat) ?? 00.00, longitude: Double(SourceLong) ?? 00.00)
        Pickup.icon = UIImage(named: "cir_blue_point")
        Pickup.title = "Pickup"
        Pickup.map = mapView_Parcel
    
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(Pickup.position)
    
        let Delivery = GMSMarker()
        Delivery.position = CLLocationCoordinate2D(latitude: Double(DestinationLat) ?? 00.00, longitude: Double(DestinationLong) ?? 00.00 )
        Delivery.icon = UIImage(named: "squar_green_point")
        Delivery.title = "Delivery"
        Delivery.map = mapView_Parcel
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.drawLine(SourceLat: SourceLat, SourceLong: SourceLong, DestinationLat: DestinationLat, DestinationLong: DestinationLong)
        }
    }
    
    
    func drawLine(SourceLat:String , SourceLong: String , DestinationLat: String , DestinationLong : String)
    {

        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let origin = "\(SourceLat),\(SourceLong)"
        let destination = "\(DestinationLat),\(DestinationLong)"

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=driving&key=AIzaSyBKjkZ9biB8ye5_kUJaYl2YYYCLDCkzM8s")!
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {

                        if let preRoutes = json["routes"] as? NSArray {
                        if preRoutes.count > 0 {
                        let routes = preRoutes[0] as! NSDictionary
                        let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                        let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                        DispatchQueue.main.async(execute: {
                            let path = GMSPath(fromEncodedPath: polyString)
                            let polyline = GMSPolyline(path: path)
                            polyline.strokeWidth = 3
                            polyline.strokeColor = UIColor.black
                            polyline.map = self.mapView_Parcel
                            
                            if self.mapView_Parcel != nil
                            {
                             let bounds = GMSCoordinateBounds(path: path!)
                             self.mapView_Parcel!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                            }
                        
                        })
                        }
                        }
                    }
                } catch {
                    print("parsing error")
                }
            }
        })
        task.resume()
    }

}

//MARK: - TableViewDataSource Delegate
extension ParcelDetailsRequestController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "ParcelDetailsListTableViewCell", for: indexPath) as? ParcelDetailsListTableViewCell else {
            return ParcelDetailsListTableViewCell()
        }
        
        cellOf.selectionStyle = .none
        cellOf.lbe_Title.text = titleArray[indexPath.row]
        
        if indexPath.row == 0 {
            
            if  let time = self.objectParcelDetails?.uovm?.orderCreatedTime {
                if let date = self.objectParcelDetails?.uovm?.orderCreatedDate {
                    if date != "" {
                        cellOf.lbe_Data.text = date + ", " + time
                    }
                }
                else{
                    cellOf.lbe_Data.text = time
                }
            }
        }
        if indexPath.row == 1 {
            cellOf.lbe_Data.text = self.objectParcelDetails?.uovm?.senderName
        }
        if indexPath.row == 2 {
            cellOf.lbe_Data.text = self.objectParcelDetails?.uovm?.senderPhoneNo
        }
        if indexPath.row == 3 {
            cellOf.lbe_Data.text = self.objectParcelDetails?.uovm?.receiverName
        }
        if indexPath.row == 4 {
            cellOf.lbe_Data.text = self.objectParcelDetails?.uovm?.receiverMobileNumber
        }
        return cellOf
    }
    
    
    @objc func action_ParcelDetails(sender: UIButton){
       // let buttonTag = sender.tag
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ParcelDetailsRequestController") as! ParcelDetailsRequestController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

    
//MARK: - TableViewDelegate Delegate
extension ParcelDetailsRequestController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
     }
}
extension MKMapView {
    func topLeftCoordinate() -> CLLocationCoordinate2D {
        return convert(.zero, toCoordinateFrom: self)
    }

    func bottomRightCoordinate() -> CLLocationCoordinate2D {
        return convert(CGPoint(x: frame.width, y: frame.height), toCoordinateFrom: self)
    }
}
