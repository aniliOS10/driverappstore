//
//  CompletedJobController.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit

class CompletedJobController: UIViewController  {
    
    @IBOutlet weak var tableViewCompletedJobs: UITableView!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    
    let refreshControl = UIRefreshControl()
    var acceptDataArray : [GetCompletedModel] = []
    var acceptArrayFilter : [GetCompletedModel] = []
    
    @objc func RefreshScreen() {
        if AlamofireRequest.shared.InterNetConnection()
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.getRequestAcceptedList(false)
            }
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        self.getRequestAcceptedList(true)

        refreshControl.addTarget(self, action:  #selector(RefreshScreen), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        
        if #available(iOS 10.0, *) {
            tableViewCompletedJobs.refreshControl = refreshControl
            } else {
            tableViewCompletedJobs.addSubview(refreshControl)
        }
    }
    
    func topViewLayout(){
        if !CompletedJobController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction fileprivate func filter_Action(_ sender: Any) {

        filterData()
    }
    
    func filterData(){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "All", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
        
        }))
        
        alert.addAction(UIAlertAction(title: "Normal", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            
        }))

        alert.addAction(UIAlertAction(title: "Express", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func getRequestAcceptedList(_ loader:Bool) {
        
        GetCompletedWorkRequest.shared.GetCompletedModelData(requestParams:[:], loader) { (obj,status) in
            DispatchQueue.main.async {
            if status{
                self.refreshControl.endRefreshing()
                self.acceptDataArray = obj!
                self.acceptArrayFilter = obj!
                self.tableViewCompletedJobs.reloadData()
                
                if self.acceptDataArray.count == 0 {
                    
                }
                else{
                }
            }
            else{
                self.refreshControl.endRefreshing()
                self.acceptDataArray.removeAll()
                self.acceptArrayFilter.removeAll()
                self.tableViewCompletedJobs.reloadData()
        }
      }
    }
  }
    
    func dynamicFontSize(_ FontSize: CGFloat) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.size.width
        let calculatedFontSize = screenWidth / 375 * FontSize
        return calculatedFontSize
    }
}


//MARK: - TableViewDataSource Delegate
extension CompletedJobController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.acceptArrayFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "CompletedTableViewCell", for: indexPath) as? CompletedTableViewCell else {
            return CompletedTableViewCell()
        }

        cellOf.selectionStyle = .none
        
        let data : GetCompletedModel = self.acceptArrayFilter[indexPath.row]
        
        cellOf.dataShow_OnView(data:data)
        cellOf.lbe_DateTime.text = data.notifyDate + ", " + data.notifyTime
        cellOf.lbe_SType.text = data.deliveryType
        cellOf.lbe_DeliveryPrice.text = String(format: "R %d", data.deliveryPrice)

        if data.deliveryType == "Normal"{
            cellOf.lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        }
        else if data.deliveryType == "Express"{
            cellOf.lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
        }

        return cellOf
    }
    
        
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label = UILabel ()
        label.frame = CGRect.init(x: 0, y: 0, width:width, height: CGFloat.greatestFiniteMagnitude)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        let style = NSMutableParagraphStyle()
            style.lineSpacing = 4
            style.alignment = .left
        
            label.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle: style])
            label.sizeToFit()
        
        let maxSize = CGSize(width:width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
    
      //  print("LINE-",CGFloat(linesRoundedUp))
        return CGFloat(linesRoundedUp * Int(charSize) + 25)
    }
}

    
//MARK: - TableViewDelegate Delegate
extension CompletedJobController : UITableViewDelegate
{
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)

    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height_Text: CGFloat = 30
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        var fontNew = UIFont()
   
        fontNew = .RegularFont(Float(dynamicFontSize(15)))
        
        let data : GetCompletedModel = self.acceptArrayFilter[indexPath.row]

        height_Text = heightForView(text:data.senderAddress, font: fontNew, width: screenWidth - 55)
        
        height_Text = heightForView(text:data.receiverAddress, font: fontNew, width: screenWidth - 55) + height_Text

        return height_Text + 90
    }
}


