//
//  RecieverVerifyController.swift
//  DriverMinaApp
//
//  Created by Apple on 19/03/21.
//

import UIKit

class RecieverVerifyController: UIViewController {

    var requestID = 0
    var orderNumber = 0
    var phoneNumber = ""
    let sessionManager = SessionManager()

    @IBOutlet weak var txt_Phone: UITextField!
    @IBOutlet weak var txt_OTP: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_Phone.text = String(phoneNumber)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction fileprivate func  Verify_Action(_ sender: Any){
        
        
        if txt_OTP.text?.count == 0{
            self.MessageAlert(title: "", message:"Please enter verify OTP")
            return
        }
        
        var dict: Dictionary<String, Any> = [:]
            dict["driverId"] = sessionManager.getUserDetails().userID
            dict["orderId"] = orderNumber
            dict["receiverPhoneNo"] = String(phoneNumber)
            dict["deliveryPIN"] = Int(txt_OTP.text ?? "00000")

        DeliveryVerifyPINRequest.shared.deliveryVerifyPINRequestData(requestParams: dict, true) { (obj,status,msg,strAny) in
        
            if status {
                DispatchQueue.main.async {
                let alert = UIAlertController(title:"", message:msg, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name("Verify_OTP_DataReload"), object:nil)

                }))
            
                self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                self.MessageAlert(title: "", message:msg)
            }
        }
    }
        
    
    @IBAction fileprivate func  resend_Action(_ sender: Any) {
        
        
    }
}
