//
//  YourRatingController.swift
//  DriverMinaApp
//
//  Created by Apple on 13/03/21.
//

import UIKit

class YourRatingController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var tableViewRating: UITableView!

    @IBOutlet weak var lbe_TotalRating: UILabel!

    var ratingDataArray : [DriverRatings] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        
        getRatingList(true)
        
    }
    func topViewLayout(){
        if !ParcelDetailsRequestController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getRatingList(_ loader:Bool) {
        
        GetDriverRatingsRequest.shared.GetRatingData(requestParams:[:], loader) { (obj,status,totalRating) in
            DispatchQueue.main.async {
            if status{
                self.ratingDataArray = obj!
                self.lbe_TotalRating.text = String(totalRating)
                self.tableViewRating.reloadData()
            }
            else{
                self.tableViewRating.reloadData()
        }
      }
    }
  }
}


//MARK: - TableViewDataSource Delegate
extension YourRatingController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratingDataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserReviewCell", for: indexPath) as? UserReviewCell else {
            return UserReviewCell()
        }
            
        cell.selectionStyle = .none
        
        let data : DriverRatings = self.ratingDataArray[indexPath.row]
        
        cell.lbe_Date.text = data.commentedAt
        cell.lbe_Review.text = data.review
        cell.lbe_Rating.text = String(data.rating)
        cell.lbe_UserName.text = data.userName
        
        return cell
        
    }
}
//MARK: - TableViewDelegate Delegate
extension YourRatingController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
}

