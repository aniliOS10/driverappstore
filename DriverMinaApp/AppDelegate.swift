//
//  AppDelegate.swift
//  DriverMinaApp
//
//  Created by Apple on 02/02/21.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController
import GooglePlaces
import GoogleMaps
import AVFoundation
import Firebase
import UserNotifications
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate{

    var window: UIWindow?
    var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
    var audioPlayer = AVAudioPlayer()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        UIApplication.shared.applicationIconBadgeNumber = 0

        FirebaseApp.configure()

        registerForPushNotifications()

        GMSServices.provideAPIKey("AIzaSyCn4NaEzeh7g-qW7xtkYewa8xv7LI1cdwU")
        GMSPlacesClient.provideAPIKey("AIzaSyCn4NaEzeh7g-qW7xtkYewa8xv7LI1cdwU")

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor.black
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        RootControllerManager().SetRootViewController()
        
        let sessionManager = SessionManager()
        sessionManager.saveFirstRequestData_BOOL(isBool:false)

        //Launched from push notification
        let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any]
        if remoteNotif != nil {
            let aps = remoteNotif!["aps"] as? [String:AnyObject]
            NSLog("\n Custom: \(String(describing: aps))")
        }
        else {
            NSLog("//////////////////////////Normal launch")
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print(url)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
       
        print("applicationWillResignActive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
        print("applicationDidEnterBackground")
        UIApplication.shared.setMinimumBackgroundFetchInterval(60)

        bgTask = application.beginBackgroundTask(withName:"MyBackgroundTask", expirationHandler: {() -> Void in
            application.endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskIdentifier.invalid
        })
    
        DispatchQueue.global(qos: .background).async {
           print("Call API Hrearrarararrarara")
        }
        // Perform your background task here
        print("The task has started")

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")

    }
    
    //MARK: -  @Register Application For Notification
    func registerForPushNotifications() {
             Messaging.messaging().delegate = self

            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
               
                // 1. Check if permission granted
                guard granted else { return }
                // 2. Attempt registration for remote notifications on the main thread
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
        //MARK: -  @DidRegister For Remote NotificationsWithDeviceToken
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            // 1. Convert device token to string
            let tokenParts = deviceToken.map { data -> String in
                return String(format: "%02.2hhx", data)
            }
            let token = tokenParts.joined()
            // 2. Print device token to use for PNs payloads
            print("Device Token: \(token)")
           
        }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            print("Failed to register for remote notifications with error: \(error)")
        }
        
         
        //MARK:-
        //MARK:- Push Notification Delegate

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
            print("Firebase registration token: \(fcmToken ?? "")")
            UserDefaults.standard.setValue(fcmToken, forKey: "DToken")
            UserDefaults.standard.synchronize()

         }

    func application(application: UIApplication,
                          didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
             Messaging.messaging().apnsToken = deviceToken as Data
         }

         
        
             
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        NSLog("Userinfo \(response.notification.request.content.userInfo)")
    
        let state: UIApplication.State = UIApplication.shared.applicationState
        if state == .background {
            
            // background
        }
        else if state == .active {
            
            // foreground
        }
        else if state == .inactive {
            
            // foreground
           // perform(#selector(MoveTonext), with: nil, afterDelay: 1)
        }
        }
             
             
    // MARK: - Received push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
             {
            print(#function)
            completionHandler([.alert, .badge, .sound])
        
             }
    
             
    // MARK: - Handle data of notification APP in BackGround
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            
             NSLog("Userinfo",userInfo)

             print(#function)
             
             completionHandler(.newData)
             
         }
    
    func playSound(){
        
        
        let alertSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "button-8", ofType: "wav")!)
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        
        try! AVAudioSession.sharedInstance().setActive(true)

        try! audioPlayer = AVAudioPlayer(contentsOf: alertSound as URL)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
}


class RootControllerManager: NSObject {
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    //MARK: -  @Set RootView Controller
    func SetRootViewController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if UserDefaults.standard.bool(forKey: "Login")
        {
            SideRootScreen()
        }
        else
        {
            PushToRootScreen(ClassName: storyboard.instantiateViewController(withIdentifier: "SignInController") as! SignInController)
        }
    }
    
    //MARK: -  @Push To RootController
    func PushToRootScreen(ClassName:UIViewController){
        let window = appDelegate?.window
        let nav = UINavigationController(rootViewController: ClassName)
        nav.isNavigationBarHidden = true
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }
    
    func SideRootScreen(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)

        let leftController = storyboard.instantiateViewController(withIdentifier: "SideMenuController")
                       
        let homecontroller = storyboard.instantiateViewController(withIdentifier: "HomeController")
        let navigationController = UINavigationController(rootViewController: homecontroller)
        navigationController.isNavigationBarHidden = true

        let sideMenu = LGSideMenuController(rootViewController: navigationController,
                                                             leftViewController: leftController,
                                                             rightViewController: nil)
                sideMenu.leftViewWidth = UIScreen.main.bounds.size.width - 80
                sideMenu.navigationController?.isNavigationBarHidden = true
                sideMenu.isLeftViewStatusBarHidden = false
                let del = UIApplication.shared.delegate as! AppDelegate
                del.window?.rootViewController = sideMenu
    }
}
