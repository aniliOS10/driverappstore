//
//  HomeController.swift
//  DriverMinaApp
//
//  Created by Apple on 04/02/21.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit

class HomeController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var img_View: UIImageView!

    @IBOutlet weak var view_onLineOne: UIView!
    @IBOutlet weak var view_onLineTwo: UIView!
   
    @IBOutlet weak var btn_onLine: UIButton!

    @IBOutlet weak var lbe_onLineStats: UILabel!
    
    @IBOutlet weak var view_MenuTopConst: NSLayoutConstraint!
    @IBOutlet weak var view_UserTopConst: NSLayoutConstraint!
    var sessionManager = SessionManager()

    let marker: GMSMarker = GMSMarker()
    let markerCar: GMSMarker = GMSMarker()

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    static let geoCoder = CLGeocoder()
    var timerOnLine = Timer()
    var isOnLine = Bool()
    
    var doubleS = Double()
    var doubleD = Double()
    var oldPolyLines = [GMSPolyline]()

    var coordinateArr = NSMutableArray()

    var counter = 0
    var oldCoordinate: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(40.7416627,-74.0049708);

    override func viewDidLoad() {
        super.viewDidLoad()
        
        topViewLayout()
        
        get_Location()
        
        startOnlineTimer()
        notificationFor_PushView()
    
        doubleS = 40.7416627
        doubleD = -74.0049708
    
//        DispatchQueue.main.async { [self] in
//            markerCar.position = CLLocationCoordinate2DMake(-33.92260081893561,18.431844714870937);
//            markerCar.icon = UIImage(named: "car")
//            markerCar.map = self.mapView
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        var userModel = UserModel()
        let sessionManager = SessionManager()
        userModel = sessionManager.getUserDetails()
        print(userModel.accessToken ?? "")
        GetDriverInfo()
    }
    
    func GetDriverInfo(){
        GetDriverInfoRequest.shared.GetDriverInfoData(requestParams:[:], false) { (obj,status) in
        
            if status {
                var userModelLogin = Personal_VehicleDetails()
                userModelLogin = obj!
                self.sessionManager.createPersonal_VehicleDetails_Session(userModel:userModelLogin)
            }
        }
    }
    
    
    func loadFileFromLocalPath(_ localFilePath: String) ->Data? {
        let filePath = Bundle.main.path(forResource: "coordinates", ofType: "json")
        let jsonData = NSData(contentsOfFile: filePath!) as Data?
        
        do {
            if let jsonData = jsonData {
                var data = NSArray()
                data = try JSONSerialization.jsonObject(with: jsonData, options: []) as! NSArray
                
                coordinateArr = NSMutableArray(array: data)
            }
        } catch {
        
        }
        
       return try? Data(contentsOf: URL(fileURLWithPath: localFilePath))
    }
    
    
    func notificationFor_PushView(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.MyJobsClass), name: NSNotification.Name(rawValue: "MyJobsClass"), object: nil)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.HomeTimer_Invalidate), name: NSNotification.Name(rawValue: "HomeTimer_Invalidate"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.New_Request_Show), name: NSNotification.Name(rawValue: "New_Request_Show"), object: nil)
    }
    
    
    @objc func New_Request_Show(_ notification: NSNotification) {
        
        if let dict = notification.userInfo{
            var dictData = NSDictionary()
            dictData = dict as NSDictionary
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ParcelController") as! ParcelController
            vc.arrayDataGet = dictData["array"] as! [[String : Any]]
            vc.int_IndexCount = 1
            vc.latitudeOn = dictData["lat"] as! Double
            vc.longitudeOn =  dictData["log"] as! Double
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
    @objc func HomeTimer_Invalidate(_ notification: NSNotification) {
            timerOnLine.invalidate()
            OnlineUserClass.shared.OnlineUserRequest(userOnline: false)
    }
    
    @objc func MyJobsClass(_ notification: NSNotification) {
        
            if let strData = notification.userInfo?["Class"] as? String {
                if strData == "MyJobsClass" {
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "MyJobsController")
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else if (strData == "Profile"){
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "ProfileController")
                self.navigationController?.pushViewController(controller, animated: true)
                }
            else if (strData == "JobProgress"){
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "JobProgressController")
                self.navigationController?.pushViewController(controller, animated: true)
                }
            else if (strData == "Notification"){
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "NotiListController")
                self.navigationController?.pushViewController(controller, animated: true)
                }
            else if (strData == "Completed"){
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "CompletedJobController")
                self.navigationController?.pushViewController(controller, animated: true)
                }
            else if (strData == "Settings"){
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "SettingVc")
                self.navigationController?.pushViewController(controller, animated: true)
                }
            else{
                let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "YourRatingController")
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func topViewLayout(){
        if !HomeController.hasSafeArea{
            if view_MenuTopConst != nil {
                view_MenuTopConst.constant = 27
            }
            
            if view_UserTopConst != nil {
                view_UserTopConst.constant = 27
            }
        }
        
//        drawLine(SourceLat: "-33.9461634499182", SourceLong: "18.467647217913164", DestinationLat: "-33.976774773922564", DestinationLong: "18.641540191581537")
        
    }
    
    func startOnlineTimer(){
        timerOnLine.invalidate()
        timerOnLine = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(timerAction_Online), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction_Online() {
        if isOnLine {
            OnlineUserClass.shared.OnlineUserRequest(userOnline: true)
        }
        else{
            OnlineUserClass.shared.OnlineUserRequest(userOnline: false)
        }

//        if counter < coordinateArr.count {
//           var dict = NSDictionary()
//            dict = coordinateArr[counter] as! NSDictionary
//            doubleS = dict["lat"] as! Double
//            doubleD = dict["long"] as! Double
//
//             self.moveData(SourceLat: "",SourceLong:"",DestinationLat:doubleS,DestinationLong:doubleD)
//
//            let f: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(doubleS,doubleD);
//            self.oldCoordinate = f;
//            self.counter = self.counter + 1;
//
//        }
    }
     
     
    
    @IBAction fileprivate func New_Request_ListView(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "GetOpenRequestsController")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction fileprivate func Menu_Action(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SideMenuUpdate"), object: nil)
        
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction fileprivate func Notification_Action(_ sender: Any) {
        let sessionManager = SessionManager()
        sessionManager.saveFirstRequestData_BOOL(isBool:false)
        let storyBoard = UIStoryboard.init(name: "Home", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "NotiListController")
        self.navigationController?.pushViewController(controller, animated: true)

        
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        
//        let storyboard = UIStoryboard(name: "Home", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "NotiListController") as! NotiListController
//
//        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        let window = appDelegate?.window
//        window?.rootViewController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction fileprivate func OnLine_Action(_ sender: Any) {
        
        if AlamofireRequest.shared.InterNetConnection()
        {
        if isOnLine{
            
            isOnLine = false
            btn_onLine.setTitle("Go", for:.normal)
            btn_onLine.setTitleColor(UIColor.white, for: .normal)
            btn_onLine.titleLabel?.font =  .BoldFont(30)
            lbe_onLineStats.text = "You're offline"
            view_onLineOne.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
            view_onLineTwo.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
            view_onLineTwo.layer.borderColor = UIColor.white.cgColor
            view_onLineTwo.layer.borderWidth = 1.5
            OnlineUserClass.shared.AddDriver_OnlineRequest(userOnline:false)
            
        }
        else{
            
            isOnLine = true
            btn_onLine.setTitle("Stop", for:.normal)
            btn_onLine.titleLabel?.font =  .SemiBoldFont(21)
            btn_onLine.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1), for: .normal)
            lbe_onLineStats.text = "You're online"
            view_onLineOne.backgroundColor = UIColor.white
            view_onLineTwo.backgroundColor = UIColor.white
            view_onLineTwo.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 0.3).cgColor
            view_onLineTwo.layer.borderWidth = 1.5
            OnlineUserClass.shared.AddDriver_OnlineRequest(userOnline:true)
        }
    }
    else{
        AlamofireRequest.shared.MsgAlert()
    }
}
    
    
    // MARK: Location Get
    func get_Location(){
        
        locationManager = CLLocationManager()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.delegate = self
        mapView.delegate = self

        locationManager.desiredAccuracy = kCLLocationAccuracyBest
       
        locationManager.startUpdatingLocation()
        
        self.mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)

        
        if CLLocationManager.locationServicesEnabled() {

               switch(CLLocationManager.authorizationStatus()) {
               //check if services disallowed for this app particularly
               case .restricted, .denied:
                   print("No access")
                   let accessAlert = UIAlertController(title: "Location Services Disabled", message: "You need to enable location services in settings.", preferredStyle: UIAlertController.Style.alert)

                   accessAlert.addAction(UIAlertAction(title: "Okay!", style: .default, handler: { (action: UIAlertAction!) in UIApplication.shared.open(URL(string: "\(UIApplication.openSettingsURLString)")!)
                   }))

                   present(accessAlert, animated: true, completion: nil)

               //check if services are allowed for this app
               case .authorizedAlways, .authorizedWhenInUse:
                   print("Access! We're good to go!")
               //check if we need to ask for access
               case .notDetermined:
                   print("asking for access...")
              
               @unknown default: break
                
            }
           } else {
               let alert = UIAlertController(title: "", message: "GPS access is restricted. In order to use Pay and Checking, Please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) in
                   UIApplication.shared.open(URL(string: "\(UIApplication.openSettingsURLString)")!)

                   
               }))
            
            self.present(alert, animated: true, completion:nil)
       }
    }
}

extension HomeController: CLLocationManagerDelegate,GMSMapViewDelegate
{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        
     
    }
    
    func gotoMyLocationAction(sender: UIButton)
    {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
              let lng = self.mapView.myLocation?.coordinate.longitude else { return }

        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15)
        self.mapView.animate(to: camera)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        marker.position = position.target
       // reverseGeocoding(marker: marker)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.first else {
            return
        }

        mapView.delegate = self
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)

      // marker.icon = UIImage(named: "current_ic")
      // marker.icon =  nil
      // marker.appearAnimation = .pop // Appearing animation. default
      // marker.position = location.coordinate // CLLocationCoordinate2D
      // marker.isDraggable = true
      // reverseGeocoding(marker: marker)
      // marker.map = nil
        
        
        let locationData = locations.last! as CLLocation
       // print(locationData.coordinate.latitude)
       // print(locationData.coordinate.longitude)
        locationManager.stopUpdatingLocation()


     
    }
    
    //Mark: Marker methods
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
//        print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
//        reverseGeocoding(marker: marker)
//        print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker)
    {
        print("didBeginDragging")
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker)
    {
        print("didDrag")
    }
    
    //Mark: Reverse GeoCoding
    
    func reverseGeocoding(marker: GMSMarker)
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: "\n")
                print(currentAddress)
            }
          //  marker.map = self.mapView
       }
    }
    
    
    func drawLine(SourceLat:String , SourceLong: String , DestinationLat: String , DestinationLong : String)
    {

        
//        let marker = GMSMarker()
//            marker.position = CLLocationCoordinate2D(latitude: Double(SourceLat) ?? 00.00, longitude: Double(SourceLong) ?? 00.00)
//            marker.icon = UIImage(named: "firstloc_ic")
//            marker.title = "Pickup"
//            marker.map = mapView
//
//
//            let markerr = GMSMarker()
//            markerr.position = CLLocationCoordinate2D(latitude: Double(DestinationLat) ?? 00.00, longitude: Double(DestinationLong) ?? 00.00 )
//            markerr.icon = UIImage(named: "lastloc_ic")
//            markerr.title = "Delivery"
//            markerr.map = mapView
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let origin = "\(SourceLat),\(SourceLong)"
        let destination = "\(DestinationLat),\(DestinationLong)"

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=driving&key=AIzaSyBKjkZ9biB8ye5_kUJaYl2YYYCLDCkzM8s")!
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {

                        if let preRoutes = json["routes"] as? NSArray {
                            if preRoutes.count > 0 {
                        let routes = preRoutes[0] as! NSDictionary
                        let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                        let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                        
                        DispatchQueue.main.async(execute: {

                        if self.oldPolyLines.count > 0 {
                            for polyline in self.oldPolyLines {
                                polyline.map = nil
                            }
                        }
                        })

                        DispatchQueue.main.async(execute: {
                            let path = GMSPath(fromEncodedPath: polyString)
                            let polyline = GMSPolyline(path: path)
                            polyline.strokeWidth = 3.5
                            polyline.strokeColor = UIColor.black
                            polyline.map = self.mapView
                            self.oldPolyLines.append(polyline)

                            
                        })
                    }
                    }
                    }
                } catch {
                    print("parsing error")
                }
            }
        })
        task.resume()
    }
    
            
    func moveData(SourceLat:String,SourceLong:String,DestinationLat:CDouble, DestinationLong:CDouble){

        
        let newCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(DestinationLat,DestinationLong)

        let calBearing: Float = getHeadingForDirection(fromCoordinate: self.oldCoordinate!, toCoordinate: newCoodinate!)
        
        markerCar.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        markerCar.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: self.oldCoordinate!, toCoordinate: newCoodinate!))
               //found bearing value by calculation when marker add
        markerCar.position = self.oldCoordinate!
               //this can be old position to make car movement to new position
        markerCar.map = self.mapView
               //marker movement animation
               CATransaction.begin()
               CATransaction.setValue(Int(2.0), forKey: kCATransactionAnimationDuration)
               CATransaction.setCompletionBlock({() -> Void in
                self.marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
                self.marker.rotation = CDouble(calBearing)
                   //New bearing value from backend after car movement is done
               })
        markerCar.position = newCoodinate!
               //this can be new position after car moved from old position to new position with animation
        markerCar.map = self.mapView
        markerCar.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        markerCar.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: self.oldCoordinate!, toCoordinate: newCoodinate!))
               //found bearing value by calculation
        CATransaction.commit()
        
        drawLine(SourceLat: String(doubleS), SourceLong: String(doubleD), DestinationLat: "40.81210", DestinationLong: "-74.07241")

    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {

            let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
            let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
            let tLat: Float = Float((toLoc.latitude).degreesToRadians)
            let tLng: Float = Float((toLoc.longitude).degreesToRadians)
            let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
            if degree >= 0 {
                return degree
            }
            else {
                return 360 + degree
            }
        }
    
    
}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
