//
//  SideMenuController.swift
//  DriverMinaApp
//
//  Created by Apple on 05/02/21.
//

import UIKit

class SideMenuController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lbeName: UILabel!
    @IBOutlet weak var lbeSp: UILabel!
    @IBOutlet weak var lbe_PhoneNo: UILabel!
    @IBOutlet weak var lbe_AppBuild: UILabel!
    @IBOutlet weak var lbeRating: UILabel!

    @IBOutlet weak var imgProfile: UIImageView!

    var titleArray = [String]()
    var imagesArray = [String]()
    var userModel = UserModel()
    var personalInfoModel = Personal_VehicleDetails()
    var sessionManager = SessionManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleArray = ["Home","Accepted Job","Job in progress","Completed Job","My Profile","Notifications","Settings","Sign Out"]
        
        imagesArray = ["home_ic","history_ic","history_ic","history_ic","profile_ic","notification_ic","setting_ic","signout_ic"]
        
        if #available(iOS 11.0, *) {
            tableview.contentInsetAdjustmentBehavior = .never
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.SideMenuUpdate), name: NSNotification.Name(rawValue: "SideMenuUpdate"), object: nil)
    }
    
    @objc func SideMenuUpdate(_ notification: NSNotification) {

        self.tableview.reloadData()
        
    }
    
    //MARK:-   headerUpdate

    func headerUpdate(){
        
        userModel = sessionManager.getUserDetails()
        let img = GlobalConstants.ImageBaseURL  + userModel.pic
        let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var strURL = String()
            strURL = escapedAddress ?? ""
                
        imgProfile.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))
        
        lbeName.text = userModel.firstName + " " + userModel.lastName
        
        if userModel.phoneNumber != "" {
            lbe_PhoneNo.text = String(userModel.phoneNumber)
        }
        
        lbe_AppBuild.text = ("Verison \(Bundle.main.releaseVersionNumber!) Build \(String(describing: Bundle.main.buildVersionNumber!))")
        
        personalInfoModel = sessionManager.getPersonalVehicleDetails()
        lbeRating.text = String(personalInfoModel.rating)

    }
    
    
    @IBAction func Rating_Action(_ sender: Any) {

        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
        let dataDict:[String: String] = ["Class":"RatingClass"]
        NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
        
    }

   //MARK:-   TableView Function
      
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                headerUpdate()
                return titleArray.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
          
          cell.lbeName.text = titleArray[indexPath.row]
          cell.imgVw.image = UIImage (named: imagesArray[indexPath.row])
          return cell
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 60
      }
    
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
        tableview.deselectRow(at: indexPath, animated: false)

        if indexPath.row == 0 {
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            return
        }
        else if indexPath.row == 1{
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            let dataDict:[String: String] = ["Class":"MyJobsClass"]
            NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
            return
        }
                
        else if indexPath.row == 2{
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            let dataDict:[String: String] = ["Class":"JobProgress"]
            NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
            return
            
         }
        else if indexPath.row == 3{
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            let dataDict:[String: String] = ["Class":"Completed"]
            NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
            return
        }
        else if indexPath.row == 4{
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            let dataDict:[String: String] = ["Class":"Profile"]
            NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
            return
        }
        else if indexPath.row == 5{
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            let dataDict:[String: String] = ["Class":"Notification"]
            NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
            return
        }
        else if indexPath.row == 6{
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            let dataDict:[String: String] = ["Class":"Settings"]
            NotificationCenter.default.post(name: Notification.Name("MyJobsClass"), object:nil,userInfo: dataDict)
            return
        }
                    
        if titleArray.count == indexPath.row + 1 {
            ActionSheet()
        }
            
        sideMenuController?.hideLeftView(animated: true, completionHandler: nil)

        }
    
    //MARK: - Logout Message Action Sheet
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message:"Are you sure you want to sign out?", preferredStyle: .alert)
        
        let No = UIAlertAction(title:"No", style: .default, handler: { action in
        })
            alert.addAction(No)
        
        let Yes = UIAlertAction(title:"Yes", style: .default, handler: { action in
            
            NotificationCenter.default.post(name: Notification.Name("HomeTimer_Invalidate"), object: nil)
            NotificationCenter.default.post(name: Notification.Name("Open_Requset_Timer_Invalidate"), object: nil)

            UserDefaults.standard.set(false, forKey: "Login")
            RootControllerManager().SetRootViewController()

        })
        alert.addAction(Yes)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
    
    func  networkAlertShow(){
      self.MessageAlert(title: "No Internet Connection", message: "Internet not available, Cross check your internet connectivity and try again")
    }
    
    //MARK: - Call Logout API
    func callLogOutApi(){
        LogoutAPIRequest.shared.Logout(requestParams:[:]) { (message, status) in
            if status == true{
                UIViewControllerX().SessionExpire()
            }else{
                UIViewControllerX().SessionExpire()
            }
        }
    }
    
}

class LogoutAPIRequest: NSObject {
    
    static let shared = LogoutAPIRequest()

    func Logout(requestParams : [String:Any], completion: @escaping (_ message : String?, _ status : Bool) -> Void) {

        AlamofireRequest.shared.PostBodyForRawData(urlString: "BaseURL".LogOutURL, parameters: requestParams, authToken: "", isLoader: true, loaderMessage: "Logout") { (data, error) in
        
            if error == nil{
                print(data as Any)
            }else{
                completion("There was an error connecting to the server.try again", false)
            }
        }
    }
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
