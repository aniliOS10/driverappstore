//
//  SideMenuCell.swift
//  DriverMinaApp
//
//  Created by Apple on 05/02/21.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var imgVw: UIImageView! = nil
    @IBOutlet weak var lbeName: UILabel! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
