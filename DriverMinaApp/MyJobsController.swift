//
//  MyJobsController.swift
//  DriverMinaApp
//
//  Created by Apple on 05/03/21.
//

import UIKit

class MyJobsController: UIViewController {

    
    @IBOutlet weak var tableViewJobs: UITableView!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var viewNoData: UIView!

    @IBOutlet weak var btn_Filter: UIButton!

    
    var acceptDataArray : [GetAcceptedModel] = []
    var acceptArrayFilter : [GetAcceptedModel] = []
    let refreshControl = UIRefreshControl()
    var userModel = UserModel()
    var sessionManager = SessionManager()

    
    @objc func RefreshScreen() {
       
        if AlamofireRequest.shared.InterNetConnection()
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.getRequestAcceptedListData(false)
            }
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        topViewLayout()
        viewNoData.isHidden = true
        getRequestAcceptedListData(true)
        UserDefaults.standard.set(true, forKey: "Call_Api_Accepted")
        
        refreshControl.addTarget(self, action:  #selector(RefreshScreen), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        if #available(iOS 10.0, *) {
            tableViewJobs.refreshControl = refreshControl
            } else {
        tableViewJobs.addSubview(refreshControl)
        }
        
        userModel = sessionManager.getUserDetails()
        if userModel.vehicleTypeId == 3 {
            btn_Filter.isHidden = true
        }
    }
    
    func topViewLayout(){
        if !MyJobsController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        if !UserDefaults.standard.bool(forKey: "Call_Api_Accepted"){
            getRequestAcceptedListData(true)
            UserDefaults.standard.set(true, forKey: "Call_Api_Accepted")
        }
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction fileprivate func filter_Action(_ sender: Any) {

        filterData()
    }
    
    func filterData(){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "All", style: .default , handler:{ (UIAlertAction)in
            self.acceptArrayFilter.removeAll()
            self.acceptArrayFilter = self.acceptDataArray
            self.tableViewJobs.reloadData()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Normal", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.data_FilterValue(1)
            
        }))

        alert.addAction(UIAlertAction(title: "Express", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
            self.data_FilterValue(2)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    
    func data_FilterValue(_ filterID:Int){
        acceptArrayFilter.removeAll()
        for data in acceptDataArray {
            if data.deliveryTypeId == filterID {
                acceptArrayFilter.append(data)
            }
        }
        self.tableViewJobs.reloadData()
    }
    
    
    func getRequestAcceptedListData(_ loader:Bool) {
        
        GetAcceptedRequest.shared.GetAcceptedRequestData(requestParams:[:], loader) { (obj,status) in
            DispatchQueue.main.async {
            if status{
                self.refreshControl.endRefreshing()
                self.acceptDataArray = obj!
                self.acceptArrayFilter = obj!
                self.tableViewJobs.reloadData()
                if self.acceptDataArray.count == 0 {
                     self.viewNoData.isHidden = false
                }
                else{
                    self.viewNoData.isHidden = true
                }
        }
        else{
                self.refreshControl.endRefreshing()
                self.viewNoData.isHidden = false
                self.acceptDataArray.removeAll()
                self.acceptArrayFilter.removeAll()
                self.tableViewJobs.reloadData()
        }
      }
    }
  }
    
    
    func cancelRequestAccepted(_ orderID:Int, _ index: Int) {
        
        AcceptedCancelRequest.shared.GetAcceptedCancelData(orderID, true) { (obj,status) in
            DispatchQueue.main.async {
            if status{
                self.alertViewSuccess(title: "Cancel!", mess:"Order cancel successfully.", index: orderID)
        }
            else{
                self.MessageAlert(title: "Oops!", message:"Something Went Wrong")
            }
      }
    }
  }
    
    
    func alertViewSuccess(title:String,mess:String,index:Int){
           let alert = UIAlertController(title: title, message:mess, preferredStyle: .alert)
           
           let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
            self.acceptArrayFilter.removeAll()
            var acceptArrayRemove : [GetAcceptedModel] = []
            acceptArrayRemove = self.acceptDataArray
            for i in 0..<acceptArrayRemove.count
            {
               let data : GetAcceptedModel = acceptArrayRemove[i]
                if data.orderId == index {
                    self.acceptDataArray.remove(at: i)
                }
            }
            self.acceptArrayFilter = self.acceptDataArray
            self.tableViewJobs.reloadData()
            
           })
           alert.addAction(ok)
           
           DispatchQueue.main.async(execute: {
               self.present(alert, animated: true)
           })
       }
}


//MARK: - TableViewDataSource Delegate
extension MyJobsController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.acceptArrayFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "MyJobsAcceptTableViewCell", for: indexPath) as? MyJobsAcceptTableViewCell else {
            return MyJobsAcceptTableViewCell()
        }

        cellOf.selectionStyle = .none
        
        let data : GetAcceptedModel = self.acceptArrayFilter[indexPath.row]
        
        cellOf.lbe_Duration.text = data.duration
        cellOf.lbe_Distance.text = data.distance
        cellOf.lbe_SType.text = data.deliveryType
        cellOf.lbe_OrderNo.text = String(data.orderId)
        cellOf.lbe_DateTime.text = data.notifyDate + ", " + data.notifyTime
        cellOf.lbe_Pickup.text = data.senderAddress
        cellOf.lbe_Delivery.text = data.receiverAddress
        cellOf.lbe_Status.text = data.status
        
        if data.deliveryType == "Normal"{
            cellOf.lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        }
        else if data.deliveryType == "Express"{
            cellOf.lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
        }
      
        cellOf.btn_Cancel.tag = indexPath.row
        cellOf.btn_Cancel.addTarget(self, action: #selector(action_Request_Cancel), for: .touchUpInside)

        return cellOf
    }
    
    
    @objc func action_Request_Cancel(sender: UIButton){
        let buttonTag = sender.tag
        if self.acceptArrayFilter.count > buttonTag {
            
            let alert = UIAlertController(title: nil, message:"Are you sure you want to Cancel this Accepted Order?", preferredStyle: .alert)
            
            let No = UIAlertAction(title:"No", style: .default, handler: { action in
            })
                alert.addAction(No)
            
            let Yes = UIAlertAction(title:"Yes", style: .destructive, handler: { action in
                
                let data : GetAcceptedModel = self.acceptArrayFilter[buttonTag]
                self.cancelRequestAccepted(data.orderId,buttonTag)

            })
            alert.addAction(Yes)
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
          
        }
    }
    
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label = UILabel ()
        label.frame = CGRect.init(x: 0, y: 0, width:width, height: CGFloat.greatestFiniteMagnitude)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        let style = NSMutableParagraphStyle()
            style.lineSpacing = 4
            style.alignment = .left
            label.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle: style])
           
            label.sizeToFit()
        
        let maxSize = CGSize(width:width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
    
       // print("LINE-",CGFloat(linesRoundedUp))
        return CGFloat(linesRoundedUp * Int(charSize) + 25)

    }
    
    func dynamicFontSize(_ FontSize: CGFloat) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.size.width
        let calculatedFontSize = screenWidth / 375 * FontSize
        return calculatedFontSize
    }
}

    
//MARK: - TableViewDelegate Delegate
extension MyJobsController : UITableViewDelegate
{
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)

        if self.acceptArrayFilter.count > indexPath.row {
            let data : GetAcceptedModel = self.acceptArrayFilter[indexPath.row]
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackController") as! OrderTrackController
                vc.requestID = data.requestId
                vc.orderNumber = data.orderId
                vc.senderNumber = data.senderPhoneNumber
                vc.acceptOrderArray = [self.acceptArrayFilter[indexPath.row]]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height_Text: CGFloat = 30
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        var fontNew = UIFont()
    
        fontNew = .RegularFont(Float(dynamicFontSize(15)))
        
        let data : GetAcceptedModel = self.acceptArrayFilter[indexPath.row]

        height_Text = heightForView(text:data.senderAddress, font: fontNew, width: screenWidth - 55)
        
        height_Text = heightForView(text:data.receiverAddress, font: fontNew, width: screenWidth - 55) + height_Text

      
        return height_Text + 190
    }
}

