//
//  ChangePasswordVc.swift
//  DriverMinaApp
//
//  Created by Apple on 23/04/21.
//

import UIKit

class ChangePasswordVc: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    @IBOutlet weak var txt_OldPass : UITextField!
    @IBOutlet weak var txt_NewPass : UITextField!

    var eyeClick = true
    var eyeClickNew = true

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        CustomizationTextFeild()
        addEyeOld()
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_OldPass.tag = 1
        txt_NewPass.tag = 2
        
        txt_NewPass.delegate = self
        txt_OldPass.delegate = self
        
        txt_NewPass.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_OldPass.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }
    
    func topViewLayout(){
        if !ChangePasswordVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view .endEditing(true)
    }
    
    func addEyeOld(){
        let buttonOld = UIButton(type: .custom)
        buttonOld.setImage(UIImage(named: "eye_ic")?.maskWithColor(color: UIColor.lightGray), for: .normal)
        buttonOld.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        buttonOld.frame = CGRect(x: CGFloat(txt_OldPass.frame.size.width - 40), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        buttonOld.addTarget(self, action: #selector(self.reveal), for: .touchUpInside)
        
        txt_OldPass.rightView = buttonOld
        txt_OldPass.rightViewMode = .always
        
        let buttonNew = UIButton(type: .custom)
        buttonNew.setImage(UIImage(named: "eye_ic")?.maskWithColor(color: UIColor.lightGray), for: .normal)
        buttonNew.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        buttonNew.frame = CGRect(x: CGFloat(txt_NewPass.frame.size.width - 40), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        buttonNew.addTarget(self, action: #selector(self.reveal_New), for: .touchUpInside)
        
        txt_NewPass.rightView = buttonNew
        txt_NewPass.rightViewMode = .always
        
        }
    
    @IBAction func reveal_New(_ sender: UIButton) {
        if(eyeClickNew == true) {
            sender.setImage(UIImage(named: "eye_ic"), for: .normal)
            txt_NewPass.isSecureTextEntry = false
        } else {
            sender.setImage(UIImage(named: "eye_ic")?.maskWithColor(color: UIColor.lightGray), for: .normal)
            txt_NewPass.isSecureTextEntry = true
        }
        eyeClickNew = !eyeClickNew
    }
        
    @IBAction func reveal(_ sender: UIButton) {
            if(eyeClick == true) {
                sender.setImage(UIImage(named: "eye_ic"), for: .normal)
                txt_OldPass.isSecureTextEntry = false
            } else {
                sender.setImage(UIImage(named: "eye_ic")?.maskWithColor(color: UIColor.lightGray), for: .normal)
                txt_OldPass.isSecureTextEntry = true
            }
            eyeClick = !eyeClick
        }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction fileprivate func ChangePass_Action(_ sender: Any) {
        self.view .endEditing(true)

        let oldText = txt_OldPass.text ?? ""

        if oldText.isEmpty {
            self.showErrorMSg(text: "Please enter in Old Password Field")
            return
        }
        
        let passWordText = txt_NewPass.text ?? ""
        if passWordText.isEmpty
        {
            self.showErrorMSg(text: "Please enter in New Password Field")
            return
        }
        
        let trimmedPassword = txt_NewPass.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        if trimmedPassword!.count < 6{
            MessageAlert(title:"",message: "New Password Required minimum 6 Characters Password")
            return
        }
        
        let sessionManager = SessionManager()

        let params = ["newPassword": passWordText,"oldPassword": oldText,"driverId":sessionManager.getUserDetails().userID] as [String : Any]
        
        ChangePassword(Params: params)
    }
    
    //MARK: -  ChangePassword Call --
    func ChangePassword(Params:[String: Any]){
        self.view.endEditing(true)
    
        ChangePasswordUser.shared.ChangePasswordByUser(requestParams: Params) { (msg, success) in
            
            if success {
                self.popMessageAlert(title:msg ?? "", message:"")
            }
            else{
                self.showErrorMSg(text:msg ?? "Error!")
            }
        }
    }
}
extension ChangePasswordVc : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
//MARK: -  Change Password User Call --


class ChangePasswordUser: NSObject {
    
    static let shared = ChangePasswordUser()
    
    func ChangePasswordByUser(requestParams : [String:Any], completion: @escaping (_ message : String?, _ status : Bool) -> Void) {
        
        let sessionManager = SessionManager()
        
        AlamofireRequest.shared.PostBodyForRaw(urlString: "BaseURL".ChangePasswordURL, parameters: requestParams, authToken: sessionManager.getUserDetails().accessToken, isLoader: true, loaderMessage: "") { (data, error) in
            if error == nil{
                
                if let status = data?["status"] as? Bool{
                    
                    var messageString : String = ""
                    if let msg = data?["message"] as? String{
                        messageString = msg
                    }
                    if status {
                        completion(messageString,status)
                    }
                    else{
                        completion(messageString, false)
                    }
                }
                else
                {
                    completion("There was an error connecting to the server.", false)
                }
            }
            else{
                completion("There was an error connecting to the server.try again", false)
           }
        }
    }
}
