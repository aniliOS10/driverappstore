//
//  JobProgressController.swift
//  DriverMinaApp
//
//  Created by Apple on 22/03/21.
//

import UIKit

class JobProgressController: UIViewController {

    @IBOutlet weak var tableViewProgressJobs: UITableView!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var btn_Filter: UIButton!

    let refreshControl = UIRefreshControl()
    var acceptDataArray : [GetJobProgressModel] = []
    var acceptArrayFilter : [GetJobProgressModel] = []
    var userModel = UserModel()
    var sessionManager = SessionManager()
    @objc func RefreshScreen() {
       
        if AlamofireRequest.shared.InterNetConnection()
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.getRequestAcceptedList(false)
            }
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
        viewNoData.isHidden = true
        
        self.getRequestAcceptedList(true)
        UserDefaults.standard.set(true, forKey: "Call_Api_Progress")
        
        refreshControl.addTarget(self, action:  #selector(RefreshScreen), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        if #available(iOS 10.0, *) {
                tableViewProgressJobs.refreshControl = refreshControl
            } else {
                tableViewProgressJobs.addSubview(refreshControl)
            }
        
        userModel = sessionManager.getUserDetails()
        if userModel.vehicleTypeId == 3 {
            btn_Filter.isHidden = true
        }
    }
    
    func topViewLayout(){
        if !JobProgressController.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        if !UserDefaults.standard.bool(forKey: "Call_Api_Progress"){
             getRequestAcceptedList(true)
             UserDefaults.standard.set(true, forKey: "Call_Api_Progress")
        }
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction fileprivate func filter_Action(_ sender: Any) {

        filterData()
    }
    
    func filterData(){
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "All", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            
            self.acceptArrayFilter.removeAll()
            self.acceptArrayFilter = self.acceptDataArray
            self.tableViewProgressJobs.reloadData()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Normal", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.data_FilterValue(1)
            
        }))

        alert.addAction(UIAlertAction(title: "Express", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
            self.data_FilterValue(2)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func data_FilterValue(_ filterID:Int){
        acceptArrayFilter.removeAll()
        for data in acceptDataArray {
            if data.deliveryTypeId == filterID {
                acceptArrayFilter.append(data)
            }
        }
        self.tableViewProgressJobs.reloadData()
    }
    
    func getRequestAcceptedList(_ loader:Bool) {
        
        JobProgressRequest.shared.GetJobProgressData(requestParams:[:], loader) { (obj,status) in
            DispatchQueue.main.async {
            if status{
                self.refreshControl.endRefreshing()
                self.acceptDataArray = obj!
                self.acceptArrayFilter = obj!
                self.tableViewProgressJobs.reloadData()
                
                if self.acceptDataArray.count == 0 {
                     self.viewNoData.isHidden = false
                }
                else{
                    self.viewNoData.isHidden = true
                }
            }
            else{
                self.refreshControl.endRefreshing()
                self.viewNoData.isHidden = false
                self.acceptDataArray.removeAll()
                self.acceptArrayFilter.removeAll()
                self.tableViewProgressJobs.reloadData()
        }
      }
    }
  }
    
    
   
}

//MARK: - TableViewDataSource Delegate
extension JobProgressController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.acceptArrayFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOf = tableView.dequeueReusableCell(withIdentifier: "MyJobsAcceptTableViewCell", for: indexPath) as? MyJobsAcceptTableViewCell else {
            return MyJobsAcceptTableViewCell()
        }

        cellOf.selectionStyle = .none
        
        let data : GetJobProgressModel = self.acceptArrayFilter[indexPath.row]
        
        cellOf.lbe_Duration.text = data.duration
        cellOf.lbe_Distance.text = data.distance
        cellOf.lbe_SType.text = data.deliveryType
        cellOf.lbe_OrderNo.text = String(data.orderId)
        cellOf.lbe_DateTime.text = data.notifyDate + ", " + data.notifyTime
        cellOf.lbe_Pickup.text = data.senderAddress
        cellOf.lbe_Delivery.text = data.receiverAddress
        cellOf.lbe_Status.text = data.status
        
        if data.deliveryType == "Normal"{
            cellOf.lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        }
        else if data.deliveryType == "Express"{
            cellOf.lbe_SType.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).redPlan(Alpha: 1)
        }
      
        cellOf.btn_Cancel.tag = indexPath.row
        cellOf.btn_Cancel.addTarget(self, action: #selector(action_Request_Cancel), for: .touchUpInside)

        return cellOf
    }
    
    
    @objc func action_Request_Cancel(sender: UIButton){
        
        if self.acceptArrayFilter.count > sender.tag {
            let data : GetJobProgressModel = self.acceptArrayFilter[sender.tag]
            guard let url = URL(string: "telprompt://\(data.receiverPhoneNumber)"),
                UIApplication.shared.canOpenURL(url) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        
        let label = UILabel ()
        label.frame = CGRect.init(x: 0, y: 0, width:width, height: CGFloat.greatestFiniteMagnitude)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        let style = NSMutableParagraphStyle()
            style.lineSpacing = 4
            style.alignment = .left
            label.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle: style])
           
            label.sizeToFit()
        
        let maxSize = CGSize(width:width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
    
      //  print("LINE-",CGFloat(linesRoundedUp))
        return CGFloat(linesRoundedUp * Int(charSize) + 25)

    }
}

    
//MARK: - TableViewDelegate Delegate
extension JobProgressController : UITableViewDelegate
{
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)

        if self.acceptArrayFilter.count > indexPath.row {
            let data : GetJobProgressModel = self.acceptArrayFilter[indexPath.row]
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackProgressController") as! OrderTrackProgressController
                vc.requestID = data.requestId
                vc.orderNumber = data.orderId
                vc.acceptOrderArray = [self.acceptArrayFilter[indexPath.row]]
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    func dynamicFontSize(_ FontSize: CGFloat) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.size.width
        let calculatedFontSize = screenWidth / 375 * FontSize
        return calculatedFontSize
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height_Text: CGFloat = 30
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        var fontNew = UIFont()
   
        fontNew = .RegularFont(Float(dynamicFontSize(15)))
        
        
        let data : GetJobProgressModel = self.acceptArrayFilter[indexPath.row]

        height_Text = heightForView(text:data.senderAddress, font: fontNew, width: screenWidth - 55)
        
        height_Text = heightForView(text:data.receiverAddress, font: fontNew, width: screenWidth - 55) + height_Text

      
        return height_Text + 190
    }
}

