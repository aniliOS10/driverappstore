//
//  PersonalDetailsUpdateVc.swift
//  DriverMinaApp
//
//  Created by Apple on 22/04/21.
//

import UIKit
import SDWebImage
import Photos
import MobileCoreServices
import DropDown
import Alamofire
import Reachability
import Lightbox

class PersonalDetailsUpdateVc: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
   
    @IBOutlet weak var txt_Email : UITextField!

    @IBOutlet weak var txt_Address : UITextField!
    @IBOutlet weak var txt_City : UITextField!
    
    @IBOutlet weak var txt_SelfieName : UITextField!
    @IBOutlet weak var txt_ProofName : UITextField!
    
    @IBOutlet weak var lbe_IdType : UILabel!
    @IBOutlet weak var lbe_State : UILabel!
    @IBOutlet weak var lbe_IdName : UILabel!
    @IBOutlet weak var lbe_IdPersonal : UILabel!
    @IBOutlet weak var lbe_IdProof : UILabel!
    @IBOutlet weak var lbe_Province : UILabel!

    @IBOutlet weak var btn_Next : UIButton!
    @IBOutlet weak var btn_IdType : UIButton!
    @IBOutlet weak var btn_State : UIButton!
    @IBOutlet weak var btn_IdUpload : UIButton!
    @IBOutlet weak var btn_IdPersonal : UIButton!
    @IBOutlet weak var btn_IdProof : UIButton!
    
    @IBOutlet weak var btn_IdSelfie_Show : UIButton!
    @IBOutlet weak var btn_IdPersonal_Show : UIButton!
    @IBOutlet weak var btn_IdProof_Show : UIButton!
    @IBOutlet weak var btn_Province : UIButton!

    @IBOutlet weak var img_User : UIImageView!

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    var imagePicker = UIImagePickerController()
    var personalInfoModel = Personal_VehicleDetails()
    var sessionManager = SessionManager()

    let dropID = DropDown()

    var stateId = Int()

    var int_Province = 0
    
    var statesArray = [String]()
    var statesArrayID = [Int]()
    let dropStates = DropDown()
    
    var imageType = ""
    
    var img_ProfilePic = UIImage()
    var img_PersonalId = UIImage()
    var img_Selfie = UIImage()
    var img_ResidentProof = UIImage()

    var isProfile = false
    var isPersonalId = false
    var isSelfie = false
    var isResidentProof = false

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.dataGetProvinces(false)
        }
        
        topViewLayout()
        setDropsColor()
        setupIDTypeDropDown()
        CustomizationTextFeild()
        CustomizationButton()
    }
    

    func topViewLayout(){
        if !PersonalDetailsUpdateVc.hasSafeArea{
            if view_NavConst != nil {
                view_NavConst.constant = 77
            }
        }
    }
    
    
    func setDropsColor() {
        DropDown.appearance().backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().selectionBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
        DropDown.appearance().cornerRadius = 10
        DropDown.appearance().textColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        dataShow_InUI()
    }
    
    
    func dataShow_InUI(){
        personalInfoModel = sessionManager.getPersonalVehicleDetails()

        txt_Email.text = personalInfoModel.email
        txt_City.text = personalInfoModel.city
        txt_Address.text = personalInfoModel.address
        lbe_Province.text = personalInfoModel.province
        int_Province = personalInfoModel.provinceId
        
        let img = String(format: "%@%@", GlobalConstants.ImageBaseURL,personalInfoModel.profilePic)
        
        let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        var strURL = String()
            strURL = escapedAddress ?? ""
                
        img_User.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))
        
        
        if let url = URL(string: GlobalConstants.ImageBaseURL + personalInfoModel.profilePic) {
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                img_ProfilePic = UIImage(data: imageData) ?? UIImage()
                isProfile = true
            }
        }
        
        if let url = URL(string: GlobalConstants.ImageBaseURL + personalInfoModel.driverLicenseImgPath) {
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                img_PersonalId = UIImage(data: imageData) ?? UIImage()
                isPersonalId = true
                btn_IdPersonal_Show.isHidden = false
                lbe_IdName.text = personalInfoModel.driverLicenseImgPath
                lbe_IdName.textColor = UIColor.black
            }
        }
        
        if let url = URL(string: GlobalConstants.ImageBaseURL + personalInfoModel.driverSelfieImgPath) {
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                img_Selfie = UIImage(data: imageData) ?? UIImage()
                isSelfie = true
                btn_IdSelfie_Show.isHidden = false
                txt_SelfieName.text = personalInfoModel.driverSelfieImgPath
                txt_SelfieName.textColor = UIColor.black
            }
        }
        if let url = URL(string: GlobalConstants.ImageBaseURL + personalInfoModel.driverProofOfResidenceImgPath) {
            let data = try? Data(contentsOf: url)
            if let imageData = data {
                img_ResidentProof = UIImage(data: imageData) ?? UIImage()
                isResidentProof = true
                btn_IdProof_Show.isHidden = false
                txt_ProofName.text = personalInfoModel.driverProofOfResidenceImgPath
                txt_ProofName.textColor = UIColor.black
            }
        }
    }
    
    
    func setupIDTypeDropDown() {
    
        dropID.anchorView = btn_IdType
        dropID.bottomOffset = CGPoint(x: 0, y: btn_IdType.bounds.height)
    
        dropID.dataSource = ["Licence"]
    
        dropID.selectionAction = { [weak self] (index, item) in
            self!.lbe_IdType.text = item
        }
    }
    
    // MARK: - Custome TextFeild
    func CustomizationTextFeild()
    {
        // Set TextFeild Tag
        txt_Email.delegate = self
        txt_Address.delegate = self
        txt_City.delegate = self

        txt_Email.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_Address.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
        txt_City.placeholderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).placeHolderColor(Alpha: 1)
    }
    
    // MARK: - Custome Button
    func CustomizationButton()
    {
        btn_Next.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).greenColor(Alpha: 1)
    }
    
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction fileprivate func Next_Action(_ sender: Any) {
        self.view.endEditing(true)
        upload_Data()
    }
    
    @IBAction fileprivate func Back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Profile_Image_Update(_ sender: Any) {
           imageType = "User_Profile"
           chooseImageFromGalleryAndCamera()
    }
    
    @IBAction func Personal_Image_Update(_ sender: Any) {
        
        imageType = "Selfie"
        chooseImageFromGalleryAndCamera()
          
    }
    
    @IBAction func ID_Image_Update(_ sender: Any) {
           imageType = "PersonalId"
           chooseImageFromGalleryAndCamera()
    }
    
    @IBAction func Res_Image_Update(_ sender: Any) {
           imageType = "ResidentProof"
           chooseImageFromGalleryAndCamera()
    }
    
    
    @IBAction func Selfie_Image_Show(_ sender: Any) {
        
        imageTapped(tappedImage: img_Selfie)
    }
    
    @IBAction func ID_Image_Show(_ sender: Any) {
        imageTapped(tappedImage: img_PersonalId)
        
    }
    
    @IBAction func Res_Image_Show(_ sender: Any) {
        imageTapped(tappedImage: img_ResidentProof)

    }
    //MARK:-  States Drills Api
    
    @IBAction func ID_Type_Action(_ sender: Any) {

        dropID.show()
    }
    
    
    @IBAction func states_Action(_ sender: Any) {
        if statesArray.count > 0 {
            dropStates.show()
        }
        else{
            dataGetProvinces(true)
        }
    }
    
    
    func setupStatesDropDown() {
    
        dropStates.anchorView = btn_Province
        dropStates.bottomOffset = CGPoint(x: 0, y: btn_Province.bounds.height)
        dropStates.dataSource = self.statesArray
        dropStates.selectionAction = { [weak self] (index, item) in
            self!.lbe_Province.text = item
            self!.int_Province = self?.statesArrayID[index] ?? 0
        }
    }
    
    
    func dataGetProvinces(_ loader:Bool){
        ProvincesListAPI.shared.getProvincesListData(requestParams: [:],loader) { (object,status)  in
            self.statesArray.removeAll()
            self.statesArrayID.removeAll()

            for i in 0..<(object?.data.list.count ?? 0)
            {
                let dict = object?.data.list[i]
                self.statesArray.append(dict?.name ?? "")
                self.statesArrayID.append(dict?.provinceID ?? 0)
            }
            self.setupStatesDropDown()
        }
    }
    
    
    
    @objc func imageTapped(tappedImage: UIImage)
    {
       // let tappedImage = tapGestureRecognizer.view as! UIImageView

        let images = [
            LightboxImage(
              image:tappedImage,
                text: ""
            )
        ]
        let controller = LightboxController(images: images)
        controller.dynamicBackground = true
        present(controller, animated: true, completion: nil)
    }
    
    @objc func chooseImageFromGalleryAndCamera()
    {
        let alert = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{
            (UIAlertAction)in
            print("User click Approve button")
            self.handelUploadCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{
            (UIAlertAction)in
            print("User click Edit button")
            self.handelUploadTap()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{
            (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    @objc func handelUploadTap() {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
            DispatchQueue.main.async {

        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                        
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                        self.imagePicker.allowsEditing = false
                        
                        self.imagePicker.mediaTypes = [kUTTypeImage as String]
                        self.present(self.imagePicker, animated: true, completion: nil)
                }
                    }
                }
                
            })
        }
        else if photos == .authorized {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
    
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: nil)
                
            }
        }
    }
    
    @objc func handelUploadCamera()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera;
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    // MARK: - Image Picker Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        if let editedImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage) {
            self.dismiss(animated: true, completion: nil)
            
            var fileName = "File"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                       fileName = url.lastPathComponent
                   //var fileType = url.pathExtension
                }
    
            if imageType == "User_Profile" {
                self.img_User.image = editedImage
                img_ProfilePic = editedImage
                isProfile = true

            }
            if imageType == "PersonalId" {
                 img_PersonalId = editedImage
                 isPersonalId = true
                 btn_IdPersonal_Show.isHidden = false
                 lbe_IdName.text = fileName
                 lbe_IdName.textColor = UIColor.black


            }
            if imageType == "Selfie" {
                 img_Selfie = editedImage
                 isSelfie = true
                 btn_IdSelfie_Show.isHidden = false
                 txt_SelfieName.text = fileName
                 txt_SelfieName.textColor = UIColor.black


            }
            if imageType == "ResidentProof" {
                img_ResidentProof = editedImage
                isResidentProof = true
                btn_IdProof_Show.isHidden = false
                txt_ProofName.text = fileName
                txt_ProofName.textColor = UIColor.black
                

            }
        }
        else if let origionalImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) {
            self.dismiss(animated: true, completion: nil)
            
            var fileName = "File"
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
                       fileName = url.lastPathComponent
                   //var fileType = url.pathExtension
                }
            
            if imageType == "User_Profile" {
                self.img_User.image = origionalImage
                img_ProfilePic = origionalImage
                isProfile = true

            }
            if imageType == "PersonalId" {
                img_PersonalId = origionalImage
                isPersonalId = true
                btn_IdPersonal_Show.isHidden = false
                lbe_IdName.text = fileName
                lbe_IdName.textColor = UIColor.black
            }
            if imageType == "Selfie" {
                img_Selfie = origionalImage
                isSelfie = true
                btn_IdSelfie_Show.isHidden = false
                txt_SelfieName.text = fileName
                txt_SelfieName.textColor = UIColor.black
            }
            if imageType == "ResidentProof" {
                img_ResidentProof = origionalImage
                isResidentProof = true
                btn_IdProof_Show.isHidden = false
                txt_ProofName.text = fileName
                txt_ProofName.textColor = UIColor.black
            }
         }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func upload_Data(){
        
        if isProfile == false {
            MessageAlert(title:"",message: "Please add profile picture")
            return
        }
        
        let trimmedEmailName = txt_Email.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        
        if self.txt_Email.text?.count == 0 {
            MessageAlert(title:"",message: "Please enter email")
            return
        }
        
        if  !(trimmedEmailName?.EmailValidation())!{
                MessageAlert(title:"",message: "Enter either valid email")
                return
              }
        
        if self.txt_Address.text?.count == 0 {
            MessageAlert(title:"",message: "Please enter address")
            return
        }
        
        if self.txt_City.text?.count == 0 {
            MessageAlert(title:"",message: "Please enter city")
            return
        }
        
       
        if isPersonalId == false {
            MessageAlert(title:"",message: "Please add licence")
            return
        }
        if isSelfie == false {
            MessageAlert(title:"",message: "Please Upload Selfie (Hold license to chest)")
            return
        }
        if isResidentProof == false {
            MessageAlert(title:"",message: "Please add Residence proof")
            return
        }

        let sessionManager = SessionManager()

        var dict: Dictionary<String, AnyObject> = [:]
            dict["Email"] = self.txt_Email.text as AnyObject
            dict["PersonalIDDocTypeId"] = 1 as AnyObject
            dict["ProvinceId"] = int_Province as AnyObject

        dict["Address"] = self.txt_Address.text as AnyObject
        dict["City"] = self.txt_City.text as AnyObject
        dict["DriverId"] = sessionManager.getUserDetails().userID as AnyObject


        PersonalDetails_MultipartFormData(parameters:dict, ResidentProof: img_ResidentProof, Selfie: img_Selfie, ProfilePic: img_ProfilePic, PersonalId: img_PersonalId, isLoader: true, loaderMessage: "Update"){ (obj,er) in

            if obj?["status"] as? Bool ?? false {
                
                if let data = obj?["data"] as? [String:Any]{
            
                    var userModelLogin = Personal_VehicleDetails()
                    userModelLogin = sessionManager.getPersonalVehicleDetails()
                    userModelLogin.driverLicenseImgPath = data["personalIDImg"] as? String ?? ""
                    userModelLogin.driverSelfieImgPath = data["selfieImg"] as? String ?? ""
                    userModelLogin.driverProofOfResidenceImgPath = data["proofOfResidenceImg"] as? String ?? ""
                    userModelLogin.profilePic = data["profilePic"] as? String ?? ""
                    userModelLogin.city = self.txt_City.text
                    userModelLogin.address = self.txt_Address.text
                    userModelLogin.email = self.txt_Email.text
                    userModelLogin.province = self.lbe_Province.text
                    userModelLogin.provinceId = self.int_Province
                    
                    self.sessionManager.createPersonal_VehicleDetails_Session(userModel:userModelLogin)
                    
                    
                    var user = UserModel()
                    user = sessionManager.getUserDetails()
                    user.pic = data["profilePic"] as! String
                    
                    self.sessionManager.createLoginSession(userModel:user)
                    
                    self.alertViewSuccess(title: "Update", mess:"Personal details update successfully.")
                }
            }
            else{
                self.MessageAlert(title:"",message: obj?["message"] as? String ?? "There was an error connecting to the server.try again")
            }
        }
    }
    
    func alertViewSuccess(title:String,mess:String){
           let alert = UIAlertController(title: title, message:mess, preferredStyle: .alert)
           let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            
            self.navigationController?.popViewController(animated: true)

           })
           alert.addAction(ok)
           DispatchQueue.main.async(execute: {
               self.present(alert, animated: true)
           })
       }

    
}

extension PersonalDetailsUpdateVc : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}


extension PersonalDetailsUpdateVc {

    func PersonalDetails_MultipartFormData(parameters : [String : AnyObject],ResidentProof : UIImage,Selfie : UIImage,ProfilePic : UIImage,PersonalId : UIImage,isLoader : Bool, loaderMessage : String, completion: @escaping ( _ success: [String : AnyObject]?, _ error : Error?) -> Void) {
        
        
        if AlamofireRequest.shared.InterNetConnection()
        {
            if isLoader{
                Indicator.shared.startAnimating(withMessage: loaderMessage, colorType: UIColor.white, colorText: UIColor.white)
            }
            
            let urlString = "BaseURL".UpdateDriverInfoURL
            print("URL - ",urlString)
            let sessionManager = SessionManager()

            let urL = URL(string: urlString)
            let bearer : String = "Bearer \(sessionManager.getUserDetails().accessToken ?? "")"
            
            let headers: HTTPHeaders = [
                "Authorization": bearer,
                "Content-type": "multipart/form-data"
            ]
            
            AF.upload(
                multipartFormData: { multipartFormData in
                    let image1 = ResidentProof.jpegData(compressionQuality: 0.5)!

                    let image2 = Selfie.jpegData(compressionQuality: 0.5)!

                    let image3 = ProfilePic.jpegData(compressionQuality: 0.8)!

                    let image4 = PersonalId.jpegData(compressionQuality: 0.5)!

                    for key in parameters.keys{
                        let name = String(key)
                        if let val = parameters[name] as? String{
                            multipartFormData.append(val.data(using: .utf8)!, withName: name)
                        }
                        if let new = parameters[name] as? Int {
                            let value = "\(new)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: name)
                        }

                    }

                   
                    if image1 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        name = name + ".jpeg"
                        multipartFormData.append(image1, withName: "ProofOfResidenceImg", fileName: name, mimeType:"image/jpeg")
                    }
                    if image2 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        
                        name = name + ".jpeg"


                        multipartFormData.append(image2, withName: "SelfieImg", fileName: name, mimeType:"image/jpeg")
                    }
                    if image3 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        name = "mio" + ".jpeg"
                        multipartFormData.append(image3, withName: "ProfilePic", fileName: name, mimeType:"image/jpeg")
                    }
                    if image4 != nil{
                        var name = String(Int64((Date().timeIntervalSince1970 * 1000.0).rounded()))
                        name = name + ".jpeg"
                        multipartFormData.append(image4, withName: "PersonalIDImg", fileName: name, mimeType:"image/jpeg")
                    }
                },
                to: urL!, method: .post , headers: headers)
                
                .response { response in
                    
                    debugPrint(response)
                    print("Response Code:---->",response.response?.statusCode as Any)
        
                    Indicator.shared.stopAnimating()
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        print("Response: \(json ?? "")")
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with:data, options: [])
                            print(jsonResponse)
                            completion(jsonResponse as? [String : AnyObject] , nil)
                            
                        } catch let parsingError {
                            print("Error", parsingError)
                            completion(nil , parsingError)
                        }
                    }
                }
        }
        else{
            AlamofireRequest.shared.MsgAlert()
        }
    }

}
