//
//  CoustomFont.swift
//  DriverMinaApp
//
//  Created by Apple on 08/02/21.
//
import UIKit

extension UIFont {
    
    static func RegularFont(_ floatSize:Float) -> UIFont {
        let navTitleFont = UIFont(name: "NunitoSans-Regular", size: CGFloat(floatSize)) ?? UIFont.systemFont(ofSize: CGFloat(floatSize), weight: .regular)
        return navTitleFont
    }
    
    
    static func BoldFont(_ floatSize:Float) -> UIFont {
        let actionTitleFont = UIFont(name: "NunitoSans-Bold", size: CGFloat(floatSize)) ?? UIFont.systemFont(ofSize: CGFloat(floatSize), weight: .bold)
        return actionTitleFont
    }
        
    static func SemiBoldFont(_ floatSize:Float) -> UIFont {
        let actionTitleFont = UIFont(name: "NunitoSans-SemiBold", size: CGFloat(floatSize)) ?? UIFont.systemFont(ofSize: CGFloat(floatSize), weight: .semibold)
        return actionTitleFont
    }
        
    static func ExtraBoldFont(_ floatSize:Float) -> UIFont {
        let actionTitleFont = UIFont(name: "NunitoSans-ExtraBold", size: CGFloat(floatSize)) ?? UIFont.systemFont(ofSize: CGFloat(floatSize), weight: .black)
        return actionTitleFont
    }
}
