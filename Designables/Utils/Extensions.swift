//
//  Extensions.swift
//  iBlah-Blah
//
//  Created by Aditya Srivastava on 23/04/19.
//  Copyright © 2019 Aditya Srivastava. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "AvenirNext-Bold", size: 16)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}

extension UITableView {
    func updateHeaderViewHeight() {
        if let header = self.tableHeaderView {
            let newSize = header.systemLayoutSizeFitting(CGSize(width: self.bounds.width, height: 0))
            header.frame.size.height = newSize.height
        }
    }
    
    //Variable-height UITableView tableHeaderView with autolayout
    func layoutTableHeaderView() {
        
        guard let headerView = self.tableHeaderView else { return }
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        let headerWidth = headerView.bounds.size.width;
        let temporaryWidthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "[headerView(width)]", options: NSLayoutConstraint.FormatOptions(rawValue: UInt(0)), metrics: ["width": headerWidth], views: ["headerView": headerView])
        
        headerView.addConstraints(temporaryWidthConstraints)
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let headerSize = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        let height = headerSize.height
        var frame = headerView.frame
        
        frame.size.height = height
        headerView.frame = frame
        
        self.tableHeaderView = headerView
        
        headerView.removeConstraints(temporaryWidthConstraints)
        headerView.translatesAutoresizingMaskIntoConstraints = true
        
    }
}

public extension String{

func EmailValidation() -> Bool
{
    let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailReg)
    if emailTest.evaluate(with: self) == false {
        return false
    }
    else
    {
        return true
    }
}
    

    
    func isValidPhone() -> Bool {
            let phoneRegex = "^[0-9]{0,1}+[0-9]{5,16}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return phoneTest.evaluate(with: self)
        }
    
func PasswordValidationCheck() -> Bool
{
    
   // let passReg = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,}"
    
    let passReg = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
    
    let passTest = NSPredicate(format: "SELF MATCHES %@", passReg)
    if passTest.evaluate(with: self) == false {
        return false
    }
    else
    {
        return true
    }
    
}
var isNumeric: Bool {
    guard self.count > 0 else { return false }
    let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    return Set(self).isSubset(of: nums)//(self.characters).isSubset(of: nums)
}
var isContainsLetters : Bool{
    let letters = CharacterSet.letters
    return self.rangeOfCharacter(from: letters) != nil
}
var isContainsNumbers : Bool{
    let numbers = CharacterSet(charactersIn: "0123456789")
    return self.rangeOfCharacter(from: numbers) != nil
}
}
public extension UITextField{
    
    func EmailValidation() -> Bool
    {
        let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailReg)
        if emailTest.evaluate(with: self.text) == false {
            return false
        }
        else
        {
            return true
        }
    }
    
    func PasswordValidation() -> Bool{
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d$@$#!%*?&]{8,}")
        
        if passwordTest.evaluate(with: self.text) == false {
            return false
        }
        else
        {
            return true
        }
        
    }
    
    func PhoneValidation() -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if phoneTest.evaluate(with: self.text) == false {
            return false
        }
        else
        {
            return true
        }
    }
    
    
    func cornerRadius(value: CGFloat) {
        self.layer.cornerRadius = value
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.masksToBounds = true
    }
    
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }
    var isNumerics: Bool {
        let characterSet = CharacterSet(charactersIn: "0123456789")
        if self.text?.rangeOfCharacter(from: characterSet.inverted) != nil {
            return false
        }
        if self.text?.count != 10
        {
            return false
        }
        return true
    }
    func textFieldBottomBorder()
    {
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width + 40, height: self.frame.size.height)
        
        border.borderWidth = 0.5
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    func Padding () {
        
        let Padding = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: self.frame.height))
        self.leftView = Padding
        self.leftViewMode = .always
        
        
    }
//    func setImageRightPaddingPoints(_ amount:CGFloat , img : Ionicons) {
//        let paddingView = UIImageView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
//        paddingView.image = UIImage.ionicon(with: img, textColor: .black, size: CGSize(width: paddingView.frame.size.width, height: paddingView.frame.size.height))
//        self.rightView = paddingView
//        self.rightViewMode = .always
//    }
    
    func BottomBorder () {
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        
        
    }
}

