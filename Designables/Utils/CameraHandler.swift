//
//  CameraHandler.swift
//  M1 Pay
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//
import Foundation
import UIKit
public enum Document : Int{
    case ID_proof = 0
    case Address_Proof = 1
    case Auth_Docu = 2
    
    static func instance(rawCode : Int)->Document{
        guard let code = Document(rawValue: rawCode)
            else{
                return .ID_proof
        }
        return code
    }


}
class CameraHandler: NSObject{
    static let shared = CameraHandler()
    
    fileprivate var currentVC: UIViewController!
    fileprivate var tagId: Int!
    //MARK: Internal Properties
    var imagePickedBlock: ((_ image : UIImage,_ docs :Document) -> ())?
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.isEditing = true
            currentVC.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.isEditing = true
            currentVC.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func showActionSheet(vc: UIViewController,id : Int) {
        currentVC = vc
        tagId = id
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
}


extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let code = Document.instance(rawCode: tagId)

        print(info)
        if let image = info[.editedImage] as? UIImage {
            
            self.imagePickedBlock?(image, code)
        }
        else if let image = info[.originalImage] as? UIImage {
            
           
                self.imagePickedBlock?(image, code)


           
            
            
        }else{
            print("Something went wrong")
        }
        currentVC.dismiss(animated: true, completion: nil)
    }
    
    
//
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//
//        currentVC.dismiss(animated: true, completion: nil)
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
//
//
//        if let Image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//        {
//
//            var imageName = ""
//            let code = Document.instance(rawCode: tagId)
//            switch code {
//            case .ID_proof:
//               imageName = "document_identity"
//                break
//            case .Address_Proof:
//                imageName = "document_address"
//                break
//            case .Auth_Docu:
//                imageName = "document_authorization"
//                break
//            default:
//                imageName = ""
//            }
//
//            /*ProfileImageUploadRequest().upload(image: image, imageName: imageName)
//            { (status,message ) in
//                if !status
//                {
//                    return
//                }
//
//
//            }*/
//
//
//            self.imagePickedBlock?(Image ,code)
//
//        }else{
//            print("Something went wrong")
//        }
//        currentVC.dismiss(animated: true, completion: nil)
    }
    

